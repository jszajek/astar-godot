extends Node

var request_queue : Object

var path_request_thread : Thread
var path_request_mutex : Mutex

func _init():
	request_queue = PathRequestQueue.new()

func request_path(path_request : PathRequest):
	if path_request_thread == null:
		path_request_thread = Thread.new()
		path_request_mutex = Mutex.new()
	path_request_mutex.lock()
	request_queue.append(path_request)
	path_request_mutex.unlock()
	if request_queue.size() == 1:
		call_deferred('path_data_defered')
    
func path_data_defered():
	var error = path_request_thread.start(self, 'path_data_execute', request_queue.front())
	if error != 0:
		print(error)
    
func path_data_execute(args):
	# Don't calculate or pass if requester is gone/dead
	if get_parent().get_parent().get_tree().get_root().has_node(args.requester_path) == null:
		return
	var new_path_args = get_parent().find_path(args.pathStart, args.pathEnd, args.smooth, args.turning_dist, args.stopping_dist)
	call_deferred('path_data_clean_up')
	return [args.callback, new_path_args, args.requester_path]
    
func path_data_clean_up():
	var args = path_request_thread.wait_to_finish()
	args[0].call_func(args[1])
	request_queue.pop_front()
	
	if request_queue.size() > 0:
		# Start the next task.
		call_deferred('path_data_defered')
	elif request_queue.size() == 0:
		path_request_thread = null
		path_request_mutex = null


class PathRequestQueue:
	var path_request_queue : Array = []

	func append(request : PathRequest):
		var index = find(request)
		if index > 0:
			replace(index, request)
			return
		path_request_queue.append(request)
	
	func find(request : PathRequest):
		for i in range(size()-1, -1, -1):
			if path_request_queue[i].unique_id == request.unique_id:
				if path_request_queue[i].is_processing:
					return -1
				return i
		return -1
	
	func replace(old_index : int, new_request : PathRequest):
		path_request_queue[old_index] = new_request
	
	func size():
		return path_request_queue.size()
	
	func pop_front():
		path_request_queue.pop_front()
	
	func front():
		return path_request_queue.front()


class PathRequest:
	var requester_path : NodePath
	var pathStart : Vector3
	var pathEnd : Vector3
	var smooth : bool
	var turning_dist : float
	var stopping_dist : float
	var callback : FuncRef
	var unique_id : int
	var is_processing : bool = false

	func _init(_requester_path : NodePath, _start : Vector3, _end : Vector3, _callback : FuncRef, 
				_unique_id : int, _smooth : bool, _turning_dist : float, _stopping_dist : float):
		self.requester_path = _requester_path
		self.pathStart = _start
		self.pathEnd = _end
		self.smooth = _smooth
		self.turning_dist = _turning_dist
		self.stopping_dist = _stopping_dist
		self.callback = _callback
		self.unique_id = _unique_id