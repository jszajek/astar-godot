extends KinematicBody

const gravity = Vector3.DOWN * 12

const MIN_PATH_UPDATE_TIME : float = 0.2
const PATH_UPDATE_MOVE_THRESHOLD : float = 0.5
const sqr_move_threshold : float = PATH_UPDATE_MOVE_THRESHOLD * PATH_UPDATE_MOVE_THRESHOLD

var target : KinematicBody
export (float) var move_speed = 6.0
export (float) var turn_dist = 5.0
export (float) var turn_speed = 3.0
export (float) var stopping_distance = 5.0
export (bool) var smooth_path = false
export (bool) var draw_path = false

var waypoints : PoolVector3Array

var Astar : Object
var Astar_core : Object
var pathIndex : int = 0
var following_path : bool = false
var requesting : bool = false
var speedPercent : float = 1

var path_debug : ImmediateGeometry
var point_debug : ImmediateGeometry

var path_timer : Timer

var target_pos_old : Vector3

func _ready():
	path_timer = get_node("Path_Update_Timer")
	path_debug = ImmediateGeometry.new()
	point_debug = ImmediateGeometry.new()
	
	get_parent().get_parent().add_child(path_debug)
	get_parent().get_parent().add_child(point_debug)

func _exit_tree():
	get_parent().get_parent().remove_child(path_debug)
	get_parent().get_parent().remove_child(point_debug)
	
func setup(_astar : Object):
	Astar = _astar
	Astar_core = _astar.get_node("Core")

func start_tracking(new_target : KinematicBody):
	target = new_target
	target_pos_old = target.global_transform.origin
	update_path()

func is_tracking() -> bool:
	return !path_timer.is_stopped()

func stop_tracking():
	following_path = false
	path_timer.stop()

func path_found(_waypoints : PoolVector3Array):
	if len(_waypoints) > 0:
		set_path(_waypoints)

func update_path():
	# Initial Path Request
	request_astar_path()
	
	path_timer.start(MIN_PATH_UPDATE_TIME)

func request_astar_path():
	var unique_id = target.get_instance_id() + self.get_instance_id() 
	Astar.request_path(self.get_path(), global_transform.origin, target.global_transform.origin, funcref(self, "path_found"), unique_id, turn_dist, stopping_distance, smooth_path)

func move_along_path() -> void:
	if !following_path or pathIndex == len(waypoints):
		return
	var currentWaypoint = waypoints[pathIndex]
	if following_path and (pathIndex < len(waypoints)):
		if (global_transform.origin.distance_to(currentWaypoint) < 3):
			pathIndex += 1
			if pathIndex > len(waypoints):
				following_path = false
				return
		var target_trans : Transform = transform.looking_at(currentWaypoint, Vector3.UP)
		rotation = lerp(rotation, target_trans.basis.get_euler(), 1)

		# warning-ignore:return_value_discarded
		move_and_slide(Vector3(0,0,-1 * move_speed * speedPercent).rotated(Vector3.UP, rotation.y), Vector3.UP)

func move_along_smooth_path() -> void:
	# Path has changed during function
	if requesting or pathIndex >= Astar_core.getTurnBoundariesSize():
		return

	var pos2D : Vector2 = Vector2(global_transform.origin.x, global_transform.origin.z)
	print(pos2D)
	while Astar_core.crossedTurnBoundary(pathIndex, pos2D):
		if pathIndex == Astar_core.getFinishLineIndex():
			following_path = false
			break
		else:
			pathIndex += 1

	if following_path:
		speedPercent = 1
		if pathIndex >= Astar_core.getSlowDownIndex() and stopping_distance > 0:
			speedPercent = clamp(Astar_core.distanceFromPoint(Astar_core.getFinishLineIndex(), pos2D) / stopping_distance, 0, 1)
			if speedPercent < 0.01:
				following_path = false
		var target_trans : Transform = transform.looking_at(Astar_core.getLookPoint(pathIndex), Vector3.UP)
		rotation = lerp(rotation, target_trans.basis.get_euler(), 1)

		# warning-ignore:return_value_discarded
		move_and_slide(Vector3(0,0,-1 * move_speed * speedPercent).rotated(Vector3.UP, rotation.y), Vector3.UP)

func set_path(_waypoints : PoolVector3Array) -> void:
	waypoints = _waypoints
#	if draw_path:
#		point_debug.clear()
#		var spatial = SpatialMaterial.new()
#		spatial.albedo_color = Color.red
#		point_debug.material_override = spatial
#		point_debug.begin(Mesh.PRIMITIVE_LINES)
#		for i in range(len(waypoints)):
#			point_debug.add_vertex(waypoints[i])
#		point_debug.end()
			
	pathIndex = 0
	following_path = true
	requesting = false
#	if smooth_path:
#		path_debug.clear()
#		var spatial = SpatialMaterial.new()
#		spatial.albedo_color = Color.blue
#		path_debug.material_override = spatial
#		path_debug.begin(Mesh.PRIMITIVE_LINES)
#		for i in range(Astar_core.getTurnBoundariesSize()):
#			var arr = Astar_core.getVisualLine(i)
#			var lineDir : Vector3 = arr[0]
#			var lineCenter : Vector3 = arr[1]
#			path_debug.add_vertex(lineCenter - lineDir * 10)
#			path_debug.add_vertex(lineCenter + lineDir * 10)
#		point_debug.end()

# warning-ignore:unused_argument
func _physics_process(delta):
	if following_path:
		if smooth_path:
			move_along_smooth_path()
		else:
			move_along_path()

func _on_Path_Update_Timer_timeout():
	if (target.global_transform.origin - target_pos_old).length_squared() > sqr_move_threshold:
		requesting = true
		request_astar_path()
		target_pos_old = target.global_transform.origin
