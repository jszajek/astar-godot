extends Node

onready var path_manager : Object = get_node("Path_Manager")

var create_visual_grid : bool = false
var center_point : Vector3
var gridWorldSize : Vector2 = Vector2(50, 50)
var gridSizeX : int
var gridSizeY : int
var nodeRadius : float = 0.5

var blurWeights : bool = true
var blurrKernelSize : int = 1

var unwalkableMask : int = 7

# Increased obstacle avoidance
var obstacleProximityPenalty : int = 10

var penaltyMax : int
var penaltyMin : int

# Higher Value is equivalent to more Rugged Terrain
var walkable_regions : Array  = [
	TerrainType.new(10, 0),  #Floor
	TerrainType.new(11, 10)]  #Grass

var bake_time : String = ""

func bake_map():
	var time_start = OS.get_ticks_msec()
	var dims = create_grid(unwalkableMask, gridWorldSize, nodeRadius, walkable_regions)
	gridSizeX = dims[0]
	gridSizeY = dims[1]
	
	clear_visual_grid()

	if create_visual_grid:
		create_visual_grid()
	bake_time = "Bake Time: " + str(OS.get_ticks_msec() - time_start) + " msec"

func create_grid(_unwalkableMask : int, _gridWorldSize : Vector2, _nodeRadius : float, _walkable_regions : Array) -> Array:
	var nodeDiameter : float = _nodeRadius * 2
	var walkableMask : PoolIntArray = []
	var walkableRegionDictionary : Dictionary = {}
	
	var _gridSizeX = int(ceil(gridWorldSize.x / nodeDiameter))
	var _gridSizeY = int(ceil(gridWorldSize.y / nodeDiameter))
	
	penaltyMin = _walkable_regions[0].terrainPenalty
	penaltyMax = _walkable_regions[-1].terrainPenalty
	$Core.setupGrid(_gridSizeX, _gridSizeY, penaltyMin, penaltyMax)
	
	for terrain in _walkable_regions:
		walkableMask.append(terrain.terrainMask - 1) # Minus 1 b/c layer is base 1 and bits are base 0
		walkableRegionDictionary[terrain.terrainMask] = terrain.terrainPenalty
	
	var worldBottomLeft : Vector3 = center_point - (Vector3.RIGHT * round(_gridWorldSize.x/2.0)) - (Vector3.FORWARD * round(gridWorldSize.y/2.0))
	var direct_space : PhysicsDirectSpaceState = $Node_Grid.get_world().direct_space_state
	var check_sphere : PhysicsShapeQueryParameters = PhysicsShapeQueryParameters.new()
	var shape : SphereShape = SphereShape.new()
	shape.radius = _nodeRadius
	check_sphere.set_shape(shape)
	check_sphere.collision_mask = get_collision_bit([_unwalkableMask])
	var terrain_layer_bit = get_collision_bit(walkableMask)
	
	for x in range(_gridSizeX):
		for y in range(_gridSizeY):
			var worldPoint : Vector3 = worldBottomLeft + (Vector3.RIGHT * (x * nodeDiameter + _nodeRadius)) + (Vector3.FORWARD * (y * nodeDiameter + _nodeRadius))
			check_sphere.transform.origin = worldPoint
			var result = direct_space.intersect_shape(check_sphere)
			var walkable : bool = result.size() == 0
			var movement_penalty : int = 0

			var results = direct_space.intersect_ray(worldPoint + (Vector3.UP * 100), worldPoint + (Vector3.DOWN * 100), [], terrain_layer_bit)
			if results:
				var layer = log_base(results.collider.collision_layer - 256, 2) # Subtract walkable layer
				# convert back from base 0 to base 1
				if walkableRegionDictionary.has(layer+1):
					movement_penalty = walkableRegionDictionary[layer+1]
			else:
				walkable = false
			if !walkable:
				movement_penalty += obstacleProximityPenalty
			
			$Core.addGridPoint(worldPoint, x, y, walkable, movement_penalty)
	
	if blurWeights:
		var blur_results : Array = $Core.blurWeights(blurrKernelSize)
		penaltyMin = blur_results[0]
		penaltyMax = blur_results[1]
		
	return [_gridSizeX, _gridSizeY]

func request_path(requester_path : NodePath, startPos : Vector3, targetPos : Vector3, callback : FuncRef, 
				  unique_id : int, smooth := false, turnDist := 0, stoppingDist := 0):
	path_manager.request_path($Path_Manager.PathRequest.new(requester_path, startPos, targetPos, callback, unique_id, smooth, turnDist, stoppingDist))

func find_path(startPos : Vector3, targetPos : Vector3, smooth : bool, turnDist : float, stoppingDist : float) -> Array:
	var waypoints : Array = $Core.findPath(startPos, targetPos)
	if smooth:
		$Core.smoothPath(waypoints, startPos, turnDist, stoppingDist)
	return waypoints

func get_bake_time() -> String:
	return bake_time

func visualize_grid():
	if $Core == null:
		return
	if get_node("Node_Grid").get_child_count() > 0:
		clear_visual_grid()
	else:
		create_visual_grid()

func clear_visual_grid():
	for child in get_node("Node_Grid").get_children():
		child.queue_free()

func log_base(value : float, base : int) -> int:
		return int(log(value)/log(base))

func get_collision_bit(array_of_values : PoolIntArray) -> int:
	var bit : int = 0
	for value in array_of_values:
		bit += int(pow(2, value))
	return bit

func create_visual_grid():
	if get_node("Node_Grid").get_child_count() > 0:
		get_node("Node_Grid").show()
	else:
		for x in range(gridSizeX):
			for y in range(gridSizeY):
				# Format of vals - [ movement penalty, world position, walkable ]
				var vals : Array = $Core.getGridPoint(x,y)
				
				if vals == null:
					continue
					
				var move_penalty : float = vals[0]
				var position : Vector3 = vals[1]
				var walkable : bool = vals[2]
				var color : Color = Color.white
				
				if move_penalty != 0:
					color = lerp(Color.white, Color.black, inverse_lerp(penaltyMin, penaltyMax, move_penalty))
				
				color = color if walkable else Color.red
				create_visual_indicator(position + Vector3(0,1,0), color)

func create_visual_indicator(position : Vector3, color : Color):
	var mesh_instance = MeshInstance.new()
	var material = SpatialMaterial.new()
	var mesh = SphereMesh.new()
	mesh.radius = nodeRadius/2
	mesh.height = mesh.radius*2
	mesh_instance.set_mesh(mesh)
	material.albedo_color = color
	get_node("Node_Grid").add_child(mesh_instance)
	mesh_instance.set_surface_material(0, material)
	mesh_instance.global_transform.origin = position

class TerrainType:
	var terrainMask : int
	var terrainPenalty : int
	
	func _init(_mask : int, _penalty : int):
		self.terrainMask = _mask
		self.terrainPenalty = _penalty 
