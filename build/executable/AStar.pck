GDPC                                                                                D   res://.import/AstarPathing.png-44dac335c3cf2c2f71dac3fd3f426d0f.stex �      El      �p"4�&h�0�I��.AD   res://.import/AstarPathing.svg-346480753f98003893249cd294368285.stex�      �%      "�m�I�����ѱ�D�   res://Main.tscn �      �      	YJ,������&�9�   res://assets/AStar.tscn �      �      ��m�zF�Gp��Q   res://assets/Canvas.tscn�      �)      ��Zo�?�1o��z   res://assets/Seeker.tscn�<            @oyt��W�Þ�   res://assets/Target.tscn�A      �      �ae�7
�e�s��>f   res://assets/World-Gap.tscn �C      +"      �?����@hEYw�9e   res://assets/World.tscn �e      �      ^<�8衭M�x7�	l�w   res://bin/gdastar.gdnlib��            h�ѥ��$�t�   res://bin/gdastar.gdns  ��      	       1���\�lp���ֺ)}   res://default_env.tres  ��      a      ���.���PK��Ç���   res://icons/AstarPathing.png�     YX      J���L ��B� $   res://icons/AstarPathing.png.import p�      �      [�NP��_�X"ȱ�c$   res://icons/AstarPathing.svg.import �     �      O�c�i=��Q��T%�D(   res://project.binaryp�     �      �]�^�5��d�(���,   res://scripts/AStar Implementation.gd.remap ��     7       ɾ��q.H�j5�)���S(   res://scripts/AStar Implementation.gdc  p      �      b����!��(��ؓ�P   res://scripts/AStar.gd.remap �     (       \���
>���z��   res://scripts/AStar.gdc  .     �      ܶ�����=�Øn��    res://scripts/Canvas.gd.remap   P�     )       ���FZ��-/�(T��R   res://scripts/Canvas.gdc I     �      X[xN�(� �iu;� �   res://scripts/Main.gd.remap ��     '       �K�D��%�nŚ��   res://scripts/Main.gdc   [     �      ���������9�]I�$   res://scripts/Path_Manager.gd.remap ��     /       ����V�R���_�    res://scripts/Path_Manager.gdc  �h     �      \�}�p���s� 4    res://scripts/Seeker.gd.remap   ��     )       �_���|�eq9�    res://scripts/Seeker.gdc�u     ,      Ի�=�����9��+���            [gd_scene load_steps=7 format=2]

[ext_resource path="res://scripts/Main.gd" type="Script" id=1]
[ext_resource path="res://assets/Seeker.tscn" type="PackedScene" id=2]
[ext_resource path="res://assets/AStar.tscn" type="PackedScene" id=3]
[ext_resource path="res://assets/Canvas.tscn" type="PackedScene" id=4]
[ext_resource path="res://assets/Target.tscn" type="PackedScene" id=5]
[ext_resource path="res://assets/World.tscn" type="PackedScene" id=6]

[node name="Node" type="Node"]
script = ExtResource( 1 )
seeker = ExtResource( 2 )

[node name="AStar" parent="." instance=ExtResource( 3 )]

[node name="Canvas" parent="." instance=ExtResource( 4 )]

[node name="DirectionalLight" type="DirectionalLight" parent="."]
transform = Transform( 0.866025, 0.211309, -0.453154, 0, 0.906308, 0.422618, 0.5, -0.365998, 0.784886, -31, 16.6627, 23 )
light_energy = 1.1
shadow_enabled = true

[node name="Pivot" type="Spatial" parent="."]
transform = Transform( 1, 0, 0, 0, 0.866025, 0.5, 0, -0.5, 0.866025, 0, 0, 0 )

[node name="Swivel" type="Spatial" parent="Pivot"]
editor/display_folded = true

[node name="Camera" type="Camera" parent="Pivot/Swivel"]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 42 )

[node name="Spawn_Points" type="Spatial" parent="."]
editor/display_folded = true

[node name="Position3D" type="Position3D" parent="Spawn_Points"]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, -14, 1.5, 4 )

[node name="Position3D2" type="Position3D" parent="Spawn_Points"]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, -12, 1.5, -10 )

[node name="Position3D3" type="Position3D" parent="Spawn_Points"]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, 2, 1.5, -13 )

[node name="Position3D4" type="Position3D" parent="Spawn_Points"]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, 17, 1.5, -7 )

[node name="Position3D5" type="Position3D" parent="Spawn_Points"]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, 20, 1.5, 5 )

[node name="Position3D6" type="Position3D" parent="Spawn_Points"]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, 15, 1.5, 20 )

[node name="Target" parent="." instance=ExtResource( 5 )]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 1, 3, 1.5, 22 )

[node name="Seekers" type="Node" parent="."]

[node name="World" parent="." instance=ExtResource( 6 )]
  [gd_scene load_steps=4 format=2]

[ext_resource path="res://scripts/AStar.gd" type="Script" id=1]
[ext_resource path="res://bin/gdastar.gdns" type="Script" id=2]
[ext_resource path="res://scripts/Path_Manager.gd" type="Script" id=3]

[node name="AStar" type="Node"]
script = ExtResource( 1 )

[node name="Node_Grid" type="Spatial" parent="."]

[node name="Core" type="Node" parent="."]
script = ExtResource( 2 )

[node name="Path_Manager" type="Node" parent="."]
script = ExtResource( 3 )
       [gd_scene load_steps=6 format=2]

[ext_resource path="res://scripts/Canvas.gd" type="Script" id=1]

[sub_resource type="StyleBoxFlat" id=1]
bg_color = Color( 0.686275, 0.686275, 0.686275, 1 )
border_width_left = 2
border_width_right = 2
border_width_bottom = 2
border_color = Color( 0.294118, 0.294118, 0.294118, 1 )

[sub_resource type="StyleBoxFlat" id=2]
bg_color = Color( 0.52549, 0.521569, 0.521569, 1 )
border_width_top = 2
border_width_right = 2
border_width_bottom = 2
border_color = Color( 0.294118, 0.294118, 0.294118, 1 )

[sub_resource type="StyleBoxFlat" id=3]
bg_color = Color( 0.188235, 0.188235, 0.188235, 0.294118 )

[sub_resource type="StyleBoxFlat" id=4]
bg_color = Color( 0.52549, 0.521569, 0.521569, 1 )
border_width_left = 2
border_width_right = 2
border_width_bottom = 2
border_color = Color( 0.294118, 0.294118, 0.294118, 1 )

[node name="Canvas" type="Control"]
anchor_right = 1.0
anchor_bottom = 1.0
mouse_filter = 2
script = ExtResource( 1 )

[node name="Label" type="Label" parent="."]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -70.5
margin_top = 15.0
margin_right = 89.5
margin_bottom = 31.0
rect_scale = Vector2( 3, 3 )
rect_pivot_offset = Vector2( 80, 0 )
text = "AStar Pathfinding"
align = 1
valign = 1

[node name="Bake_Time" type="Label" parent="."]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -83.5
margin_top = 50.71
margin_right = 83.5
margin_bottom = 80.7101
text = "Bake Time : --"
align = 1
valign = 1

[node name="Parameters" type="ColorRect" parent="."]
anchor_left = 1.0
anchor_right = 1.0
anchor_bottom = 1.0
margin_left = -249.0
margin_right = 0.998535
rect_pivot_offset = Vector2( 0, 300 )
color = Color( 0.482353, 0.482353, 0.482353, 0.392157 )

[node name="Parameter_Button" type="Button" parent="Parameters"]
anchor_top = 0.5
anchor_bottom = 0.5
margin_top = -20.0
margin_right = 20.0
margin_bottom = 20.0
focus_mode = 0
custom_styles/hover = SubResource( 1 )
custom_styles/normal = SubResource( 2 )
enabled_focus_mode = 0
text = ">"

[node name="Display" type="Control" parent="Parameters"]
anchor_right = 1.0
anchor_bottom = 1.0
mouse_filter = 2

[node name="Label" type="Label" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -127.0
margin_top = 10.0
margin_right = 143.0
margin_bottom = 40.0
text = "Map Parameters"
align = 1
valign = 1

[node name="Visualize_Grid" type="Panel" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -105.0
margin_top = 50.0
margin_right = 105.0
margin_bottom = 90.0
custom_styles/panel = SubResource( 3 )

[node name="Label" type="Label" parent="Parameters/Display/Visualize_Grid"]
anchor_top = 0.5
anchor_bottom = 0.5
margin_left = 10.0
margin_top = -7.0
margin_right = 150.0
margin_bottom = 7.0
text = "Visualize Grid"
valign = 1

[node name="Visualize_Grid_CheckBox" type="CheckBox" parent="Parameters/Display/Visualize_Grid"]
anchor_left = 1.0
anchor_top = 0.5
anchor_right = 1.0
anchor_bottom = 0.5
margin_left = -30.0
margin_top = -12.0
margin_right = -6.0
margin_bottom = 12.0
focus_mode = 0
pressed = true
enabled_focus_mode = 0

[node name="Node_Size" type="Panel" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -105.0
margin_top = 100.0
margin_right = 105.0
margin_bottom = 140.0
custom_styles/panel = SubResource( 3 )

[node name="Label" type="Label" parent="Parameters/Display/Node_Size"]
anchor_top = 0.5
anchor_bottom = 0.5
margin_left = 10.0
margin_top = -7.0
margin_right = 150.0
margin_bottom = 7.0
text = "Node Radius"
valign = 1

[node name="LineEdit" type="LineEdit" parent="Parameters/Display/Node_Size"]
anchor_left = 1.0
anchor_top = 0.5
anchor_right = 1.0
anchor_bottom = 0.5
margin_left = -62.0
margin_top = -12.0
margin_right = -4.0
margin_bottom = 12.0
text = "1.0"
align = 1
context_menu_enabled = false

[node name="Weights_Control" type="Panel" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -105.0
margin_top = 150.0
margin_right = 105.0
margin_bottom = 190.0
custom_styles/panel = SubResource( 3 )

[node name="Label" type="Label" parent="Parameters/Display/Weights_Control"]
anchor_top = 0.5
anchor_bottom = 0.5
margin_left = 10.0
margin_top = -7.0
margin_right = 150.0
margin_bottom = 7.0
text = "Blurr Weights"
valign = 1

[node name="CheckBox" type="CheckBox" parent="Parameters/Display/Weights_Control"]
anchor_left = 1.0
anchor_top = 0.5
anchor_right = 1.0
anchor_bottom = 0.5
margin_left = -30.0
margin_top = -12.0
margin_right = -6.0
margin_bottom = 12.0
focus_mode = 0
pressed = true
enabled_focus_mode = 0

[node name="Kernel_Size" type="Panel" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -105.0
margin_top = 200.0
margin_right = 105.0
margin_bottom = 240.0
custom_styles/panel = SubResource( 3 )

[node name="Label" type="Label" parent="Parameters/Display/Kernel_Size"]
anchor_top = 0.5
anchor_bottom = 0.5
margin_left = 10.0
margin_top = -7.0
margin_right = 150.0
margin_bottom = 7.0
text = "Kernel Size"
valign = 1

[node name="LineEdit" type="LineEdit" parent="Parameters/Display/Kernel_Size"]
anchor_left = 1.0
anchor_top = 0.5
anchor_right = 1.0
anchor_bottom = 0.5
margin_left = -62.0
margin_top = -12.0
margin_right = -4.0
margin_bottom = 12.0
text = "1"
align = 1
max_length = 1
context_menu_enabled = false

[node name="Bake_Button" type="Button" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -85.0
margin_top = 250.0
margin_right = 85.0
margin_bottom = 280.0
focus_mode = 0
enabled_focus_mode = 0
text = "Bake Navigation Grid"

[node name="Label2" type="Label" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -137.0
margin_top = 320.0
margin_right = 133.0
margin_bottom = 350.0
text = "Seekers Parameters"
align = 1
valign = 1

[node name="Spawn_Seeker" type="Button" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -85.0
margin_top = 360.0
margin_right = 85.0
margin_bottom = 390.0
disabled = true
enabled_focus_mode = 0
text = "Spawn Seeker"

[node name="Clear_Seekers" type="Button" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -85.0
margin_top = 405.0
margin_right = 85.0
margin_bottom = 435.0
disabled = true
enabled_focus_mode = 0
text = "Clear Seekers"

[node name="Visualize_Path" type="Panel" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -105.0
margin_top = 445.0
margin_right = 105.0
margin_bottom = 485.0
custom_styles/panel = SubResource( 3 )

[node name="Label" type="Label" parent="Parameters/Display/Visualize_Path"]
anchor_top = 0.5
anchor_bottom = 0.5
margin_left = 10.0
margin_top = -7.0
margin_right = 150.0
margin_bottom = 7.0
text = "Visualize Path"
valign = 1

[node name="Visualize_Path_CheckBox" type="CheckBox" parent="Parameters/Display/Visualize_Path"]
anchor_left = 1.0
anchor_top = 0.5
anchor_right = 1.0
anchor_bottom = 0.5
margin_left = -30.0
margin_top = -12.0
margin_right = -6.0
margin_bottom = 12.0
focus_mode = 0
enabled_focus_mode = 0

[node name="Track_Button" type="Button" parent="Parameters/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -85.0
margin_top = 495.0
margin_right = 85.0
margin_bottom = 525.0
disabled = true
enabled_focus_mode = 0
text = "Start Tracking"

[node name="Navigation" type="ColorRect" parent="."]
anchor_left = 0.5
anchor_top = 1.0
anchor_right = 0.5
anchor_bottom = 1.0
margin_left = -175.0
margin_top = -120.0
margin_right = 175.0
margin_bottom = 0.000183105
color = Color( 0.482353, 0.482353, 0.482353, 0.392157 )

[node name="Navigation_Button" type="Button" parent="Navigation"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -50.0
margin_right = 50.0
margin_bottom = 20.0
focus_mode = 0
custom_styles/hover = SubResource( 1 )
custom_styles/normal = SubResource( 4 )
enabled_focus_mode = 0
text = "v"

[node name="Display" type="Control" parent="Navigation"]
editor/display_folded = true
anchor_right = 1.0
anchor_bottom = 1.0
mouse_filter = 2

[node name="Down_Rotation" type="Button" parent="Navigation/Display"]
anchor_left = 0.5
anchor_top = 1.0
anchor_right = 0.5
anchor_bottom = 1.0
margin_left = -50.0
margin_top = -40.0
margin_right = 50.0
margin_bottom = -10.0
focus_mode = 0
enabled_focus_mode = 0
text = "v"

[node name="Up_Rotation" type="Button" parent="Navigation/Display"]
anchor_left = 0.5
anchor_top = 1.0
anchor_right = 0.5
anchor_bottom = 1.0
margin_left = -50.0
margin_top = -70.0
margin_right = 50.0
margin_bottom = -40.0
focus_mode = 0
enabled_focus_mode = 0
text = "^"

[node name="Right_Rotation" type="Button" parent="Navigation/Display"]
anchor_left = 0.5
anchor_top = 1.0
anchor_right = 0.5
anchor_bottom = 1.0
margin_left = 50.0
margin_top = -70.0
margin_right = 115.0
margin_bottom = -10.0
focus_mode = 0
enabled_focus_mode = 0
text = ">"

[node name="Left_Rotation" type="Button" parent="Navigation/Display"]
anchor_left = 0.5
anchor_top = 1.0
anchor_right = 0.5
anchor_bottom = 1.0
margin_left = -115.0
margin_top = -70.0
margin_right = -50.0
margin_bottom = -10.0
focus_mode = 0
enabled_focus_mode = 0
text = "<"

[node name="Label" type="Label" parent="Navigation/Display"]
anchor_left = 0.5
anchor_right = 0.5
margin_left = -115.0
margin_top = 27.0
margin_right = 115.0
margin_bottom = 41.0
text = "Navigation"
align = 1
valign = 1
[connection signal="pressed" from="Parameters/Parameter_Button" to="." method="_on_Parameter_Button_pressed"]
[connection signal="pressed" from="Parameters/Display/Visualize_Grid/Visualize_Grid_CheckBox" to="." method="_on_Visualize_Grid_CheckBox_pressed"]
[connection signal="pressed" from="Parameters/Display/Bake_Button" to="." method="_on_Bake_Button_pressed"]
[connection signal="pressed" from="Parameters/Display/Spawn_Seeker" to="." method="_on_Spawn_Seeker_pressed"]
[connection signal="pressed" from="Parameters/Display/Clear_Seekers" to="." method="_on_Clear_Seekers_pressed"]
[connection signal="pressed" from="Parameters/Display/Visualize_Path/Visualize_Path_CheckBox" to="." method="_on_Visualize_Path_CheckBox_pressed"]
[connection signal="pressed" from="Parameters/Display/Track_Button" to="." method="_on_Track_Button_pressed"]
[connection signal="pressed" from="Navigation/Navigation_Button" to="." method="_on_Navigation_Button_pressed"]
[connection signal="pressed" from="Navigation/Display/Down_Rotation" to="." method="_on_Down_Rotation_pressed"]
[connection signal="pressed" from="Navigation/Display/Up_Rotation" to="." method="_on_Up_Rotation_pressed"]
[connection signal="pressed" from="Navigation/Display/Right_Rotation" to="." method="_on_Right_Rotation_pressed"]
[connection signal="pressed" from="Navigation/Display/Left_Rotation" to="." method="_on_Left_Rotation_pressed"]
           [gd_scene load_steps=7 format=2]

[ext_resource path="res://scripts/Seeker.gd" type="Script" id=1]

[sub_resource type="SphereShape" id=1]
radius = 2.0

[sub_resource type="SphereMesh" id=2]

[sub_resource type="SpatialMaterial" id=3]
albedo_color = Color( 0, 1, 0.952941, 1 )

[sub_resource type="PrismMesh" id=4]
size = Vector3( 1.5, 1, 0.25 )

[sub_resource type="SpatialMaterial" id=5]
albedo_color = Color( 0.643137, 0.643137, 0.643137, 1 )

[node name="Seeker" type="KinematicBody"]
script = ExtResource( 1 )
stopping_distance = 1.0

[node name="CollisionShape" type="CollisionShape" parent="."]
shape = SubResource( 1 )

[node name="Body" type="MeshInstance" parent="."]
mesh = SubResource( 2 )
material/0 = SubResource( 3 )

[node name="Direction_Indicator" type="MeshInstance" parent="Body"]
transform = Transform( 1, 0, 0, 0, -4.37114e-008, 1, 0, -1, -4.37114e-008, 0, 0, -1 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="Point_Debug" type="ImmediateGeometry" parent="."]
transform = Transform( -4.37114e-008, 0, -1, 0, 1, 0, 1, 0, -4.37114e-008, 0, 0, 0 )

[node name="Path_Debug" type="ImmediateGeometry" parent="."]

[node name="Path_Update_Timer" type="Timer" parent="."]
[connection signal="timeout" from="Path_Update_Timer" to="." method="_on_Path_Update_Timer_timeout"]
   [gd_scene load_steps=4 format=2]

[sub_resource type="SphereMesh" id=1]

[sub_resource type="SpatialMaterial" id=2]
albedo_color = Color( 1, 0.215686, 0.215686, 1 )

[sub_resource type="SphereShape" id=3]

[node name="Target" type="KinematicBody"]

[node name="MeshInstance" type="MeshInstance" parent="."]
mesh = SubResource( 1 )
material/0 = SubResource( 2 )

[node name="CollisionShape" type="CollisionShape" parent="."]
shape = SubResource( 3 )
disabled = true
               [gd_scene load_steps=10 format=2]

[sub_resource type="CubeMesh" id=1]
size = Vector3( 50, 1, 50 )

[sub_resource type="SpatialMaterial" id=2]
albedo_color = Color( 0.0117647, 0.541176, 0.133333, 1 )

[sub_resource type="ConvexPolygonShape" id=3]
points = PoolVector3Array( -25, 0.5, 25, 25, 0.5, -25, 25, 0.5, 25, -25, 0.5, -25, -25, -0.5, 25, 25, -0.5, -25, 25, -0.5, 25, -25, -0.5, -25, 25, 0.5, 25, -25, 0.5, -25, 25, 0.5, -25, -25, 0.5, 25, 25, -0.5, 25, -25, -0.5, -25, 25, -0.5, -25, -25, -0.5, 25, 25, 0.5, 25, -25, -0.5, 25, -25, 0.5, 25, 25, -0.5, 25, 25, 0.5, -25, -25, -0.5, -25, -25, 0.5, -25, 25, -0.5, -25 )

[sub_resource type="CubeMesh" id=4]

[sub_resource type="SpatialMaterial" id=5]
albedo_color = Color( 0.34902, 0.34902, 0.34902, 1 )

[sub_resource type="ConvexPolygonShape" id=6]
points = PoolVector3Array( -1, 1, 1, 1, 1, -1, 1, 1, 1, -1, 1, -1, -1, -1, 1, 1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 1, 1, -1, 1, -1, 1, 1, -1, -1, 1, 1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, -1, 1, 1, 1, 1, -1, -1, 1, -1, 1, 1, 1, -1, 1, 1, 1, -1, -1, -1, -1, -1, 1, -1, 1, -1, -1 )

[sub_resource type="CubeMesh" id=7]
size = Vector3( 1, 0.5, 1 )

[sub_resource type="SpatialMaterial" id=8]
albedo_color = Color( 0.505882, 0.262745, 0, 1 )

[sub_resource type="ConvexPolygonShape" id=9]
points = PoolVector3Array( -0.5, 0.25, 0.5, 0.5, 0.25, -0.5, 0.5, 0.25, 0.5, -0.5, 0.25, -0.5, -0.5, -0.25, 0.5, 0.5, -0.25, -0.5, 0.5, -0.25, 0.5, -0.5, -0.25, -0.5, 0.5, 0.25, 0.5, -0.5, 0.25, -0.5, 0.5, 0.25, -0.5, -0.5, 0.25, 0.5, 0.5, -0.25, 0.5, -0.5, -0.25, -0.5, 0.5, -0.25, -0.5, -0.5, -0.25, 0.5, 0.5, 0.25, 0.5, -0.5, -0.25, 0.5, -0.5, 0.25, 0.5, 0.5, -0.25, 0.5, 0.5, 0.25, -0.5, -0.5, -0.25, -0.5, -0.5, 0.25, -0.5, 0.5, -0.25, -0.5 )

[node name="World-Gap" type="Spatial"]

[node name="Grass" type="MeshInstance" parent="."]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 0.3, 0, 0, -17.5196 )
mesh = SubResource( 1 )
material/0 = SubResource( 2 )

[node name="StaticBody" type="StaticBody" parent="Grass"]
editor/display_folded = true
collision_layer = 1280

[node name="CollisionShape" type="CollisionShape" parent="Grass/StaticBody"]
shape = SubResource( 3 )

[node name="Grass2" type="MeshInstance" parent="."]
transform = Transform( 1, 0, 0, 0, 1, 0, 0, 0, 0.497, 0, 0, 12.7042 )
mesh = SubResource( 1 )
material/0 = SubResource( 2 )

[node name="StaticBody" type="StaticBody" parent="Grass2"]
editor/display_folded = true
collision_layer = 1280

[node name="CollisionShape" type="CollisionShape" parent="Grass2/StaticBody"]
shape = SubResource( 3 )

[node name="Grass3" type="MeshInstance" parent="."]
transform = Transform( 0.512, 0, 0, 0, 1, 0, 0, 0, 0.249, 12.2399, 0, -4.73234 )
mesh = SubResource( 1 )
material/0 = SubResource( 2 )

[node name="StaticBody" type="StaticBody" parent="Grass3"]
editor/display_folded = true
collision_layer = 1280

[node name="CollisionShape" type="CollisionShape" parent="Grass3/StaticBody"]
shape = SubResource( 3 )

[node name="Obstacles" type="Spatial" parent="."]
editor/display_folded = true

[node name="Rock_1" type="MeshInstance" parent="Obstacles"]
transform = Transform( 1.71257, 0, 1.46013, 0, 2.12504, 0, -1.71257, 0, 1.46013, -16.6858, 2.42098, -15.6126 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_1"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_1/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_2" type="MeshInstance" parent="Obstacles"]
transform = Transform( 4.76534, 0, 0, 0, 2.12504, 0, 0, 0, 3.88683, 14.063, 2.42098, 12.0901 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_2"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_2/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_3" type="MeshInstance" parent="Obstacles"]
transform = Transform( 2.1444, 0, 0, 0, 2.12504, 0, 0, 0, 2.36894, 9.06304, 0.420984, 16.2119 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_3"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_3/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_4" type="MeshInstance" parent="Obstacles"]
transform = Transform( 3.96715, 0, 0, 0, 2.97506, 0, 0, 0, 3.55341, -10.4422, 2.42098, 12.5596 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_4"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_4/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_5" type="MeshInstance" parent="Obstacles"]
transform = Transform( 4.51521, 0, 0, 0, 2.97506, 0, 0, 0, 3.55341, 16.5723, 2.42098, -1.89553 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_5"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_5/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_6" type="MeshInstance" parent="Obstacles"]
transform = Transform( 4.24291, 0, -1.21534, 0, 2.97506, 0, 1.54429, 0, 3.33912, 15.0408, 2.42098, -17.5742 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_6"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_6/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_7" type="MeshInstance" parent="Obstacles"]
transform = Transform( 3.45187, 0, -9.11355e-008, 0, 1.58697, 0, 8.40287e-008, 0, 2.50474, 9.12328, 1.19613, -22.7615 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_7"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_7/StaticBody"]
shape = SubResource( 6 )

[node name="Roads" type="Spatial" parent="."]
editor/display_folded = true

[node name="Road_1" type="MeshInstance" parent="Roads"]
editor/display_folded = true
transform = Transform( 6.162, 0, 0, 0, 1, 0, 0, 0, 19.5404, -20.8167, 0.358764, 15.1624 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_1"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_1/StaticBody"]
shape = SubResource( 9 )

[node name="Road_2" type="MeshInstance" parent="Roads"]
editor/display_folded = true
transform = Transform( -1.59484, 0, 19.7229, 0, 1, 0, -5.95203, 0, -5.28475, -13.2908, 0.358764, 5.75345 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_2"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_2/StaticBody"]
shape = SubResource( 9 )

[node name="Road_5" type="MeshInstance" parent="Roads"]
editor/display_folded = true
transform = Transform( -2.6935e-007, 0, 8.83415, 0, 1, 0, -6.162, 0, -3.86153e-007, 0.0514565, 0.358764, 3.22394 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_5"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_5/StaticBody"]
shape = SubResource( 9 )

[node name="Road_6" type="MeshInstance" parent="Roads"]
editor/display_folded = true
transform = Transform( 3.53438, 0, 16.7261, 0, 1, 0, -5.04762, 0, 11.7117, -2.79204, 0.358764, -16.755 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_6"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_6/StaticBody"]
shape = SubResource( 9 )

[node name="Road_7" type="MeshInstance" parent="Roads"]
editor/display_folded = true
transform = Transform( 0, 0, 15.568, 0, 1, 0, -6.162, 0, -9.53674e-007, -17.2814, 0.358764, -22.0921 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_7"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_7/StaticBody"]
shape = SubResource( 9 )

[node name="Road_3" type="MeshInstance" parent="Roads"]
editor/display_folded = true
transform = Transform( 6.162, 0, -7.8134e-008, 0, 1, 0, 2.40731e-008, 0, 20, 4.31256, 0.358764, -3.71169 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_3"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_3/StaticBody"]
shape = SubResource( 9 )
     [gd_scene load_steps=10 format=2]

[sub_resource type="CubeMesh" id=1]
size = Vector3( 50, 1, 50 )

[sub_resource type="SpatialMaterial" id=2]
albedo_color = Color( 0.0117647, 0.541176, 0.133333, 1 )

[sub_resource type="ConvexPolygonShape" id=3]
points = PoolVector3Array( -25, 0.5, 25, 25, 0.5, -25, 25, 0.5, 25, -25, 0.5, -25, -25, -0.5, 25, 25, -0.5, -25, 25, -0.5, 25, -25, -0.5, -25, 25, 0.5, 25, -25, 0.5, -25, 25, 0.5, -25, -25, 0.5, 25, 25, -0.5, 25, -25, -0.5, -25, 25, -0.5, -25, -25, -0.5, 25, 25, 0.5, 25, -25, -0.5, 25, -25, 0.5, 25, 25, -0.5, 25, 25, 0.5, -25, -25, -0.5, -25, -25, 0.5, -25, 25, -0.5, -25 )

[sub_resource type="CubeMesh" id=4]

[sub_resource type="SpatialMaterial" id=5]
albedo_color = Color( 0.34902, 0.34902, 0.34902, 1 )

[sub_resource type="ConvexPolygonShape" id=6]
points = PoolVector3Array( -1, 1, 1, 1, 1, -1, 1, 1, 1, -1, 1, -1, -1, -1, 1, 1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 1, 1, -1, 1, -1, 1, 1, -1, -1, 1, 1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, -1, 1, 1, 1, 1, -1, -1, 1, -1, 1, 1, 1, -1, 1, 1, 1, -1, -1, -1, -1, -1, 1, -1, 1, -1, -1 )

[sub_resource type="CubeMesh" id=7]
size = Vector3( 1, 0.5, 1 )

[sub_resource type="SpatialMaterial" id=8]
albedo_color = Color( 0.505882, 0.262745, 0, 1 )

[sub_resource type="ConvexPolygonShape" id=9]
points = PoolVector3Array( -0.5, 0.25, 0.5, 0.5, 0.25, -0.5, 0.5, 0.25, 0.5, -0.5, 0.25, -0.5, -0.5, -0.25, 0.5, 0.5, -0.25, -0.5, 0.5, -0.25, 0.5, -0.5, -0.25, -0.5, 0.5, 0.25, 0.5, -0.5, 0.25, -0.5, 0.5, 0.25, -0.5, -0.5, 0.25, 0.5, 0.5, -0.25, 0.5, -0.5, -0.25, -0.5, 0.5, -0.25, -0.5, -0.5, -0.25, 0.5, 0.5, 0.25, 0.5, -0.5, -0.25, 0.5, -0.5, 0.25, 0.5, 0.5, -0.25, 0.5, 0.5, 0.25, -0.5, -0.5, -0.25, -0.5, -0.5, 0.25, -0.5, 0.5, -0.25, -0.5 )

[node name="World" type="Spatial"]

[node name="Grass" type="MeshInstance" parent="."]
mesh = SubResource( 1 )
material/0 = SubResource( 2 )

[node name="StaticBody" type="StaticBody" parent="Grass"]
editor/display_folded = true
collision_layer = 1280

[node name="CollisionShape" type="CollisionShape" parent="Grass/StaticBody"]
shape = SubResource( 3 )

[node name="Obstacles" type="Spatial" parent="."]
editor/display_folded = true

[node name="Rock_1" type="MeshInstance" parent="Obstacles"]
transform = Transform( 1.71257, 0, 1.46013, 0, 2.12504, 0, -1.71257, 0, 1.46013, -16.6858, 2.42098, -15.6126 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_1"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_1/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_2" type="MeshInstance" parent="Obstacles"]
transform = Transform( 4.76534, 0, 0, 0, 2.12504, 0, 0, 0, 3.88683, 14.063, 2.42098, 12.0901 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_2"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_2/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_3" type="MeshInstance" parent="Obstacles"]
transform = Transform( 2.1444, 0, 0, 0, 2.12504, 0, 0, 0, 2.36894, 9.06304, 0.420984, 16.2119 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_3"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_3/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_4" type="MeshInstance" parent="Obstacles"]
transform = Transform( 3.96715, 0, 0, 0, 2.97506, 0, 0, 0, 3.55341, -10.4422, 2.42098, 12.5596 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_4"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_4/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_5" type="MeshInstance" parent="Obstacles"]
transform = Transform( 4.51521, 0, 0, 0, 2.97506, 0, 0, 0, 3.55341, 2.5723, 2.42098, -3.89553 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_5"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_5/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_6" type="MeshInstance" parent="Obstacles"]
transform = Transform( 4.24291, 0, -1.21534, 0, 2.97506, 0, 1.54429, 0, 3.33912, 15.0408, 2.42098, -17.5742 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_6"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_6/StaticBody"]
shape = SubResource( 6 )

[node name="Rock_7" type="MeshInstance" parent="Obstacles"]
transform = Transform( 3.45187, 0, -9.11355e-008, 0, 1.58697, 0, 8.40287e-008, 0, 2.50474, 9.12328, 1.19613, -22.7615 )
mesh = SubResource( 4 )
material/0 = SubResource( 5 )

[node name="StaticBody" type="StaticBody" parent="Obstacles/Rock_7"]
collision_layer = 128

[node name="CollisionShape" type="CollisionShape" parent="Obstacles/Rock_7/StaticBody"]
shape = SubResource( 6 )

[node name="Roads" type="Spatial" parent="."]
editor/display_folded = true

[node name="Road_1" type="MeshInstance" parent="Roads"]
transform = Transform( 6.162, 0, 0, 0, 1, 0, 0, 0, 19.5404, 2.5831, 0.358764, 15.316 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_1"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_1/StaticBody"]
shape = SubResource( 9 )

[node name="Road_2" type="MeshInstance" parent="Roads"]
transform = Transform( -2.02012e-007, 0, 20.4187, 0, 1, 0, -6.162, 0, -1.19004e-006, 2.5831, 0.358764, 2.63609 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_2"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_2/StaticBody"]
shape = SubResource( 9 )

[node name="Road_5" type="MeshInstance" parent="Roads"]
transform = Transform( -2.02012e-007, 0, 20.4187, 0, 1, 0, -6.162, 0, -1.19004e-006, 2.5831, 0.358764, -10.4573 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_5"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_5/StaticBody"]
shape = SubResource( 9 )

[node name="Road_6" type="MeshInstance" parent="Roads"]
transform = Transform( 3.53438, 0, 16.7261, 0, 1, 0, -5.04762, 0, 11.7117, -3.16143, 0.358764, -16.7759 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_6"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_6/StaticBody"]
shape = SubResource( 9 )

[node name="Road_7" type="MeshInstance" parent="Roads"]
transform = Transform( 0, 0, 15.568, 0, 1, 0, -6.162, 0, -9.53674e-007, -17.2814, 0.358764, -22.0921 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_7"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_7/StaticBody"]
shape = SubResource( 9 )

[node name="Road_3" type="MeshInstance" parent="Roads"]
transform = Transform( 6.162, 0, 2.97511e-007, 0, 1, 0, 2.40731e-008, 0, 7.2997, -4.55537, 0.358764, -4 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_3"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_3/StaticBody"]
shape = SubResource( 9 )

[node name="Road_4" type="MeshInstance" parent="Roads"]
transform = Transform( 6.162, 0, 2.97511e-007, 0, 1, 0, 2.40731e-008, 0, 7.2997, 9.71567, 0.358764, -4 )
mesh = SubResource( 7 )
material/0 = SubResource( 8 )

[node name="StaticBody" type="StaticBody" parent="Roads/Road_4"]
collision_layer = 768

[node name="CollisionShape" type="CollisionShape" parent="Roads/Road_4/StaticBody"]
shape = SubResource( 9 )
          [general]

singleton=false
load_once=true
symbol_prefix="godot_"
reloadable=true

[entry]

OSX.64="res://bin/osx/libgdastar.dylib"
Windows.64="res://bin/win64/libgdastar.dll"
X11.64="res://bin/x11/libgdastar.so"

[dependencies]

OSX.64=[  ]
Windows.64=[  ]
X11.64=[  ]
   [gd_resource type="NativeScript" load_steps=2 format=2]

[ext_resource path="res://bin/gdastar.gdnlib" type="GDNativeLibrary" id=1]

[resource]

resource_name = "gdastar"
class_name = "GDAStar"
library = ExtResource( 1 )
_sections_unfolded = [ "Resource" ]       [gd_resource type="Environment" load_steps=2 format=2]

[sub_resource type="ProceduralSky" id=1]
sky_top_color = Color( 0.4, 0.666667, 0.815686, 1 )
ground_bottom_color = Color( 1, 0.670588, 0.380392, 1 )
ground_horizon_color = Color( 0.4, 0.666667, 0.815686, 1 )
ground_curve = 17.8289

[resource]
background_mode = 2
background_sky = SubResource( 1 )
               GDSTO  O          )l  PNG �PNG

   IHDR  O  O   e�0n    IDATx���y�U��������$�$저���,�	(�+.I��v�!��\��l	x/.�W�=�o��u!(�	ʾ%$�]�����TW�twUWuU^��Ofz����d�>}Ω������{�s�_��;��vpLz~�y��&�Sx	;!澡ȟ�~x��ǝ�����s
O"""sc>�3<�S�S�Rx��tR�}�����O&�y��4D��v�
�'qk�j���A���?nO���	���0l�a*�����T`r�YO�� O�?��r|����l��y�F�2��$""�7;��H��px
��O�1�~��x�eG`��+�G=L��-���^=O�Q/X��gr���9�')���\����g]@q�i=�<�ǲ���k�S�,7 ��£N&�g���8��DD�|����]x�)P����1��;��{Z_����;x\�-vԩ�]!*u
O""R.�h�>ۍ}���a9ض�%��F�.���O�ŝͣ���J����C\����O;P�����_������%�+V5|=�'(D�@�IDD��$�}���;�J�e�_�<4N�ZT����W�*:/�s$���%�H�{�{�b�+�����\�ke�$""e7��^G�����yX� �eUZ���2�ٸ{��\�EI��$""�s��`�Ծ��eWX߰�7�����
�����<��e����T�+���,�O=���
PmSx�b8!��p���jg<���Y�U �N�5c��-�W�jKti���H��ۓ.M�(���m
N�fc�9���:ø͒��F�DD���f]7��"_,��%�qY�UPO��n�(�@�T�""RFsq#K5�k����i�ޔCuE48�p[�G���W�����=ps$�j37{�U^Ay���z�MM�
PM(<��H
��iP�a��Kr���6�#ڟ=�L#P��DD���-\^�v~l�iM��J��V.2E�@(@5Px��2/��a��M�-�Xl�mQ��R`3��)� 5�����FO�.DM�p#�S�E�=�����^���4F�IDD��pSF��{�I|
p=nDz�������_���5������(�6�*4� G4���h
O""��'�� �Iܥ��{�b`�����Y-}K�IDD�ݳ��uXdX�����X��}����Jk�DD$_�>n��'�6�ƍ���dk`}�:3'��~�qE}D#O""���t�h��*Pţ�%������Q�F�"�DD���}tpH~�n,p)5�7v�P���qMۉ�H>��Nϕ�ͯ������V�5�
���������ȓ������:��ay��Dh��'������薴� �̺i�`9;6a7�4m'""��#��!�*�e[��as����=���y�l�m���cxi��H',�)Hy�lō:�0|��u9Ҷ��'���G�l�;�<��Hv�]�ny;���~�`ʠ�>)<��H��O�]��eSlhCú=?�K=�Px�ly�?�X��(�j�sF�B� Rx�l���#��p�H�[b8v��p����$""�1<�f��4��i���$""�	��ر�q0�@�u Cl�wyQx�ދ�H���ޕy-��o�Ԁj�6 �DD${���eyo��:A�IDD���o��ޑoA�Ћ�]�m�A�4�DD$[����y�"�信���V�X��fZ���~c������i����	R{Ã�
O""�{���IJ�aye�EdM�IDD�O�Hj�pɛeXCu��[�]����ڼKȚ�����p�Κ�(���. k
O""�[��{v��%�0X�w!YQ����w�*x~�EdI�IDDzg6qW�)<��e��Sx��)<��@��j͓���V���[�4�̭���y��%�<��H6\���s���]@�4�$""٩�~��wRtF�IDD$�����I��VxҴ���dI�v�4P�I#O""�;��Lu� �晴'�R�D���@��*<��H��ɻ 鉧�. K����1�<]���J�IDD$Ն{�V��2
O"""�h\0���S	��
OZ�$""Y��� �W�'��00�*�ټK��=�wYRx��&z!���"=c��n%��$""Y[�w�2�r�'�4,����Qx*�wQ�q�Sx��)<�����."KjU ""��8��\�2K����w�ȓ��d�O�sy!����G��	�DD$K>�����K��ܒwYSx��۱۲|��X���!w;/ߢzM�IDD�0p�%u'� �DD�׾{��-=J�f��m ]j
O""�-���
�4�R$!˲�����m (<��H6��m]����[�$�8pc�E�A�IDD�r�L�EH׾�aMCk���RK��DD���W_���g��s�F�`�tl1U�W����ۺ���[�t��W�����F�����Y.���G�/y����d���n��.̡��(>_ET��k���l�p�8��Ȼ�<)<��H~\�z��y�"m�a8�a`8���b�)<��Hv�51��O_ Vg]�t����c�Ņ���$""��!,_ϻi�b���&�,��JDD���>^��xk*���y=Ҏocy[ý��_XZy�|4^��j|6�bd�0��4]�P���� �[\\a؃c��#�Y �}�Qn�	4�$""�a�ߡ��4�R$��ٌ�]F���]�����r�H �<m�cE��uN��)f89P{�0�<��H�&WG���g�qA8�u�=��ȓ���f=�j�_vDׅ������l��3��N��$""���N6}�Ӹ��YU$a�`8xh��p3����M�0�#oV��i`�,w�F �(<��H��� ���h�	���R��1_1��h�NDD���:z�(n����{7 �6`	�8��y=}I#O""����}�z������)�$a�ۀ���=BS�ԀSx�^�\�:�1��?x��d��믬�pFyf\+�p�Ҩ�m�"""��!p�g�c܇��������)HϬ�r �ϱ{� 5�P
N�h�XDDzeC܈S���`�~ ^	�o�<��tXR���E܃G�^����^��[|���X�qzpܽ��U,�k���!���E��|��S,�'I����W$8�r\;����ˡ�v�ҙ_�F�L��4Px�4m�xy�c܅qj�=�-X�]�'��.�����=qji�E����eS�f�	��g܉}e��5�����;�� ���Y'���%-�4l��$�1~�N�G������g�p�����F�ֿG�x��u�t����$�%8����i������#'#�?kH�K�l���2�x�ǀua)��R��ۦd.""IlN���;�&�����N�N����7�T�a�����kە�i�IDD��.8=?�1~�qzl��sB��ۍ-X���}pQ/��±�7��^}
Ԇ��O�ɰ��Px�nl�N;$8���At������'�a�L���
`����jh�ٿY��%c��4]t�Nû��$""��Xl��?� <���9�?�S�L��q,��K��}d����=mov��S�,9}���]�i�D�DD�����v	��3\pJ�����AH�;�U؄'c�n����thS^;���t�K��-c�*����-ph�.5
O""Үmq�i���)��+�I�ϣg�`t�X6�x?�EO��m����S_�N#;aLé�'�� ��ֵ?��S5M��'i�pk��Lp�����3�T�Jt$
�O���0��q�;��uQV6�%�_�����F��Z=�I`���иN��zjO�$��FDD&�#.8%��1pY'pSSq*0~M���)�Qo�rn��{Wd[�����m���n3��&m>g�������{Y� Sx�Vv��Y	�q����T*jW���-�1<\R�U���zP ���/47Bw�0����6;�։��Z���S�h�NDD���f&8�����:853њ�@��~g2��#����aG\��t��~,wc��]�r`y��5,�|�g�Otp�Ӏ3�w.X���2%�F�DD$�q�i�Ǹ��K��4�跚�k�����b�:�
Lǰ0���a1<���|�3�4�g�q�n�̅��wZ��&v�����'���	x^�c�x3�����&�-��o��|�i�X� �����k'��(<��H��p{�%	N? �֤RQ���TtZ��	P��Y�.ǅ��m���q#a�vV��C�IDD����	�q%�v`4�����=7�*X4s]p�}�6�vo�n?�_�]�h����8��S�B��'����q#U�����0�δM�}/Z�(Ma_��niƘ����}�JE��R��'y9��@����������q�����Z-�o⇈�H��wU]���m18�������Ì-�U�$��$"2�^��*n����.������]�QhJ���`ڋ���2���(�`��i��)<����q�)ɾm��EKv:���V��V�U�7IN�IDd����η	[
�4\&=����8�-�{�t@�IDdp��2%IpZ�A���,X�1&z�tѸ����"������:u�O�t/�=K��mZ<O�%i�wRD���G��t
N� ئ�]��)8�K�M�r{nĩ�=���'
N��}���mZ��y���^�4��DD��0��/M��F=4���Ӹ����3�H���5O""�t8n��I	��y���#)�hѸ�iI�F�DD��-�wI��B�)wڦ�?)<����1�7���8�D:�Hh���)<����[q[�$9K.@���i���)<����ۀo�,8��S����iɑ�H����T`q:�H�MK��DD��x\p�$8���L�I[J۴�:�C�]Z=&"R\'�L����>|9���'b�d�z�I�Ɠ�wPD��N$yp�
NEu7ڦ%7�.�����g�V�dM۴�H�ID�X�����[��WR�H�mZr�5O""��a�&�ݲ���S��L۴�D#O""��Q�'x?
N��`�����MKN��+�O<1�~c�؟�mp��6�����?�w��H�>|.��k��tq:�H��v���K��3
O4w�܆�������1�0=�y���1E$��5׭��%�#}$ئe�6l���ޕ4�
b޼y�%	K	�={�l .�������4u
��ϯ��5єr�����juW�KґV20g���c��i��T[LӴq��v����	-�ott���m���nw�qG¿���a��R���R�Fr������`�v�a���1���n���?˖-K�́����fϞ=.���ΰ��7|v^g�98�s�&`?c�^�E��3�X�)�Bұc�G��F���sOn���d9ien��nՀ���)G������~��ݦeS���(py�c��S�4mׇ��q�M�Z�Je\�5�k�����1���1q#K�"r�Y�'�2�Z��j�T*�j5N8� �.]���PDZX�ۤ�[k����N9�O�MK�w��k�L�m,�}���j�����{pS�C��|��?���۲a��GF��J?�ʙ$NoE�i�h����G����F��^P�V/�V����R.c7�K��g��N�Zm��:���t��~�>��k�c��R���iɁ�S��3gθ�c�2M��.���w�q�#L�� ������FFFN!2�]��(����C	��xpu*I�$ަ�INiB�4q
}������>4EM��~�Z{���hp
F�4%�1�n��n����T$E��6-�FÓtF�)gA��[�cc���>=���y�2�|��9P�i��m����X\�JE��RڦE{�%��hЈ����p�M���4��ec�y@�I�S����
�����#��R�H���mZzQȠ藓����q
��FƘ��gYS�Xk�֋Y�����'"�漍��o�F��T*���ii�t`��2�r�&�c#k폭��ʴ�c�4�\NL��f�JD� _7�l߳���[R�Hʢ�~O�9���E��ܢϢ\Fz����o���;�q*�E�-S�'������=cv�t*gѢE���t�27�
ẅ�{�OB���Z� .8�7���x8 �5�z��,XݢcL�W�5�+��)<�	c�\\g�"Zh�y]��~EƩ �Jp�'���_�R�^4�XkmӢ���w+C-Q��Z��,kIِ��2k�� 0���F�d�U�K�w&8���+�JERVڦ%C�����}���׀�yג����5�$��v�G�c��שT$e�mZ2��f�N����o������ڗ�]�H��)�1~�JE2:���}W��	ߴMK{�rd���.̻�U<�[7�+�d����x��-��d`��xF�rd�y�}�u��Z{���}�A5\����5���D(��w������t�I�կ��x�����^��� �\���ݭ 8�_*ɠ�6-Qxʉ1f�y��Ƙ��1ӣ�i��$M�E�Z������'�?��T*��RC�ڦ%
O	�h�zr�wM=�pL�E�dd
p5nqw���?�R����z��ˀ�S�*�J���y֑�����I�#NI6�~ ؛�.3iF۴d@g�T*��p}��l/`X�IJl*pnĨ[��M�"(��~��iɉ�l��j�[�_�E1��WF�\�h���H�i���
N�P
۴�|�Y���]���#��P��� ~|��V[��>����ԨM��?�Z����c��i_p���:�!������w܈ս	�!g9����;���>ӻ��G��N8ᄸ_�;���Ƙ�����rˎ��p���_�?~�ʤ�z�l5�Y=��Y�dI�C���J�2����C��n����||�M��N��Sb��yԑ��a]p
F���
��
W�!�\:79����h����_p��T*�w'�'|�ߕHxZ�`�f ZPx�@pү�%��YOV*�ʋ7�|�cpSV���4%X>ё��n� �"�1���N���tC۴���S�|���iyב�M���	������}@ӡ���Va
W�o��&��^�;�� ���{R�1O ��̖w����>Pd��\pJ�Vq9.8�Hp�,[����h(<uH�)c��O��b~xg���㆓��2����[�V��J���*8c������m��r`h�)��?_b�o����&pn��Q�Hvr��J�I���wP��6)<���P÷ئy�/��&��ú�:Q>�;����@O�є`6�\n�À� ��N˕Y�{�1���?[��{\v�m�]N�m��8�L�F�	�g۴��Op�6-�����Qx���y����N�u�j5%�w֍$���U��NGc�M�����w�e�S�������w�Y������J�<�F1#����f̻��Զ�_/���f��ы
0� |��d��X?�������������|i��*�F'�p¸ϛLk�`�=�sh&E��Y���o��?��,X����#��L�Y�pa���� �h�Ƙ����}�{i"y�@��[Ý�g�SMvB���m�ШU�VS���eq������i����2���KƘv���jҤI��5�a뭷fFҸ    IDATٲe�^�z��� <���D�6-1���{��6���~��Rx�@�;���1
O�#ɔ�?�j/���܌c�gϞ�0�W�*`�x[�5�r�-9�裹��Y�������$��]��mZ�5���B�5�̍{l�L�>��S��]F�n�0є`��S�&>v�Tc���`�S\���m��;.�(=��>˖-�{�~�v\pz,��d���|Wpkl������mZ4mO#O�I�֖><�]BVZ�`X���%.X�x�Ձ�N���7��G(Dm�k�s�E���<��&M�ğ������qk�Z��ɐ�i�!��T��:�hJ�+��Rxje���-X������g�h�p�����.�!jc�(}����k/j�˗/�9�z�6N�ڦ�G���wܨ���.�W&M��ņ�y^��
X�B#N�l�P�ըT*� 5���+���{�m��c�/ O�����v�y��S��:ڦ�Gtv��������C#Ng�ʣ�,L�2�f̘��Z��llK:�ו�3����A;�1�Ђa�ܹ�]�n0-�y�?�"�}����=�۝z�� �r�=�ܼ��M�~��[:8�Op[����Fy�H�4ݥ�7<=|�^�=	״m�����W�}����y��w�����1EN 3j�ڷ���O��w�;w�F�$/ڦ�G��s�s�5y��\�j��3fD�_ú�w���mo��e����p�3n�Wi__�`�v�mY�b��T�V�544��^�VV�1���~X�%�H�MKoi�.#�'BSo��^�KA�S��{ f�j��UpՕ��Y'� \ml��wi��#ܕ=ڑ������+������[]�T^B��A����y�s�9y�%�I�� mw�7�\����OO^\��-R�bփ\�V��>]Y�&���$�N�j�)��y��.	�=��#�KܣӁ��`K�7QW�V-����y޾��!8�W�V�8��W�J����n����h�"�{�Px�I��N1�\�s9iY����[��(C'#Pq�Wki=%8���[����yo�ͦW�n����O���l�9��݀߁:5K��l���wڦ�=��dhΜ9ͦ3.7Ƽ%�zz`�����wz��\��E�5�7m>	Ŝ���jJpk�F#m��Y�柏>�h_�sꆵ�
��h�3���p\z.&<��c����߻yO'��N�6Ƽש���|��.���2.\��S�ÕJe�Z���1f;k�v@��i&E�ˌU�V��{SƘ7m�[3&���+��ݦe`Zt�O�)C�>'�xb܈����w)�h�*k���u0DT�Q�v�
Wi��hj�R����q5lfVת�pW�H����{.�2zū�j� �MyH�MK(<�����{����[MW��F�<�� �-E�\��n��`���E����4V�^���y����5O�'�MK��2�d�N8ᄱ�#�\?��������b�⸰�j�S&�A�)Ah��j[Jؕ�ģN�{��-�#�A۴�L��9�S�w����?4��eM�0�,����PK�,ɼ��,^ܰ�Z���Z;����b�ެC���?��C�nӏ�1�_�7::�E]�SE2(Rئ姸���h�i�����f��O�S�1�˲�NXk��yއ ?�ϋ�&��/���S
W��<o���ю�}ە}tt��~8�2�p1����jU�IzjΜ9q�a6��>��a�½1����x�ҥݖX

O9���Y_`:T�yo慵�c>���Y�8j�s��4k�������-Fq��+��U�x�'�~�<���:�ju�N7
O��O<��-�����~�(v��#4������b�|���*��W��Z{.0%��<l�y7p�;aGԠ�v��Z	���Cir�a`+�wd�IW���~���~���7� T��q�t�'I˜9s�}�=��Ϝ93���mZ�]׍pxZ�f͸�\x������@�!&8v.�ֶ}�Dڌ1׌���^�?��?���+�n���S
S�=����㏳z��n�Z8�juܾ�c��vۆǩ�t":�7m�;��ԧ����V�T�U�����T*S�T��W�XA�V{������T�T�ͳ�S�F�Z�Xk��n˲��}��z�w���( q����k��IܨU���D����裏6��-+k�K���r�-���GO^�?��$0{��;0���ah� k�~���-��`����j���;��)�3UP�7�
O}$:�����xc�'��{X�}��/cη�>����� U��"IcJ�mL	n�y��h�z�����Z!}���W��j�� :
O;�u%T�5[#�>���7�^�? �]<O�J�!�[kK=
���g�T��$�6j�1��z3�}H��c̵��K�k�������Sq��+{�`�y^��W�X�[`�T
�sƘ��gΜ���(ix�����Uq�x≱#���O<�0k�`����v�V[��Yp������>P��Ҫ�[oY�T�4i�g���f�+{&P�b���sp�1�V'�px-~-��Z0����+ ~�:a����F-�<y���Ƙ��
c̽�ݾ�[5�I��&<MD�?�7�o��1�|xq���_c����0�W�+���X�a���+3�J��3g�
�c��Z�R���}k�����CCC3W�X�P��
4�n��X�S�S������w_ԣ��&�l�0��8nK�p�����_}�2��^���(\e/�F�I��|x����7����8E���V����Sx*�fWV�G��==~���C� �x���.T��2Vh�f�N�Ax
'�� �dTV�6�l�v�G��8�`źPu�1�^�����d��d nw-jOW��H�����C�u5aq!�cƘ�q�	���F�@�?L�Zm�Zݏ�i����	 �Ni�L��%[1�>k�9%�Z�d�a��7oumR�QV�C��ve�����ӬM��LG�������1Kq�7�c�O�#+����}�e$e9�(<P�7^$8A��iC�I�Oe�O.��9��Z[��rCCCl��y��x�z�"4�U����c��'���̛7�ami���_�7�����!��q�o�����Px*��i�� {up��=�ZS$��}��~�~����z̘�eK����1T� �W�4���������A��4o�<��✺S���ʹ��܋;��^Y���b��Iau�����_N��ہ��CyQ���gF�x�`Q{��7-�l�g�r�Ŏ-X�����SܕJ%��o��L�Eug{�6^��>�ѣF��Qx*�D�I$#U�g�!y�KO혁��x��P�PW�Q��q!��4�L���B�����)�l�.����Z��ٳgzi��S�(<IQ\O��S�Ri�sSR��U��¸v�Z�>*��~W��+�m�*����j�@חm�� k������8�{�(<I_�a���y������L��˫�$W@l�z�p=��W���]��� <�7��rQ{=D�cL/���)c�|�����䎻ؠȣO���5����\p�cW����xC������u_6��v���0��1�8�u=j���`��{���9s7+}�a�9�������y��dͻ��(<����P�s���O��Ï?lC1�Z��dB��c�U��1t"�pU�t�)���>�9s
����S�(<Iߊ�}�[z��i���1�͸�ٶfcf�[k� �=�5���$Z_1�u;�@�ڛ�����+Wƍ:}(�t]�O��:�^�n�(<I�Xc�g����]HZ��?3�ܼr�JfΜ��u'�ffx�7�B�jn�jj�.�n�1a�.2h�0s�LV�t-����>����eC�d��?2W$
O"�sK�.m��ĕ��[�1�f\R/Ԭ���;�8��=��{u{��D��,`&.�H�{ee��Cx3u2j�٠�Dk�F��%
�C�c��Yĩ;��r�ȓ����c$DY��N�}�NcL��cΧyj��n�c�W���A�
n;���K2I�1�C}�Љ̚5+�M��T�61����Th
O��$}/��kw�m7��xE���S��<�@���mK)\�Ǻ���-Œd&j�w�`t+lw��R��G=<y�N�\��h�ν�;�̙3����}����FTt�E����+`5��]Mƅ��P�=�-��mQ�^1�f͚u>�Ӛ5kr(33o �7ƴ���w
O��$ErpP�ֲl�2�<�H6�`���ꈵ��F~&Z�4���i��6=G�p5	؂z�2�̌\1�v�MԎ�l��1{��]H
O��$Eq�-B��V�^͵�^�G��)S�3�%q�V*����Y�ZC�p�v)�}l{��ۏHx�;w.�w^N�tN���E�I�`�{�Q�q�z�)���:=��"t�>X�h����C����g��������ju�මѪ�5.�	nK�y�3��Rx*�⮾�A�*\pZ��y����j=�P�M��]e�֞e��D�y� ��~��vhh�ʺ}�~��aj�PW����Y�ڝ���\��w�ʥ��4oo���7T?����'�ફ���d�M7�}e�[�\���Ԓ%Kr+p"Y�����N/jǰΠ�'`S`#��˻�n)<����� 7�.�n˳�>��W_͞{��K_���U־��c���R\�*�.�,G�2jǰ9�O�[yQx�7Px��Qx��<�ظ�'���/~�����gw�}�� ;M������~����Vq������3�8#���vyO�f��Ma)<�K!�]�@�
���1Z`p!��Z�c� Yl]a�k}���1���Q&��y�.K��zꄏQ;�x�4��$�D�v�O� ����w��o��6v�m�5�g����}7���*p��yg���;`�b� 5#Ni*X;�aܛ �cH�1�/F����rQx�~�	n�n��8�k��4�9�����1�'��a�Z{�1曾�����6
F��T]����(��4^1�����y��N���T�5$��T.
O�6�^����|�����Z[���*Ƙ�|�����/���kW^�]:���S�G�^7}���
*�&E�t��v���-�1Rx�T*
O�7�$o����	����[= ���Sn���i�Je�1fU�Z}x��"��j����چ�ꤿ�����m_�V��V\�}���kHB�\�$OS���W&8�W���}aɒ%̞=���	ޭ�V*�ǩ�*�G�:9N���}�4ׯ�V�]�`��ǀG�."	��rQx��� W �$8��q�����R�J��"�V**x�F�K;�)�E������ޕ�A�_���
O��$y.ސ��'2����:�9s�$x��<��"p�PZW���i=�>�;�C�λ����E�I�V.�Hp����-�n[��wO:餖����9�N^R�m턫pp���>܉�ʨ��1�F�+Z�P��rQx�,\+��%8��秺�׹瞛��DR7�|�̙��cL��Swu�8E�-�Р���5�<!�1n��5)H��EoW�]S/cn�Ȼ����E#O����w뗸�:�rD�����/��d\Jf|߿��}·"Rx*�'��B\��n���."u�}-pq޵�����]D��E�Iz�C@땰��8�P7o�Al.r�|s�c̪hO�V�+��rQx�^:�R���8x4�rDʧ��֖�R�g��
>1�z;��rQx�^ypv����L��r������J�F�Z�yk��EkIЌ�S�(<I/,������U�HY,X��Y�f�}�k�'���G�
|.�$<MY����\�$mGߤ�����I�"��o�$@���^;:::�UM=`���1�A�a6���rQx�4 |��J7� ��ZE"%�p��J��3f����p�2�|�7�|l�S�c��_D�(<IZ^��6er��
8�mj��D0��b������CCCl�������*���f��4�'�C`j��_������-N��UL�xp-0-�c���L�>=��R����d������k�
O�R�x/Y���{�.��x1{W���'�P��k��0U>u�T�M���_=T�T��a���ӏQ���h�N���6a�.�?
����hp
��)�Whq^�`��`�zP]��T*���M�c
��)����E�I���ج��׀wW�V�H	�q��>�V�чT��_�s�iӦa�ᩧ���+ׂ�����c��Y�n�q4�T.
Oҍ����k��`���+�D�n���=d��6�S`�ԩl���T*�nK�K�����Zm��`\��tjS�T�v]>���i,M�"�X�h���hhhl�g�}��n�?22�&�l����C�W��ǌ1��F���P�w^ƥ���S�(<I'������mb*"uA� 8�L�m��=(��x����VƘ#Ⱦ��.�<��7�F����s�ͮ�(<���k:p���88+�rD�!ڊ &@m�����/u��=�yU��NƘ���n��1���_�m�W����'Ђ�Qx�vL���}y�c|X�N9"�p�±}ۂ�ґ?_X�ծǭ3�5�|�Z���Ƙ��%�����c��Hkaԃ�e�J�B�����?�~ ���bw���S�(<�D���Np��N��r����^�������nU�1���G�bXk���O�Z�U�T3��ۜ���i�Ƙ۬�� 7?�]U��ycSr�julW��[���Qx\
O�g��K�[��N9"����^o��_�o>���B}Ԣ������-V K�7|��lhh�����Yk7�<o}����<�����<�>c�]���XӬ���aFGG�/���f�ʥ|׃JZ*�7���J����"�"��)��'���7�o���yo���,���j����ۭ���ym��
zW-]:8�*<����$��[qL�c\�h��'2��[�����g��⬕�!��ͦ�f͚�<�� ��N4{���˃e\>��rQx�(��#�1nކ�~EDh��J=@�b��l��R�T���	g���O?��?>��M,�*�b�v(<��D}����?�<�N9"��,4�T��_�u�O�v�V'��T*�j5�T� .��q8�o�\M7�:��T.
O�i�	��+�����ŋǮ'
�M��5�K�
	<U�ըT*�����<� �Aʚ�d����w�Nܻާ�)G�؂�VZ�'7�1v��t�M�S�A`^���	�����S�(<	���	�7nۈ��)G�آ]�cT����	_�b�-�L��TS��c
O��$�_J��{p}���N9"�4��X�	P���N�Rg����Z��N;-��H��\��q�fx����8�XOd�#<��`�QpE]�RIe����کE�����Rx*�7��h�pSuK�"���S���n�����VRp�?
O���`�2��y~��+��D
,:����G��V������Z��h�S�h�n��K|R��׽���U$R`���A����o�%Ypz���q۫Dk����T.
O��ո�4���ވk�'2��[�qj���
����y�?��U-�_��E�ip��0���~�ZE"�j� c�)��Hv���7���sw\�Rp�Z�T.
O���M��.���I��*)�6����~�p�n�nE����<|���*�݊�/�<���S�� �ظ��׀w�F�DZ�iw1F���#`/Bm@��Q�(������Rx*��q]�7���>�n���*)�6��T�
�I\�7U>NxJۭ�F�ʣ�6
OŲ%n��m�|�N½�hA��f�Fx��K���	N�SWL
O��T^��~�o��.H���
�pj"�n�5	_�lུ�n%\�����Cᩜ6�^�����N9"�լkx�����)�nY���Z=H�����C���g܈�n	���L:�W0U׬k8����&	^�f��c���Ճ��O�v�K#O�m���[    IDAT
�}�	��_�.ߑ�+1]��n&YpZ\�����T��+��J�u	�q�/�)��v+�P�:�Z{-n��[Ox�w �]u
O��Tø���*�K�ѿ��fۭ�1'�~޺���!`?�'�>8��C��*��sx�c|8��Id`M��
p�����n厸�����S�h�xy(<��ޚ���`�K�E���3�����h�!�Z��#	_jl�����mW��I�<��� � �Op����p�VERAeX�뿔ď�#�'�_P����i��Px*����	��\pz.�rD�gѢE@�H���p���V)�M�R�ǭIlNu/?���Px*�3qݿ�up�L:�O0��7����[yC�x3-�[	�#��T
O��\��n�p �x:�OT��Cj&n_��&|������V�y*��b� ɶL�p�h:�O4�Dl���n�q�!��Xc�'��guR���F�D��^\��n�8�_Fd ۭD�F�^�[�$8Հ��Y�.ay*�<�1�R�ߋ�`_��i$R4AX��� ����"Y��5ƘwZk�l����Ӏ��Sy(<�����p�0��/�T��R�H�`�ۭČ��d��\��+c��k�E��SA���*<����o��h��ˣ��ZE"�l��P�y/p9ɶ[���y�n�_]�%��T
O�k/ܶ)��|�S�����*)���[1Ɯ|�d�Q�T*{�k�O����O?=�KHh�Sy(<��W�S�|�*���oR�H�@��n���G�������>�m
o��yRp��S�(<��]q�i�.��8����is��I�~�<�0�G��$��T
O�e'�F�3�|�(.8]�ZE"��v+W�K�R� �����I�4mWP###ѻ�ڵk;9��S�<Xl���k�q�_�"'��J��e84�{S�}�w�p<��
�j��!wZ��1�v�i	_F�F᩠b��F���V���-!�a�9��S�H�@.\��6�[x�f�Z���>��`���SOM�2RF
O"��7�m�Ϸ�<���tF}z��
�X��j�m]�i:iE᩠"C� &�*�V4�MH�K��t�)��v+1���_l��ejƘ���KZ=H�I&��TPq��vx���L�-�~Q�c�?���#R,��J(@�K
ۭ ��h��K�ZD��
J#O}c܈��	�����)G�X�ۭ�m�k�9�Z�-�u��fk�ڧNҠ�TPy�S���W&8�WHޣF���[���Xk���\�O�P�A`�P
N�	���
.�1�B�6)<%3\�k�׭�>�J5"�j��z�:�Z�:cvp0pw40�k�$��TP1AI#O��m>��Ǹ8�;Ȁic��Xk�>����Nw�(�8I7�
J#O�� G$8���w��a��ŋS���o_.�5�L�׸��G�_Ј��A᩠4���=�1n �t�tE�.�T*��$�2��	_����M�ci�I�Rx*(�<e� _NLp���NkR�H� -Z����j5*�J\�����^	_�p{ݵ��X�I����"����w�~��Fx6�rD�!h~�y�t��J�<ds\g������Qp�h䩠4m������ݺ��N��b��p
�Y	�@mW��n�m���Y�'کE$
Oz�0,ČRxjχ��	��� ��t�)�`�� 8��� ���L�s����VRp��)<TLP��S�N����"��2�J��R(@����U�������.�rm�"YSx*(�<��{��<�~�@`e:�Ct�.&@��Z�m`�/�p4��U m�"�Rx*(�<����R����A`?\wc���l��P�z7p!��=����n��V$+
O����9�&��l<����'��D
`��V�1��~�dۭ��R�\�ՖG�P�Kɂ�SAi�' ���~�O �අg�y���6-��~�x�n%�'*��$
O���Խ�m��.����oS�H��-^�x��T��p�8&�k�T�#�m�k�dI᩠4�=�뀩]>n��_�V�H�����c0����ѧI���G'|���7���$K
O��ȓ8� ��w����[��V�H��Jb�
������	_�2��t��N;-�ˈtF۳.�<���K�7�����1�S�H��ۭ�7m��wm��n%ip�p-�[��j
N��<�����~�.�_����:��D�\��=�B#P��j��$|�	�[M�I~�
*2D`B���1��iK�F���'�Z���SL�z	n��	^��n�yS
M�7����	JyjϦ��]�Ϸ�_�KR�H��5+� �:�(l��Vp�t���,)<�F���<�`���$pN:����v+QƘ4�[y�s����vjɛ�SAi�c�qS
/Np��qk1DB��VB���^H��e�<�pk�/������+(��7��� �^��_�S�H�k��J����M���J���A(��Rp�~������S���!�e�_>�N9"����VNǍ�&�硡�����#N�?A�I���SAi�S[F�ˁ��b���)G���T*����	_�7���#͂�6��~�i���}?z���x�R�~Xݺ�'WG�T��-r��ё���Y�I�����[���T�U`�T]�O'�g
ORFn���Ǹ
x;PM�"�>�`D;f�nn��-	_�q#Nc�������'|��Rx*(-o��ޙ�7o�Ŷ"e���	P�?H�R���ך��JۭHh�SAi�xS�f'x�ρ����)G��)����y�����趸�{��J�Z��p)���҂�X�>�����M����p��q=�bԋ}߿�dۭXc�{�.IU�{���{�ad (�9\'2�$A��B !x$(^=!� 1F�1�a��Q������ B1
"f 9.#d�g��u�X]���ս�����z?����]����==�o��j-�Ik�:�H�I�F�)�����B�������"y��褔� �g���)�a�q�gߠ�/%o�2J-OM>N�	,W � �ݛrD�Wt��� �.�F���ڄ��v�	�"�5
O�����:v	L�i�<P/�����n��P�9��d���ޅ?��8
N�i:�.�t��V`�����s�;�zV�Ȑ�l�c��pS|$	N�<�;�Pp�9���d�Z�2J-O�������ո��L�*BS*�Zn�	�X`ep�^��V�IrA-OU�c���>���{�ɭŋOv������W����+��3.S�IrC�I��(��R�X���౞U$2���R.���1�7�͋��V~?::
4Z��J�I�D�)�
���[p�i�.�8x�g�� �3w��Y�m�I	u3n������ ��$y��Q�����7�n��� ��g��hP�	P;?���K�k��� ����w��֩�<Ҁ�*؀���֛�����q߰��YE"C�]O�R�\.S.���T*?�Ix�I�[����Ox���4���c6Vq���>L�i�=���y��znm�ng:��4�~�*B�u�U*���;���� |>ng���n:�;u�	;���8�c��o���c�.�q�������5_R�'���]��>|�g���Nv�7���q�d�������S������e.�΂���Ƙ��S=8ũ�t5�:�������87��Hn�שk�h�G��f�1fp�Tj�;u��= nc�;�t��}���M����ۋ�we�:������Ra�4���m�Ze�<�D�>y���&�Ҧ�o0�9�g\W��	�S��ޔ#2�&[��Rt=ZnE��8�.k!���;C�{�D���_ȅ���2þ�b���m:uL�Gye��7��{�y�,��Y�ϝ5C0tʋ�������nޘ�<�����)Gd8Ņ����p��"�zV��E�M��w�E$��������3�5��J��Ѹ7�=�R`C�5p>w�ᱵ���/�1m���]װ=C�~X��hɵNa�pε��K��<]*���V�O�<���BSxꓖ�� x�O8�a9xm�j����}X�|����J�i�F�
�8lZ�j�E�	�/2Ԣ˭s8����Lx�GGG��V���V��^	��']�E$��z��̴���}�מ��Ӂ?�[a�l�p��pǽc[�GRD���G������p-lݺ�fi�*�)[�hQˠ�Hx�ޗ�P� �FGG�C#8E�Z����z�@c�P <ڠ���' � o%[�����/by��Z� 7��
5܄�ڭ뀿���ɅK.�����Y���$<�-�/s[�Y¡5@)8I�e�C{h�tѵPx���N��MX����7�'CK�O��T��?A7��?I|ɦ���p��T*;�&�}s�C]�A��R\����F$�������C����p*p.�N�F[p]f�+�Ć'\	����w��&x���`������s�J�r'07�b�[	�'@˭��(<u��l�8���a��W,���a���f"�� _�Jp����$x���p���mm��~�j�N`���ƘOYk?��F�i�ƻ`���n���K��q��L,cy8v��^F��� 0'ɩ`��� ���T��{I�*ƘӀ�uZ�@�I����4��7K��~|��S�|�׺�G��#X���X����[�7��H�D�JL�:����˭l�uy���q:�""N�>�i<s�ٻ��5�s-�N�ϓ�0|��ڔt���#�ہW��&2|�a%fj�'˭x�w����w8���H
OS�v�� u��c@ee����YO��&�/$x�'qh�أ�D�J��Vj>j��	�[�<�8�j��wM~)2	��I�i�.�o]��ϒ��`�ݻN~�����1V����(�!1��V~W*����6-�P
N"�Sx�����i���U��$��0VW�]+�cӺ�s�����R�H���5V�B�, �Y*�VC�¾A�RW���(<�a�1U\\���
:Ⱥ�n�'=wLm��j���~�$��E��,y1
\K�Id~���Trg�D�Ei=m����v����T߳n�'�i�փ[����`қ���Wp�ܹ�K�F���-��Y���=��Ԃ4f
�sRp����s��@��p�]������n :�\��d���a���m���z����\���y^�hG�tG$<�7�Sh��k-��a�Qp��SD�<NP{c��s�����|x�y�f�<N�Q�H?�_Zk��9�]��2�ctX,�Z���"]Ҙ��?��Hc���0�O�}YS�Y����k��ݸo�"��y晱��˞�g�OHץ�dɒ�k	�8��<�^�[n�\k�g'�ED���Tc�]�>+��{��a�%Ƅ�y+�ܾ���H�ɶv�)l��)8-jٲeS�)n��H�:�������ƫ�1W�?J�I$9������gfpnfk铊eK�p8���@���TJ�9�3Z��:qz��BT\�S����-�v��V���7�<~S-"���1�)���o X1Ŷ�2o �SE�R�4ʒa5�����Ы�T�Vc�}k'��[�����,�ԝ����O���Sp�'����S1\=�jý�Q�Zt
'Px���Oo��px2�DC��-\���v_`/c̶��m���MƘ�ﭵ�K��r�j��i:��Z˕W^�q��Z�|�dSƼ�[0���40\����	Z��0�7���g�OG�S@���ϟ�ҍne
��cv�e�=�y�������s7�k��N7Oz����{���3�1�S�n�'��+txjY����~4���p��hp�����6E�Z��Ƙ��	"G;�o��u�_��.���6����ޛ�����9���u<
���F
N"�Q����q�?���S�<,�b�5ng�1j�[q�)fL�N2�<d���Go����E�?�Z�S���$N?��n��$��'�H.�<!��h�REҰ�z,{� +�T�kq�}?����|����=p>���D&.��)�###��mqK�| �qoōqZ��F
N"�U����r��U<F��J
%I��q�q;�v)䫶�&�ʞ�]�yރƘ�����1W ?�F�����qk5&q-n��X�)8���>� |J����i�"-�̨w��^�V�R�6��;n��� :;�`c�T*�*����y�v�u��5�!���w�X
N"���N��q�ă��=��k)q~:UI�`�{�RE}	y��y�3����i;k����?�ibbb.p'��´�pxy�)4�V�ζ���0��|3�y/H�b3�}�g��d��S��W�t!Gz��tF��<��]w��P.�b�9�Z��N7Rp�B�'{�s5B���`�G�|�XK�EYS�M0kx���f�����j3g�d�v����![���17�֩6��)�������T��v�_�qN��E�#8��;�@pc�ƍ��u��y� n�����$���'CsL��8!�jd궡�'(C�M[�7q��{����y�;�SUw6l���XǓ�^�-B~_təp�RpIWa�S����9�=w6�S�EH�}����#� אl�uj֭[G���$��3�R�0����P
N"�+vh(���.C�l�w�k�VDɕP�:8>�R�}�5kִ�T�h�\�s��R�T�4(-�+2���ڥZ��F�kȸӁR44�R�Ez$$>22����w�9��zfbb�͛7��}/n��烮�� �'��Q��d��lp�Op������`8*�"��FFF0�`�����sjgÆT��yR�7�x}��� u�E�H��B��X�À��.C����z+b�d�d^�Zm
�ry�1&7_p|�gӦM�M�略�Jx\T8@��Id�'<]u~���t��N��]K�]��Q2!<P\��}���#g�Q�6m
���V&���Å^8��Dd:r?��D�ۓ[X�U�o|�M�⦗�6�B�?���J��^�ֲnݺ{fϞ���A�Z�D�[��S,�c��>�2$���S�U<$+��=�n����v�ϲ������ƌ1���y��v�D���)xn�Ѐ��;�o��!�Y�xq˶蘞��/���BT-@}��5��uƘ7Dw(8�dC1�S�D�ޞn!����F���� �u�T���y����@aAP*���S*���V�M��b��G$<�Z�*�jDd��� J�L���.Cz�đD�S�ϼ�|�a�S�I�.���/�\=2�Z��7v�p���(����%�U8
�K�o'�@qxr@W�u�D�ϰ���T*�)�>͙3'PEh>���6�BDd�r��8"��G,o���YF/��rn;`{�j?��gG�o?gΜ�W�^}�ׂ˲�1� ���"�1�O�{p�/}C:�H�������5@3q!'�l��Ӿ�*��h�A;=�1�p�վ(<�dR��S�B���n!�S��D�ӂ���L'����w�Ex����ُ�R�������i� "�)Nx�0���w�K�Т�_~�'~
��t[y��f�p(ր�g��nʌ1;�]��t'�����7�����.<i�0�呸��wsE�ϰ��^)Rxf�]��t'������Y?+KZ��z s�\yS��h�����;<��k�S��C����HscrJ�H^�;<�zu�H�m���Oo���+o��|�ߐv"ҝ!"�G��f��Qٚv	�#��﷤0c�cወdC�ߩl�E�)�F+�i� =R*��.a�V�]��t�h�v
O94C�)7<�k��=Ϭ�O�]��t�h�I���脺��\.3>��@\*���]��tG�I2O-OCi0V��&�2l1Ƭ�֮	~z��fbb���i> �G�.BD�S��S�s�j�=t/���ja�) y�����5�+@�����yx���W�V'�p�Dr�h�I����Ȍ�K&q-=M(��3l	�դ�%����X�0�V�f��+�=�U�3E��h�ic�H�m-�N~���u7�����V75lF�Z�*��k���t+�����D7��&�  �IDAT*<�dG�������������r#4�ѵ�ڼ������,Y�B)"ҍ|���9�6f�{�����)��\Y�`�����<�̸ͷ/����tc�
�9U�Ք��n�;<�N�!&PI�Y`�<�+`m���¸�S���ҳ�������mX�ɃO/,]�4.@m5�|�Z�/}/`���=��%�M�O�4�o���q�t�722���D*�>}�U���.�W������'�,�wxjmeҤt�����^�L����rzze�1f���Ki�#�W�g�]u�Ui�$"]�u'�ݿe��J�韯ZTx�Uk-��a�i��?��d��Z[�	�]BD��{m�>����p9�f�1o �^;��t�Ҵ��.���
�0�\��B1��ċ�����]0�������.�����%=S5�̷��dvN
c�ר'ht�����$�A�M-O���Y`��#�����汔*��ڴ>a�=��`�陇�1o������}����J�$�?b�����S�����t�\�u�^a��9�Z����>b���$�Q�O��r�I=�W�EH�D���1f�|�s_
euk���	<�v!"�[�O�ą�{R-Jz�.<r�*.�Pp
6��I��� >��n'�uɮ"~��M��� ��K�.�N Xk� �?|eS��3�%P
N"�V���;���k�S>-[�,.8���npYS�"pp{P� %"�U��W�]�<I�x'��r#�:
N������O �,�-k�+����m9(�:�d_��S�rG�Iܩ���w�EY�㋞��9���lp.p4�B��։�P�I\$r��Ӧ%�a�1�Tڱ|�ޒ�:��Ȳe�Z������/�?��-=(������Ƙz�\\���;D$�r�:�V�Hמ�}`JA��G$8*������U},�c��Z�g�{	-8^.7m(uՉ�K!���po���Pf%Pj�#��B`1��טߦS��g�4��:q�����	�)�1����S�7�oc�������W_݃C��0)rx�2?�1�r$K����״�Sx*���:��zb�c�"���Ƙ#��[k�
�fNr���>l����}��P\�p< ��QD��	��]��v
��_�$��ѲU�p�ϟ�t���P�m�����^[�Vg��13���u��Ƙ�fk-Ƙ��|�rL'�|+Vxi�<��)`ρ$ݱ��4��d�H�I_Е�,<As/cZ������w�V�H1=<|���F���?�i��+<IkKTX����Sx
߷]x
��I�X
� 쁱�_��`��V#�f� p#�<���ą�^����%K��*"U���x�<�t���t���?мƅ��N2�� ��T��;ND�*<A-@��Gn�;{k��ˑ�;�h~�*<��Ȁy�̰��������ͯ�3���$""�W��'���Ԫ<4�bd2á��j��GS�IDD
L-O�o��ԓ4]M��i;'IC!Óy����1�q52^a����.EDD�)dx�5Z��p.��)W#�?/ׯ�BT� ,""�W�O�� 45���P�]�n�j3����Hj
����-X��r%E�,nj`#Jc�DD$M�O-]?[� |
�� � &�x?�4�:�%""C���I88���x| �ʺ�����u�ƫt̊4�i(�<OQv��/嘝>	܄�T�_7ax/nn�f�����pP�`'>F��go���$�d���6�)2\_�IDD���SM�2~ӵe�E+�h��0��� 8�|GB
Oq�ح��+[H!<�X4Z C�<�BU"""mh�SD�u�pp�@�ɿUT9�2������]'""CF-O�|X��)��RL��8XA%f�Qp���c�m|8o ��ӯ(���9�� ��0EDdX)<�1�V��p�`ǵ�H{wG����M�F
OL!@]�X��Zr�*�8`}}KL�R����3��I��	� ����d�F,ga�V���U�C�DDdة�i
����7��24;Q�o���V�g���$""٠��i�BXލ���N��g�Y,K1�����D^u
M""�%jy��I?�]�����a����̿������mN�IDD�F-O]�m�����S,_�sI�b3��b�g|Ɓ����%qDDD2@�)�z��N��2�������+�6%���� p �U��$""Y���?fc�3��fa8�?s�]׀����\��C����ʘ ��$""Y���#����� �7�i`������U����H�>��	0��8�~Rx�!{`tC�����XN��a�h�g=V��p������'B��(4饈���S�(o�:�n��%�|��]�4�n��Z<~���kǽ @�4����������S�m��Q� rp,�#�C�Y�����Ï��l�w͕h�?��	uщ�H~)<@KKT\��vڹ}#�����▂���Z`� �{�p#�4���g�
��ԭ�&�;��j93o��g&0�>���0�,�}�b١v�1`#�X��0��e9+�<�奦cVj��������ZU�]��HQ(<����q�ᩱ?��34����7n�GkX8<��~��9��bRxJY˸�����H��S�x��Spw�9'""��4d���0&<wY1ŚDDD
@�)ڮ�����V˒��������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������H��R0f�t<    IEND�B`�           [remap]

importer="texture"
type="StreamTexture"
path="res://.import/AstarPathing.png-44dac335c3cf2c2f71dac3fd3f426d0f.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://icons/AstarPathing.png"
dest_files=[ "res://.import/AstarPathing.png-44dac335c3cf2c2f71dac3fd3f426d0f.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
  GDST�   �           �%  PNG �PNG

   IHDR   �   �   �Iī    IDATx��y�E����3���\��MvCvs�F�G9$��1�<������M@�>xA9�#
�Gy�A$�d7hv�E6�M�;3]�?�k������������LWWw��|��[U����`0��`0��`0��`0��`0eD�`B�{y~P�R���P<K�,�] 2`� �Y�wC�*#xC�\��v�����\Z�
�ސ��"}����N�%���}�)���co�·}�-λ[��s��G#��%	~*�âJ[F��`�&��Z\����AX���`��s��#� �$y�x��$vMȢ7�7d�x���M���3!x[�W�I�_��F���r�?.��9��w*Ð`J��m��ѵ4p>�B�QD�-Q#�8�(!��n����=��%\% S��,p�cλ��o��_"86����\��T�{�+W}�V�,�a��7F���p޷s(�<Q����~�@��C4�{f�͛ia�Щ�S��$ශb?x����G:�1祅o3��V�1�"J/�׌}6p8e�ޘ4�+�g����u�F`�\| �'xT���ّ\Oڊϖ�p�Fa��"�9�	a	]coP$���p8��r#)����c�3���=AO�1����Ң���Y�؋�����0Oio�$�qޟu5Į���e�u�5˼��A��EH�5	�ʙ?l����"��j�z`�~��%�4�8G�u)#x��w�U�n�Z^r��#���C��CC������y+i��:o,G�����gJ�=�r59�T$Ƥ1�	M\!bo	����IzUY�f��� ���$$0ƉLo��S��i/��05�pg]FJO�3T+��&4�����)k���y-��wvf����$	�TI�纙�y(����g�?g�[�(gNr�Luܛr ӯ1�7��l�ɚ��|n��瀮�.eoPNc�����6�/k�0k����p��w�L�Arm�c�E.�\B��V{�)|�h5�O|ۓx��&s�E_ن����!]g���g?� b�X�H� 9��DPV2c������x�&ex�"�>!#x�����IG��?���W�o��3��zɴAʌ/io�|��v�?���Z��*�a+}�F:�+��»�	�g 8����� l�i�y7������=)��9�9�^ B�j!#eq#x���~�6�ľ1�҂d��Q�$q~��9��6����E��p.fL������:����FM����r`w9�S?��Mo��p���y/Å �[r�rjP�-�NT͞�1�P�U��f&r�|�q6����ƣg�m�k��Z�� X�ϕ�����w�8A�O�,b��7 '��W>-v�D�[�`<��M-(˻I7��<A��V6�|��c�>`�m۞Ȗ�?{��po�_23�:���>���UT��ςΰ9 M�pW�CW6�x���l۞��gB����Ë���+(�=�صK�؋@J���yϩ�x���R�.��+�u��=��PP��tL�����zWFގR(�fx�Y�:�b�;p�LھϷ�_�>x�W��~���۾��P�-e���a���z��r�B�U5��fhs	�beƔ'���҄
!�YG*5��/�\�D7\�@���M7�T����|��/�y���5���m����#�Hj�W)廀��.G��d��v�����3j���?���~:���\�K{%���/A=/;��g����+����C-G�?�| ���hii)���>��zk��"\
|՗�j��/�Z�*Ȏνq衇f���T��2hl�B�^u���;w. �-
�L�!+��S���&b���xg3��6eæ�5�ҥKټ�ۙ������G ��U-�A�nԟv��X-���
x��������[�!N���0�I�D��~��P����?��\���Z���.C�Q����V�2`s��.\��7�n���@�.n�@U���k���~^1@�h��,��]�HUM�u�����Jkk�;�E����@D�#�<�s��S�;|�L��U�:�����A�m�u�����JKK˸�����ZW(�	#F�x�����`�v�����3���_ڟQ5{���gժU)������AU�t�R϶��!��B��k��E"��"�Ȅ����·�����	� �f����|��F_�~�u��}ࠃ��ޖR����������D��p�9k�U�V�^���rG)%�r�-AY��rp��4ʜ�Te:��o	!FCj�TM����s�B�f���Ŗ�J)����O$������%�k��v�c���Gb�D" 6�{� B�6`���b�X�j��(O= ��Ɨo���$��0
��^,�X�paI�q�<��S��������<
|�\�O��s���[��^�[Jy0��J]�V����
�݃�����^~��|�r,��:(�m���O �H�L�"�� ����3fO)'$5\=������[�drX����}̘1�FͶ߄���|�D�W�(��-�⩧�b۶mL�4)H���|�I�vԀ�T��.�;�`p�7��RI��⊠ߨ���U|��C{�:��i�4�X,���/�܄�!^u޻P7�F�0
�͒ џ#��:��;d�����3=�����>�,�dj�!�#�	��;/��C|H%/V�t�ò��u?��B��c<8�.�7�&�7F���'Uss3[�lѢ�$��b���� �D�8��N���?�޽{@E�
b�%��$�hw�߉�.�Hs�l�פ��8B,
����))��R'8�n!K�"��A=	�ȼ16��Ѧ��)*��#��[ZZ��Mss3��vZ�]w�u.CG��p�rB�QR���9t�.x�X�Z"��������ӥ��R�����R�v˲f 3�B�	!��bn��&�2$��](�oD����� ����3"����i�~�	&�\�x���;wz-����!c�Ц{Aq�k�]�����\�*��/^�B�
L*���d�$���S�z��H�6��ض}����K)g9��v熨D���N���d2ٙL&7�ر�b���!;v�x'>'���v���P
])t����u�eY��R�U(��z���ի)�����:=��C	.{�칾��>e�E���Q�#�z[�m�%~����BPl�����gǎt�_����͛7��;��#������S|��~w��t�R��xj�'�9�e�n`)�ݨv
���z�����p����T-�ޒ%K� ˲.�,�Q�&��_���������w?>�	ޟ�/D!�H)e05�І�9gmzh<�v���/����w�=�Q�Ffr��A���񄰧ԁ�����=��o	!�B4:�� �r�2?eʔ�k��N�vj��'���H��{1{��WW������~���Ѣ/�Bf�H)�P�-^ʒe��r0u3� f:�R;Ί���``�=��&�bтt���qC�I����b�www3q��(�ɶ��;Ռ��޹�{)T5��\����}G"��D"�8��A���޻w�w6�����H�^G2X�bE���jx��T×�k�ݶ���۷�d��̲O�=�X�o�F����k�l]�ٞ~����qb�X���Z�.�1u�-�tX��o�����MJypsOOO�q�{1�|M�;����֙�h��d2��w˖-[>�w��ohh��E�ӟ������um�f�֭�|��FO��R������N_s^]�d�؞O���%��[��}zZ�O���
y<?�����@WP�>h�;�/�i�R��L��F��TOOOJ�)��cԄ1����;��#�HꂉD"477��OJ�o���&�XH'ГO����ޕ�B�K�ۤ�Ǒe
`���I|x�i�E(x����Rhї�MU*��RJOw�a˲�)�ڍ��45�Y�(�끴�8O	'�5˲d.����7�K��Y���Q��)�
�n��aP^�p�Bwͮ�*x�[��1�6�|bh�,�U�տ#�?_�;�����IB�6)�4����D	��_^��㝱X����xM�j�f|ww7B�1����,�-/Rʯ�9�q�<֔y?f-�6�z{Sm�ύ?��R�'������c������m�Z�m(G��Θ�4���!��K�%�MBm*u�6��97��g9}\)m����$�B_�ؒC�o�ѽ9Xt�I'1sf�5Rʍ�Ž��466�ҝv�]��	۶��3�AJ�&���G��ܿ��V=����{KKK�ƭ[��t���10BJ�A��R6��c���{�1"�ENZY���s:.����ւ��b����,��;�Hz,A�x&�]��F��YJ��{�=a�`?�	�8� Ԛ5k8�~6k֬�����?B|xC'������R���{�9�ȴc�i:j0m�ӆhC9�M�u~��x9ݾ�C_I�W��T��BJ�E��͛7O�<y�',˚/��LFMJ�<(���b�p�|��T�r(�)�5Y�,YFڶ=e"�L%)�4`���d�P���|e�~�kº�J��Gy����oC�\��dS�L�b�6ԿbŊ��xC�����{�,M�4�̙�PF�#�����K��y ������zrClݺ����.���#��`?����haE����l�׽����X\��B�_h�՗��*	#����!?�K���0.P���|O�|7Dgg'�	z{=*R�8(�oɿ?�_��x/%�|�\M悎w K�������e��bŊ�ޘ^Pmۦ�&tzlҤI[w��=N�O���.\>hw�b�?hV "\���Ž��@E���О�|0�a�,�}�̙3f�A#Gz��E���Ϡ̆�|x|	X�K[��3��صpD���ӧ�	5C��c�2f�(<��R����	(��3��� _��=�
��_��TOO��>��GET��#G��l�\)�d����]�mBe]�a
=�|.@9u�*����1/_�J��"���-�:ղ�_���q۶?%����>%�<ڙ8?FJ�c����e=j�V�H�h�3U��%�������j�^���2H%��˗�H$\zHq��f�n�{��Y�e=�R�F������a�}1��t>��o�2*���G��M�����
!~�W�;����tON[[[�keԊF�%-`gj��8�����6��C�Z�U�O~R-������Tm��B�Y�~�gN>���SQlۦ���Z�>��0�/��_��d4�@��l<b�`����ֲ,l�֢�������A����<�z�R=7d7}�0�-�ǿ�Hv7¶�j��Z�ttt��Օ1}�����ӧc�v]4�i$���8)��]]]�[�������/����FC36|�
< ����@������
\v��N�d��A�Q�e�&@쏢VTO�xz�����3�/�w�zƹ�v�l�kR�*p�*���� ���#B�����m�n��T�E_�8�F����1uG9�C��>U�U��M� �8cƌ?!��>n``�&�R�~w~7MMM�sO��JJ��𹙆Z8�=J���؟jR�*��с��H$�+�dR��}��!�T�	��,�H�h4��),�ڳU�>;��ߡ&4kP>�ԤDU���E�GE��ߢ"0h���o�`�z+)e���U�I�D��g�Ғ�y���ϛ�l~1:6����h��xŞ�R.D���ӣ�R�A!v05|M�U���l`!j~�$�t?WM�eY���k��9B���ǹk��}�k*u���(��1w( �Zx����|3��Y�u;^������ܗm�?�*���������_���� E��9&b/B|�V^߹s�<�zq�����Ƙ4�:�.�@���>cd�� tuu�/�׫b�	!��p7tvv���ԴaϞ=466z"
�������n��Q%n��k��P����H�wn��]J�\gg��=uo���������~��b����y(�M]�k��0RJy��\�~)��y��:-H��X-L����TW�����U�8�gٲe ̛7�=0^�Ʋ��m۾�F����[�W]uU�%��,�kQ�3��,E��)tO�6=D�
<.��4����-��Rʽ�*����w��
�:|���$��X@�3������ �ϙ>}��`�)��� ��d��<-]�e8��,s^nD�Om!�������J�ge޼y�! <��K)�|C��~4��U�X�[�9T��f-��:��i���h\5�	B��xŞ�R.��ϯ=&{OL.�S���>���<�ff߿�74��J�h�_��S�:����乖e��?�����J���0\j��P=2��T��[5)Q�6�{����,��^��900ppw����ht�;�H����W9�ș��v�w}�,�l�;�����eY7�]Ec����Q�?H����j�V��ʳ=�z9��\y�*؞�,������:=�
�(y���v��/Z��HD W!��^�}��S�͛�
�{p�K��q��ء
5���^$�?w坅��A�䤀��P�1n����t=��1��d�f�إ�/:���]�H���k��"v����;�dp=5W�bM}�4�[)�t��(����T�M�a?!���U`�eY��F���BJ���;�u7c�诸���FTĤɨu�����h�o���.Zz[�޶u���#1�1�=u#�z��g���׻�No��I ^��p������(?��1�X�lYJ�n���7�
!�s�۶���@��8�YH��]���2C�O�;���>l�"�G���YH"�=��v��I��^7n.�_}��^�DT�̐��.K��YD?����!�!��D"q�eY����	�c�=V��טP/[H/`�]�囈����ɵ�7���~6*&�dW�ݨ�%��6����ߛ����ӧ?�
1�BJ��d2�y�֍Q!D��ar(���h���\�~c����u�a��9X�~��Qa54{��P5���.�H�=$z��ߵx��ꁘE�!�{�#�\\�>�-z�&��PYE?�I܋�ma]��M;齰��dz q ���J\/t$^�{u�\�O&��E�菫���5]cK)/F�e�E?����"�^9'�y����yǉ�_+%v�E�h��4��M��X�/�\������=�W��d�����>ζ�a#v�P��K��^2�iB��Qpǁ0�����d�ҥ �f������6w$Y�F��vw2�<�3��@$�I|�ZR�I#߁�fm��pҼQ���]%^��K�Ƿ�K<=�����h�"O�
��3�fZ$9hB)ew2����mڸ̚K�������Z����� 6���@x���=��bVb�O ?W~�!�+B�X�xq�[1�����ׯBt���ف��+�_�b1Z[[p��K���غu���ƍ{2E>�\��,�F�|��u4N���b/e�o�c� �{�ѽ!��Ⱥ��_644ܟM�α�����"�����W��.�|������;Uo�ߜj���b4#w+�%An������5>�7Z���eY�V��e���^�F����R�'��������@�臚�@��eҤjxp;��'�k�T�U>�n�2�"c��E�|�������!DJ�z��͛�K@9�n�D"�w�������L�0A���̮����<z��q�T�������xJ�ζ(k�0�N�s�b,�b�x��Rt����&��#�,xQ���q�eYy��/������޽{����Sq��2c�Ⱦ�>F��Ν;����WrY��4	f���	��j�L�:>L�_��^Y�B�ԩSS5���b����o��f�q-��uO�����]�g�dJ�W�sΐ� �̦��@�Q3�Z��U1�0�F���3�+��	Z)#ށ��6���'�f(�ŔB�&M_��\�r�n*Ő��o J�/�P>ͨ9*�l�  PIDAT�����`<� LA=c&�Ǎ7e�Ν�߾P�孫�;
��'O�<�FO�!���g� 9���q�T�k�>h��H'����|�w�5�=%�Iξf�țt�z�(�u�����@�?��=;�>�O	�|!��s?�CN>)u�;oY�{�~�Z֝��s�r��*I8�W&���K��Q�}s�9�H�D"�������a�g��I#Y��K��g*��@��r'�W��Y�t["��D"o
!�@-o�#�Bl�R�%���D��̡�x'����j'�?��a��o�zEb�2V���R���o!���=@�b���-�-!� ��ɻ�Rn�R�G"�-�'�L�cǎ�*��{��I%��3C,C�	ӆ�Q]m��O��{��B�I�U�t@�r��NY �G"�S)ôY�n]�)f�̙���x��נ�m���W:�\Y��5��ve��x<�11��r܆zJ �a�6l�J��2�	>���kּ�Zb@�:�XY��q��z��R�5ŞGJ�'`��ٳ��5x)o�5s����P�X1$��q'�:ge��SH�R��R����b<-J��A/g�GLqp��d�˲�اϳ~�������2G�xzI/"K�d��c������F��q�ʚ�~�!D�؉�y��2{o�R�G��W�m˲R#Ƴg���ސ��j���~���r����'-Z��~���%v���"��[J٢6E7𔔲�������n��*|�����/#y#�;%�1wqr�^rq>��^C�,Y��Q{Pz6��/�իWW��C�JD-�H߯�y��7��$�
"3_�'s�^�:�؃��s{b�N����ӟ]&��z��b�r�d5	�+���kz7�A��� �o�����"��ˁ���r?ޕ&��$�����p,�;#�� ���M��57�pC��4T����q>X\�U�	!����(�`�H<!D�+U,�aPR�/���.�\C���|�]�q&����?}×P�>�kO�Oc�
]��4^E�^���YQS���BvxŞޖ�B��	;Τ�w�A� {T�ZW�	Cj�C9�VËu�;�C�l���0V���J?g'�������0�	�M��jޗ��N�eD95����,IVa5��AC\NM�oc�7�Y�L��,vE�eǒ�9���t��H�Q+�!�=Dٔj�[�/7a�Q��'�>�T�t�ae��K3�<7���.u���٨��������m$�4A�w���I��m0dR��������^o�+�p�aE��2d���R�`����`0��`0��`0��`0��`0��&��c��tp    IEND�B`�           [remap]

importer="texture"
type="StreamTexture"
path="res://.import/AstarPathing.svg-346480753f98003893249cd294368285.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://icons/AstarPathing.svg"
dest_files=[ "res://.import/AstarPathing.svg-346480753f98003893249cd294368285.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
  GDSC   @      V   P     ���Ӷ���   ����������Ӷ   �����Ķ�   ���������޶�   ����������������������Ŷ   ����¶��   ������ڶ   �����ڶ�   �����¶�   ������������϶��   �����׶�   �����׶�   �������Ѷ���   �������������Ӷ�   �����϶�   �������Ӷ���   ���������������¶���   ����¶��   ��������������������ض��   ������Ҷ   �����������ζ���   �������۶���   �����������������ض�   �������ض���   �����ٶ�   �����������������ڶ�   ����������Ӷ   ��������Ҷ��   �����������������Ӷ�   �����¶�   ������������϶��   ����������������������ض   ����������������Ҷ��   ������������׶��   ���������������۶���   �����ض�   �����������¶���   ����Ӷ��   ����Ҷ��   �����������ض���   ����������Ѷ   �������������Ѷ�   ������������Ѷ��   ������������Ŷ��   ���������Ӷ�   �����������Ķ���   �����������Ŷ���   ���������ζ�   ��������������¶   ����¶��   �������؅�   ��������Ҷ��   ���������Ķ�   �������Ӷ���   ����ƶ��   ��������Ҷ��   ���������������������ض�   ���������Ӷ�   ��������޶��   ��������ض��   �������ζ���   �������϶���   ���������������Ŷ���   ζ��   �     
                Pivot         Pivot/Swivel      Camera        Target              AStar               ui_down       down      ui_up         up        ui_left       left      ui_right      right               �>      Seekers       Spawn_Points   -                            
                                 	       
   %      &      +      ,      1      2      9      @      A      G      N      U      ^      e      f      m      }      �      �      �      �      �       �   !   �   "   �   #   �   $   �   %   �   &   �   '   �   (   �   )   �   *     +     ,     -   "  .   &  /   *  0   7  1   @  2   G  3   J  4   W  5   ]  6   ^  7   d  8   q  9   w  :   x  ;   ~  <   �  =   �  >   �  ?   �  @   �  A   �  B   �  C   �  D   �  E   �  F   �  G   �  H   �  I   �  J   �  K   �  L     M     N     O     P     Q   "  R   ,  S   /  T   :  U   N  V   3YY8P�  Q;�  YY:�  V�  YY:�  V�  �  YY;�  V�  Y;�  V�  YY;�  V�	  YY;�
  V�  YY;�  V�  �  Y;�  V�  �  YY0�  PQV�  �  �  P�  Q�  �  �  P�  Q�  �
  �  T�  P�  Q�  �  �  P�  QYY0�  P�  QV�  &�  4�  �  T�  �  T�  �  V�  ;�  V�  �
  T�  P�  T�  Q�  ;�  V�  �
  T�  P�  T�  Q�  �  ;�  �  P�  QT�  PQT�  �  ;�  �  T�  P�  R�  RLMR�	  Q�  &�  V�  �  P�  T�  Q�  &�  T�   P�
  QV�  �!  P�  Q�  &�  T�   P�  QV�  �!  P�  Q�  &�  T�   P�  QV�  �!  P�  Q�  &�  T�   P�  QV�  �!  P�  Q�  Y0�  P�  V�  QV�  �  T�"  T�#  �  �  P�  R�  R�  QYY0�$  P�%  V�  QV�  �  �%  �  &�  V�  )�&  �  P�  QT�'  PQV�  &�&  T�(  PQV�  �&  T�)  P�  Q�  (V�  )�&  �  P�  QT�'  PQV�  �&  T�*  PQYY0�+  PQV�  )�&  �  P�  QT�'  PQV�  �&  T�,  PQYY0�-  PQV�  ;�.  �  P�  Q�  ;�/  �   PQ�.  T�0  PQ�  ;�1  V�2  �.  T�3  P�/  Q�  ;�4  �  T�5  PQ�  �4  T�6  P�  P�  QQ�  �  P�  QT�7  P�4  Q�  �4  T�"  T�#  �1  T�"  T�#  �  &�  V�  �4  T�)  P�  QYY0�8  P�9  V�  QV�  �  �9  �  )�&  �  P�  QT�'  PQV�  �&  T�:  �  YY0�!  P�;  V�  QV�  /P�;  QV�  �  V�  �  T�<  P�%  P�  QQ�  �  V�  �  T�<  P�%  P�  QQ�  �  V�  �  T�=  P�%  P�  QQ�  �  V�  �  T�=  P�%  P�  QQ�  �  T�>  T�?  �/  P�  T�>  T�?  R�  R�  QY`   GDSC   �      �        ���Ӷ���   �����������Ķ���   �������Ӷ���   �����������������Ҷ�   �����������¶���   ������������Ӷ��   ���������   ��������ﶶ�   ���������Ŷ�   ����������Ŷ   ��������������Ӷ   �������������ݶ�   �����������������������϶���   ���������ζ�   ���������ض�   ���������������Ŷ���   ����������Ӷ   ����   ��������Ӷ��   �������ƶ���   ���������¶�   �嶶   �������������ն�   ���Ŷ���   ����������Ҷ   ����������������Ҷ��   ��������������ݶ   �������������Ӷ�   ����������Ŷ   ����������������Ŷ��   �����������Ķ���   �����������ݶ���   �����������������������϶���   ���������   ζ��   ���������ﶶ   ϶��   �������������϶�   ���Ӷ���   ��������Ҷ��   ������ض   �����Ҷ�   ����������ݶ   ��������������¶   ����ⶶ�   �������   �����������Ӷ���   ����������������������Ӷ   ��������Ҷ��   ��������Ҷ��   �����������������Ӷ�   �����������Ӷ���   ��������������������������Ŷ   ����Ӷ��   ����������Ӷ   �����Ŷ�   ��������Ӷ��   �������������ݶ�   ����������������¶��   ����������������¶��   ���������¶�   ��������۶��   �����ض�   �����¶�   ��������������Ӷ   �������Ӷ���   ���Ӷ���   ���������������϶���   ������Ŷ   ������������϶��   �涶   ��������   ����Ķ��   �������Ӷ���   �������Ķ���   ��������������Ķ   ��Ŷ   �����������¶���   �����������Ŷ���   �����������޶���   �������������޶�   �������Ŷ���   ��������Ŷ��   �������ݶ���   ������ж   ��������Ҷ��   �����޶�   �������¶���   �����������¶���   �����������Ķ���   ����������¶   ��������޶��   ��������Ŷ��   �������޶���   ���������޶�   ������������Ӷ��   �������������Ҷ�   ��������������¶   ����Ҷ��   �����������ض���   ���������Ӷ�   ����Ӷ��   ���Ӷ���   ��������������Ŷ   ��¶   ��������   ���Ŷ���   �����������¶���   �����������϶���   �������ض���   ����Ķ��   ����Ӷ��   ����ݶ��   ��Ҷ   ����������������������Ķ   ������������Ӷ��   �����������Ӷ���   �������ڶ���   ��������������ڶ   ���޶���   ���������޶�   �����¶�   �������޶���   �����������Ķ���   ��������Ҷ��   �������������������ڶ���   ���������������۶���   ����¶��   ����ݶ��   �������϶���      Path_Manager          2         ?                     
                             Bake Time:         msec               @   d                	   Node_Grid                                              '      ,   	   1   
   8      9      @      G      H      O      P      Q      X      Y      ^      c      d      e      l      v      �      �      �      �      �      �       �   !   �   "   �   #   �   $   �   %   �   &   �   '   �   (   �   )   �   *   �   +   �   ,   �   -     .     /     0   #  1   $  2   -  3   7  4   E  5   F  6   L  7   W  8   b  9   c  :   �  ;   �  <   �  =   �  >   �  ?   �  @   �  A   �  B   �  C   �  D   �  E     F   
  G     H   !  I   (  J   )  K   J  L   N  M   ^  N   _  O   j  P   s  Q   v  R   z  S     T   �  U   �  V   �  W   �  X   �  Y   �  Z   �  [   �  \   �  ]   �  ^   �  _   �  `   �  a     b     c     d   .  e   2  f   @  g   C  h   D  i   L  j   O  k   P  l   V  m   ]  n   _  o   l  p   p  q   s  r   w  s   x  t   ~  u   �  v   �  w   �  x   �  y   �  z   �  {   �  |   �  }   �  ~   �     �  �   �  �   �  �   �  �   �  �   �  �      �   	  �   
  �     �     �      �   "  �   #  �   -  �   7  �   A  �   J  �   K  �   Q  �   g  �   h  �   r  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �     �     �     �   3YY5;�  V�  �  PQYY;�  V�  �  Y;�  V�  Y;�  V�  �  P�  R�  QY;�  V�  Y;�  V�  Y;�  V�  �  YY;�	  V�  �  Y;�
  V�  �  YY;�  V�  �  YYY;�  V�  �  YY;�  V�  Y;�  V�  YYY;�  V�  L�  �  T�  P�  R�  QR�  �  T�  P�	  R�  QMYY;�  V�  �
  YY0�  PQV�  ;�  �  T�  PQ�  ;�  �  P�  R�  R�  R�  Q�  �  �  L�  M�  �  �  L�  M�  �  �  PQY�  &�  V�  �  PQ�  �  �  �7  P�  T�  PQ�  Q�  YY0�  P�  V�  R�  V�  R�  V�  R�  V�  QX�  V�  ;�  V�  �  �  �  ;�  V�  LM�  ;�   V�  NO�  �  ;�!  �  P�  P�  T�"  �  QQ�  ;�#  �  P�  P�  T�$  �  QQ�  �  �  �  L�  MT�%  �  �  �  L�  MT�%  �  W�&  T�'  P�!  R�#  R�  R�  Q�  �  )�(  �  V�  �  T�)  P�(  T�*  �  Q�  �   L�(  T�*  M�(  T�%  �  �  ;�+  V�  �  P�  T�,  �  P�  T�"  �  QQP�  T�-  �  P�  T�$  �  QQ�  ;�.  V�/  W�0  T�1  PQT�2  �  ;�3  V�4  �4  T�  PQ�  ;�5  V�6  �6  T�  PQ�  �5  T�7  �  �  �3  T�8  P�5  Q�  �3  T�9  �:  PL�  MQ�  ;�;  �:  P�  Q�  �  )�"  �D  P�!  QV�  )�$  �D  P�#  QV�  ;�<  V�  �+  P�  T�,  P�"  �  �  QQP�  T�-  P�$  �  �  QQ�  �3  T�=  T�>  �<  �  ;�?  �.  T�@  P�3  Q�  ;�A  V�  �?  T�B  PQ�  �  ;�C  V�  �  Y�  ;�D  �.  T�E  P�<  P�  T�F  �  QR�<  P�  T�G  �  QRLMR�;  Q�  &�D  V�  ;�H  �I  P�D  T�J  T�K  �  R�  Q�  �  &�   T�L  P�H  �  QV�  �C  �   L�H  �  M�  (V�  �A  �  �  &�A  V�  �C  �  �  �  W�&  T�M  P�<  R�"  R�$  R�A  R�C  Q�  �  &�	  V�  ;�N  V�  W�&  T�	  P�
  Q�  �  �N  L�  M�  �  �N  L�  M�  �  .L�!  R�#  MYY0�O  P�P  V�  R�Q  V�  R�R  V�  R�S  V�T  R�  �U  V�  R�V  V�  R�W  V�  R�X  V�  QV�  �  T�O  PW�Y  T�Z  T�  P�P  R�Q  R�R  R�S  R�U  R�V  R�W  R�X  QQYY0�[  P�Q  V�  R�R  V�  R�V  V�  R�W  V�  R�X  V�  QX�  V�  ;�\  V�  W�&  T�]  P�Q  R�R  Q�  &�V  V�  W�&  T�^  P�\  R�Q  R�W  R�X  Q�  .�\  YY0�_  PQX�  V�  .�  YY0�`  PQV�  &W�&  �  V�  .�  &�  P�  QT�a  PQ�  V�  �  PQ�  (V�  �  PQYY0�  PQV�  )�b  �  P�  QT�c  PQV�  �b  T�d  PQYY0�I  P�e  V�  R�f  V�  QX�  V�  .�  P�  P�e  Q�  P�f  QQYY0�:  P�g  V�  QX�  V�  ;�h  V�  �  �  )�e  �g  V�  �h  �  P�  P�  R�e  QQ�  .�h  YY0�  PQV�  &�  P�  QT�a  PQ�  V�  �  P�  QT�i  PQ�  (V�  )�"  �D  P�  QV�  )�$  �D  P�  QV�  �  ;�j  V�  W�&  T�k  P�"  R�$  Q�  �  &�j  �  V�  ,�  �  ;�l  V�  �j  L�  M�  ;�m  V�  �j  L�  M�  ;�A  V�  �j  L�  M�  ;�n  V�  �  T�o  �  �  &�l  �  V�  �n  �  P�  T�o  R�  T�p  R�  P�  R�  R�l  QQ�  �  �n  �n  &�A  (�  T�q  �  �r  P�m  �  P�  R�  R�  QR�n  QYY0�r  P�m  V�  R�n  V�  QV�  ;�s  �t  T�  PQ�  ;�u  �v  T�  PQ�  ;�w  �x  T�  PQ�  �w  T�7  �  �  �  �w  T�y  �w  T�7  �  �  �s  T�z  P�w  Q�  �u  T�{  �n  �  �  P�  QT�|  P�s  Q�  �s  T�}  P�  R�u  Q�  �s  T�~  T�>  �m  YY1�  V�  ;�*  V�  �  ;�%  V�  �  �  0�  P��  V�  R��  V�  QV�  T�*  ��  �  T�%  ��  Y`             GDSC   6   "   s   D     ������ڶ   ����Ķ��   ����������������϶��   �����������������϶�   �����϶�   ���������¶�   �������Ӷ���   �������Ŷ���   ����׶��   ��������������¶   �������Ҷ���    ���������������������������Ҷ���   ������Ӷ   ���Ӷ���   ���¶���   �������Ŷ���   ������������ض��   ��������¶��   �����������¶���   ���Ӷ���   ζ��   ϶��   ��������    ����������������������������Ҷ��   ����������������������Ҷ   �����������Ŷ���   ����������Ӷ   �������������Ҷ�   ��������Ӷ��   ���������Ҷ�   �������ⶶ��   ������������   ���������Ŷ�   ����������Ŷ   ��������������Ӷ   �����������������Ҷ�   �������ƶ���   ������������Ӷ��   ������������������������Ҷ��   ������������׶��   ����������������������Ҷ   �������������������������Ҷ�   ������������������������Ҷ��   �����������������������Ҷ���   �����������Ķ���   ������������������������Ҷ��   ������������Ŷ��   ����������������Ӷ��$   ����������������������������������Ҷ   ���������������������ض�   �����������������������Ҷ���   �����������¶���   �������Ѷ���$   ����������������������������������Ҷ      AStar         Parameters/Display        Navigation/Display        Seekers              Bake_Button             Track_Button             Parameter_Button      <               >      �         Navigation_Button         ^         v      d              �?      Weights_Control/CheckBox      Kernel_Size/LineEdit      Node_Size/LineEdit     &   Visualize_Grid/Visualize_Grid_CheckBox        Spawn_Seeker      Clear_Seekers      	   Bake_Time         down      up        right         left   &   Visualize_Path/Visualize_Path_CheckBox        Stop Tracking         Start Tracking                           	                           	   %   
   ,      3      4      ;      L      W      b      c      i      o      u      �      �      �      �      �      �      �      �      �      �      �       �   !   �   "   �   #     $     %     &   %  '   &  (   ;  )   >  *   D  +   S  ,   ^  -   g  .   |  /   }  0   �  1   �  2   �  3   �  4   �  5   �  6   �  7   �  8   �  9   �  :   �  ;   �  <   �  =   �  >     ?   
  @     A     B     C     D   (  E   3  F   >  G   I  H   V  I   W  J   ]  K   f  L   g  M   m  N   v  O   w  P   }  Q   �  R   �  S   �  T   �  U   �  V   �  W   �  X   �  Y   �  Z   �  [   �  \   �  ]   �  ^   �  _   �  `   �  a   �  b   �  c   �  d   �  e   �  f   �  g   �  h     i     j     k     l     m   "  n   -  o   .  p   /  q   5  r   B  s   3YY;�  V�  YY;�  VY;�  VYY0�  PQV�  �  �  PQT�  PQ�  �  �  P�  Q�  �  �  P�  QYY0�  P�  QV�  &�  PQT�  P�  QT�	  PQ�  V�  �  T�  P�  QT�
  �  �  �  T�  P�  QT�
  �  YY0�  PQV�  &�  T�  V�  �  T�  PQ�  �  T�  PQT�  P�	  QT�  �
  �  ;�  �  T�  PQT�  �  ;�  �  PQT�  �  �  T�  PQT�  �  P�  T�  �  R�  T�  Q�  (V�  �  T�  PQ�  �  T�  PQT�  P�	  QT�  �  �  ;�  �  T�  PQT�  �  ;�  �  PQT�  �  �  T�  PQT�  �  P�  T�  �  R�  T�  QYYY0�  PQV�  &�  T�  V�  �  T�  PQ�  �  T�  PQT�  P�  QT�  �  �  ;�  �  T�  PQT�  �  ;�  �  PQT�  �  �  �  T�  PQT�  �  P�  T�  R�  T�  �  Q�  (V�  �  T�  PQ�  �  T�  PQT�  P�  QT�  �  �  ;�  �  T�  PQT�  �  ;�  �  PQT�  �  �  T�  PQT�  �  P�  T�  R�  T�  �  QYY0�  PQV�  ;�  V�  �  �  ;�  V�  �  �  ;�  V�  �  �  ;�  V�  �  �  &�  T�  P�  QT�  PQV�  �  �  �  &�4  P�  T�  P�  QT�  Q�  V�  �  �  P�  T�  P�  QT�  Q�  &�4  P�  T�  P�  QT�  Q�  V�  �  �  P�  T�  P�  QT�  Q�  &�  T�  P�  QT�  PQV�  �  �  �  �  �  T�   �  �  �  T�!  �  �  �  T�"  �  �  �  T�#  �  �  �  �  T�$  PQ�  �  T�  P�  QT�
  �  �  �  T�  P�  QT�
  �  �  �  T�  P�  QT�
  �  �  �  T�  P�  QT�
  �  �  �  P�  QT�  �  T�%  PQYY0�&  PQV�  �  PQT�'  P�  QYY0�(  PQV�  �  PQT�'  P�  QYY0�)  PQV�  �  PQT�'  P�  QYY0�*  PQV�  �  PQT�'  P�  QYYY0�+  PQV�  �  PQT�,  PQYYY0�-  PQV�  �  PQT�.  PQ�  �  �  T�  P�  QT�
  �  �  �  T�  P�  QT�
  �  �  �/  PQYYY0�0  PQV�  �  PQT�1  P�  T�  P�  QT�  PQQYY0�2  PQV�  �/  PQ�  �  PQT�3  P�  PQT�4  QYY0�/  PQV�  &�  PQT�4  V�  �  T�  P�  QT�  �   �  (V�  �  T�  P�  QT�  �!  YYY0�5  PQV�  �  PQT�  PQT�  PQY`         GDSC   @      V   P     ���Ӷ���   ����������Ӷ   �����Ķ�   ���������޶�   ����������������������Ŷ   ����¶��   ������ڶ   �����ڶ�   �����¶�   ������������϶��   �����׶�   �����׶�   �������Ѷ���   �������������Ӷ�   �����϶�   �������Ӷ���   ���������������¶���   ����¶��   ��������������������ض��   ������Ҷ   �����������ζ���   �������۶���   �����������������ض�   �������ض���   �����ٶ�   �����������������ڶ�   ����������Ӷ   ��������Ҷ��   �����������������Ӷ�   �����¶�   ������������϶��   ����������������������ض   ����������������Ҷ��   ������������׶��   ���������������۶���   �����ض�   �����������¶���   ����Ӷ��   ����Ҷ��   �����������ض���   ����������Ѷ   �������������Ѷ�   ������������Ѷ��   ������������Ŷ��   ���������Ӷ�   �����������Ķ���   �����������Ŷ���   ���������ζ�   ��������������¶   ����¶��   �������؅�   ��������Ҷ��   ���������Ķ�   �������Ӷ���   ����ƶ��   ��������Ҷ��   ���������������������ض�   ���������Ӷ�   ��������޶��   ��������ض��   �������ζ���   �������϶���   ���������������Ŷ���   ζ��   �     
                Pivot         Pivot/Swivel      Camera        Target              World               ui_down       down      ui_up         up        ui_left       left      ui_right      right               �>      Seekers       Spawn_Points      AStar      -                            
                                 	       
   %      &      +      ,      1      2      9      @      A      G      N      U      ^      e      f      m      }      �      �      �      �      �       �   !   �   "   �   #   �   $   �   %   �   &   �   '   �   (   �   )   �   *     +     ,     -   "  .   &  /   *  0   7  1   @  2   G  3   J  4   W  5   ]  6   ^  7   d  8   q  9   w  :   x  ;   ~  <   �  =   �  >   �  ?   �  @   �  A   �  B   �  C   �  D   �  E   �  F   �  G   �  H   �  I   �  J   �  K   �  L     M     N     O     P     Q   "  R   ,  S   /  T   :  U   N  V   3YY8P�  Q;�  YY:�  V�  YY:�  V�  �  YY;�  V�  Y;�  V�  YY;�  V�	  YY;�
  V�  YY;�  V�  �  Y;�  V�  �  YY0�  PQV�  �  �  P�  Q�  �  �  P�  Q�  �
  �  T�  P�  Q�  �  �  P�  QYY0�  P�  QV�  &�  4�  �  T�  �  T�  �  V�  ;�  V�  �
  T�  P�  T�  Q�  ;�  V�  �
  T�  P�  T�  Q�  �  ;�  �  P�  QT�  PQT�  �  ;�  �  T�  P�  R�  RLMR�	  Q�  &�  V�  �  P�  T�  Q�  &�  T�   P�
  QV�  �!  P�  Q�  &�  T�   P�  QV�  �!  P�  Q�  &�  T�   P�  QV�  �!  P�  Q�  &�  T�   P�  QV�  �!  P�  Q�  Y0�  P�  V�  QV�  �  T�"  T�#  �  �  P�  R�  R�  QYY0�$  P�%  V�  QV�  �  �%  �  &�  V�  )�&  �  P�  QT�'  PQV�  &�&  T�(  PQV�  �&  T�)  P�  Q�  (V�  )�&  �  P�  QT�'  PQV�  �&  T�*  PQYY0�+  PQV�  )�&  �  P�  QT�'  PQV�  �&  T�,  PQYY0�-  PQV�  ;�.  �  P�  Q�  ;�/  �   PQ�.  T�0  PQ�  ;�1  V�2  �.  T�3  P�/  Q�  ;�4  �  T�5  PQ�  �4  T�6  P�  P�  QQ�  �  P�  QT�7  P�4  Q�  �4  T�"  T�#  �1  T�"  T�#  �  &�  V�  �4  T�)  P�  QYY0�8  P�9  V�  QV�  �  �9  �  )�&  �  P�  QT�'  PQV�  �&  T�:  �  YY0�!  P�;  V�  QV�  /P�;  QV�  �  V�  �  T�<  P�%  P�  QQ�  �  V�  �  T�<  P�%  P�  QQ�  �  V�  �  T�=  P�%  P�  QQ�  �  V�  �  T�=  P�%  P�  QQ�  �  T�>  T�?  �/  P�  T�>  T�?  R�  R�  QY`   GDSC   ;      b   c     ���Ӷ���   ������������Ӷ��   ������������������Ҷ   �����Ҷ�   �����������������ζ�   ����ζ��   ����¶��   ���������������Ӷ���   ����   �����������޶���   �����������¶���   ����������¶   ���ݶ���   �����Ҷ�   �����ݶ�   ���Ӷ���   ������������Ҷ��   ����������������Ҷ��   ����Ķ��   ����¶��   ����¶��   ����������������Ӷ��   ���Ŷ���   ���������¶�   �������Ӷ���   �������¶���   �������Ӷ���   �������������޶�   ������������Ŷ��   ��������޶��   ��������¶��   ������Ҷ   �����޶�   �����������¶���   ������������¶��   �������ݶ���   �����������������ƶ�   �������������޶�   ��������ն��   ��������¶��   �����������������Ӷ�   ������¶   ����ζ��   ���Ҷ���   ������Ӷ   ߶��   ��������Ҷ��   ������������Ѷ��   ��������ζ��   ����������¶   ������ж   ��������������޶   �����¶�   ���Ҷ���   ��������ݶ��   ���������Ҷ�   ������޶   ������������¶��   �������������¶�                path_data_defered         path_data_execute                path_data_clean_up                                  	                           	   "   
   #      ,      2      :      B      H      O      U      _      d      e      k      }      �      �      �      �      �      �      �      �      �       �   !   �   "   �   #   �   $   �   %     &     '     (     )     *     +      ,   $  -   %  .   &  /   *  0   2  1   3  2   <  3   D  4   J  5   Q  6   S  7   Z  8   [  9   d  :   w  ;   �  <   �  =   �  >   �  ?   �  @   �  A   �  B   �  C   �  D   �  E   �  F   �  G   �  H   �  I   �  J   �  K   �  L   �  M   �  N   �  O   �  P   �  Q   �  R   �  S   �  T   �  U   �  V     W     X     Y      Z   2  [   8  \   >  ]   D  ^   J  _   P  `   V  a   \  b   3YY;�  V�  YY;�  V�  Y;�  V�  YY0�  PQV�  �  �  T�  PQYY0�	  P�
  V�  QV�  &�  V�  �  �  T�  PQ�  �  �  T�  PQ�  �  T�  PQ�  �  T�  P�
  Q�  �  T�  PQ�  &�  T�  PQ�  V�  �  P�  Q�  Y0�  PQV�  ;�  �  T�  PR�  R�  T�  PQQ�  &�  �  V�  �8  P�  Q�  Y0�  P�  QV�  �  &�  PQT�  PQT�  PQT�  PQT�  P�  T�  QV�  .�  ;�  �  PQT�  P�  T�  R�  T�  R�  T�   R�  T�!  R�  T�"  Q�  �  P�  Q�  .L�  T�#  R�  R�  T�  M�  Y0�$  PQV�  ;�  �  T�%  PQ�  �  L�  MT�&  P�  L�  MQ�  �  T�'  PQ�  �  &�  T�  PQ�  V�  �  �  P�  Q�  '�  T�  PQ�  V�  �  �  �  YYY1�  V�  ;�(  V�  LMY�  0�  P�)  V�  QV�  ;�*  �+  P�)  Q�  &�*  �  V�  �,  P�*  R�)  Q�  .�  �(  T�  P�)  Q�  �  0�+  P�)  V�  QV�  )�-  �D  P�  PQ�  R�  R�  QV�  &�(  L�-  MT�.  �)  T�.  V�  &�(  L�-  MT�/  V�  .�  �  .�-  �  .�  �  �  0�,  P�0  V�  R�1  V�  QV�  �(  L�0  M�1  �  �  0�  PQV�  .�(  T�  PQ�  �  0�'  PQV�  �(  T�'  PQ�  �  0�  PQV�  .�(  T�  PQYYY1�  V�  ;�  V�  �  ;�  V�  �  ;�  V�  �  ;�   V�  �  ;�!  V�  �  ;�"  V�  �  ;�#  V�2  �  ;�.  V�  �  ;�/  V�  �  Y�  0�  P�3  V�  R�4  V�  R�5  V�  R�6  V�2  R�  �7  V�  R�8  V�  R�9  V�  R�:  V�  QV�  T�  �3  �  T�  �4  �  T�  �5  �  T�   �8  �  T�!  �9  �  T�"  �:  �  T�#  �6  �  T�.  �7  `  GDSC   P      �   �     ������������϶��   ������϶   ��������   �������������������󶶶�   ��������������������������   �����������������Ҷ�   �����¶�   ���������Ҷ�   ��������¶��   ���������Ҷ�   ����������������Ӷ��   ����������޶   ��������޶��   ��������Ŷ��   ����Ķ��   ���������Ӷ�   ��������ζ��   �������������޶�   ���������Ѷ�   �����������¶���   ���������Ѷ�   ����������������϶��   ����������Ѷ   ���������Ķ�   ����Ķ��   �������������Ҷ�   �����϶�   �������Ӷ���   ����   ���������¶�   ��������Ҷ��   ���������Ӷ�   �����������Ҷ���   ����ƶ��   �����Ķ�   �������������Ѷ�   ���������¶�   ���������������۶���   �����ض�   ����������޶   ����������Ѷ   ���������Ҷ�   ������������Ѷ��   ���ƶ���   ���������Ҷ�   ���������Ŷ�   �������޶���   �����������������޶�   ����¶��   ��������Ҷ��   ��������������Ҷ   �����������޶���   �������޶���   ��������������޶   ��������������¶   ����������ٶ   �����������Ŷ���   ��������۶��   ���������¶�   �涶   �������ض���   ����Ŷ��   ��������Ķ��   �������������Ӷ�   ������Ҷ   ϶��   ���������������������޶�   ��������������������Ӷ��   ��ń򶶶   ζ��   ̶��   ������������������϶   �����������������ζ�   ���������������ζ���   ����������������¶��   �����������¶���   ���������������Ŷ���   ����׶��    ����������������������������¶��   �������������Ҷ�        �������?      ?     �@     �@     @@                          Path_Update_Timer         Core   
   path_found          {�G�z�?                                                 $      %   	   *   
   3      <      E      N      W      `      a      f      g      l      q      x            �      �      �      �      �      �      �      �      �       �   !   �   "   �   #   �   $   �   %   �   &   �   '   �   (   �   )   �   *   �   +   �   ,   �   -     .     /     0     1     2   #  3   +  4   /  5   0  6   8  7   @  8   A  9   G  :   K  ;   Q  <   R  =   [  >   d  ?   i  @   j  A   p  B   q  C   u  D   v  E   }  F   ~  G   �  H   �  I   �  J   �  K   �  L   �  M   �  N   �  O   �  P   �  Q   �  R   �  S     T     U     V   &  W   '  X   (  Y   H  Z   I  [   Q  \   R  ]   ^  ^   `  _   a  `   u  a   z  b   �  c   �  d   �  e   �  f   �  g   �  h   �  i   �  j   �  k   �  l   �  m   �  n   �  o   �  p   �  q   �  r   �  s     t     u   )  v   -  w   .  x   /  y   0  z   1  {   2  |   3  }   4  ~   5     6  �   7  �   ;  �   ?  �   C  �   D  �   E  �   F  �   G  �   H  �   I  �   J  �   K  �   L  �   M  �   N  �   O  �   P  �   Q  �   R  �   Y  �   ]  �   a  �   e  �   h  �   l  �   m  �   s  �   �  �   �  �   �  �   �  �   3YY:�  �  T�  YY:�  V�  �  Y:�  V�  �  Y:�  V�  �  �  YY;�  VY8P�  Q;�  �  Y8P�  Q;�  �  Y8P�  Q;�	  �  Y8P�  Q;�
  �  Y8P�  Q;�  �  Y8P�  Q;�  �  YY;�  V�  YY;�  V�  Y;�  V�  Y;�  V�  �  Y;�  V�  �  Y;�  V�  �  Y;�  V�  �  YY;�  V�  Y;�  V�  YY;�  V�  YY;�  V�  YY0�  PQV�  �  �  P�	  Q�  �  �  T�  PQ�  �  �  T�  PQ�  �  �  PQT�  PQT�  P�  Q�  �  PQT�  PQT�  P�  QYY0�  PQV�  �  PQT�  PQT�   P�  Q�  �  PQT�  PQT�   P�  Q�  Y0�!  P�"  V�  QV�  �  �"  �  �  �"  T�  P�
  QYY0�#  P�$  VQV�  �  �$  �  �  �  T�%  T�&  �  �'  PQYY0�(  PQX�  V�  .�  T�)  PQYY0�*  PQV�  �  �  �  �  T�+  PQYY0�,  P�-  V�  QV�  &�Q  P�-  Q�  V�  �.  P�-  QYY0�'  PQV�  �  �/  PQ�  �  �  T�0  P�  QYY0�/  PQV�  ;�1  �  T�2  PQT�2  PQ�  �  T�3  PT�4  PQR�%  T�&  R�  T�%  T�&  R�2  PR�  QR�1  R�  R�
  R�  QYY0�5  PQX=V�  &�  �  �Q  P�  QV�  .�  ;�6  �  L�  M�  &�  P�  	�Q  P�  QQV�  &P�%  T�&  T�7  P�6  Q	�  QV�  �  �  �  &�  �Q  P�  QV�  �  �  �  .�  ;�8  V�  �9  T�:  P�6  R�  T�;  Q�  �<  �  P�<  R�8  T�=  T�>  PQR�  QY�  �  �?  P�  P�  R�  R�  �  �  QT�@  P�  T�;  R�<  T�A  QR�  T�;  QYY0�B  PQX=V�  �  &�  �  �  T�C  PQV�  .Y�  ;�D  V�  �  P�%  T�&  T�E  R�%  T�&  T�F  Q�  �8  P�D  Q�  *�  T�G  P�  R�D  QV�  &�  �  T�H  PQV�  �  �  �  +�  (V�  �  �  Y�  &�  V�  �  �  �  &�  �  T�I  PQ�
  �  V�  �  �/  P�  T�J  P�  T�H  PQR�D  Q�
  R�  R�  Q�  &�  	�  V�  �  �  �  ;�8  V�  �9  T�:  P�  T�K  P�  QR�  T�;  Q�  �<  �  P�<  R�8  T�=  T�>  PQR�  QY�  �  �?  P�  P�  R�  R�  �  �  QT�@  P�  T�;  R�<  T�A  QR�  T�;  QYY0�.  P�-  V�  QX=V�  �  �-  YYYYYYYYY�  �  �  �  �  �  �  �  �  �  YYYYYYYYYYYYYYYY0�L  P�M  QV�  &�  V�  &�  V�  �B  PQ�  (V�  �5  PQYY0�N  PQV�  &P�  T�%  T�&  �  QT�O  PQ�  V�  �  �  �  �/  PQ�  �  �  T�%  T�&  Y`    [remap]

path="res://scripts/AStar Implementation.gdc"
         [remap]

path="res://scripts/AStar.gdc"
        [remap]

path="res://scripts/Canvas.gdc"
       [remap]

path="res://scripts/Main.gdc"
         [remap]

path="res://scripts/Path_Manager.gdc"
 [remap]

path="res://scripts/Seeker.gdc"
       �PNG

   IHDR  O  O   e�0n   sBIT|d�   	pHYs  .#  .#x�?v   tEXtSoftware www.inkscape.org��<    IDATx���y��U���׹��0,"�
�Zj�|�\�4��5�
3Sd� ��e.?e΀Ef�I3�����[&n�b��iIb���
����{~|Cd�{?�{��~>���1��_�Ϲog�{��|��D2����Ԝ�0�"�QdW`7[`�c`S�/����� ��2�����e(��x�����!�Qyِ�lE�#�#ɰ�]������ԣ�M��,:"~��ʓ��Ț.�7+8�G�8��C�����q����A�C�IDD`4�p�/�����a��7�#^�&�T�DD$���N9�p`G�qz�H0u3o�K���C�IDD�'�.d�&0���S�k8���D�X�;N��<��Hz�fo���@�s�����깎�x�w�4Py��Ŏ�N!��}K0�gK&b��&ɒ�����#�W�|ǩ�'q�a
���T*O""�L�8��P�Q<p�Op|�)��;LҨ<��H�|���38�w�X��d��$IT�DD$9��N�[0��;J̴��&��w�$Py�d�Y8&�+'ks��"���s���:�'�m�Ӌ.��}G�� g1��|�eI��BDD�h�p�	��;J�h Ng^g.s}��U*O""R�F���C|G�1�$��7sy�w�Z��$""��+��a{��R�a��$����0�D�IDDjK�݁�;��� �·9�;T�zN�IDDj�(�!�`[�Qd/�ي���w�Z��$""�!Ϧdx ��w�ڏ�(2���RT�DD$���!�l`�Q�p������%�T�DD$�,�s�)�QR�����e�� q��@DDd�^�2�$�1R"��hv�$δø����y|�"9�QR�1��f��;H鶝�����lA������6�a�2�w�8Ry���dx�[�}}GI�ٟ'��Ӿ�č�<��H���h�8�1R� �ȳ�� q��'���lE��	����es�.�A�D3O""/u|�s��9��`�1�DOۉ�H|��0`z��'؊��t��m'""�`ɱ�;1l�;�|��,g1���A�@��DD$^�\{��!���6�#T�DD�?K�7}ǐD/��*O""�߫|t$H��<}|��M�IDD��d0\�;��Ȗd�;�o*O""�׫��Cz�b�Q�;�O*O""��B���mٜ�}��I�IDD�Ç@0֠����$""�8��1k�Q�϶�C���$""��K�CHY28�w_T�DDď�8mOP�g����ʓ����8�w	eF���>�<����c}G��R�5Ty�������cHhG���ʓ��T�I�nJ�:�!�M�IDD��q���~d8�w�jSy�@"��"��$""��w��w����#T�ʓ��TW��}G�H����$""Օe7�$Rۑ���դ�$""զ�,�|�w�jRy��r*O���u�N�IDD�M�)yR�5Uy��	6T�;�D̰��դ�$""Փ���R��PM*O""R=�t�ɦ�I��U�IDD��.]o�����uUy�j�m�dRy��b��dS$U_W�'��:��"R�uUy�jZ�;�TĻ�T�ʓ��T�Iכl�����$""�ӕ�7��HY)Vy��)�]"9�'��Ȥ�M6ER�uUy��ic9��;�D���I�IDD���$b&]_S�'���H�L���*O""R]){�M�O��PM*O""Rm*O��6Sx�w�jRy����SҤj��<��H��3X�;�D�o�T�ʓ��T�V�C"�� զ�$""���;�D��!�M�IDD|H�lEB=��tm�	*O""�������P���ʓ��T�����RJo��<����[|G�P�P��C���$""��
,�B��K&��wT�DDďI,��C�d��;�/*O""��f��,�1)�{u�<���?[� ���R"�L����ʓ���c)�}ǐ�tR�ǾC���$""~e�-������1|Ry�n�-��cH�0\�;�o*O""�V�!�Kn��!|Sy�&�*.��hj��1�w�8Py�x(r5��wY�[��?}����� """ ���%K�#|G�XF�S�+K}��<��H\�3��X�;�|��2����*O""��-8�?��"k�'E��"Nt�NDD|�f� ��HVs8Π��}��<���O��ۀ�������⡕)<�;Dܨ<���/�����>�O�����b����w�8�m;�pp�z?�]�_�T)��i���ȫ��đf�DD����8��Ϝ,�t� �(&��w��Ry�j��٣�.��O�5�)��w�8Sy�jY]�J����@"Y����y�Cĝ�<��H5��-��v�M^���a��U��N�&��m�f�DD��6f��%��ǁ/�̓h=K'1�e����w�����!�x��Vߋ������d��B���+���
�'������c�8x��?y��؏�0�P	�E�h��� �D�IDD*a ��B���੼Έ����SNr!^#͞"ǧ��|�5j�""����,���ӆwyő� 6	�Zi�g2����;H-Ry�(m<�b��	�Ӣ}�(�p7�e��L��Y���J�Aj�����lI���/���=+N S�+�����]Z؊a*N�h͓��Da+�a`�c<AP�^/�ʿ�Ύ���8���49�~/��4���#:n9,ݶ��V��!��;�!�o�N���d��c��c%�]tp�Y�;HR�<��H[ܪS�'(N�-^����/D6f�Y��R&14�%�')�v3N
1���SefEFsp�GEƏ'��7�R�-P�(�')���i�c���R�i�<u���*�_E_˳��f��v��~��T�Y�L���H��� ;������HmH�����7q�����V�f��8�ߡ�ݰ7Y��1�)�T�DD�;��B��(Aqz7�D=5����9�q8��-�Y[��}?�^{��~��9���X���$""=�#Aq�1� N���iMy���D�\ �l�-Ov�ہ���n��a�V����Li�5O""�&X�m�1~OP��E�(*��s0�����t]Ԁ� �lؓ}{�ˠ�}�;�ځ�	��Qy��ٕ�8����p"q+NkC?�38����̼c�}΀s>�C�[���\.��W\�R%���nۉ�Ȇ�FP����~�T`E$�*iˀ���F���(pT��'��B�W	f��q?Sx`��.��z����� ��
Qy��ٝ�81�l�4j�8�m���Ws64�2�c7�cؕ�`�z��e�`x���`�겴OPBy��*1���ʓ����G���!Ƹ��8%���0�ǻ?��l�K_�l��?Џ���0�Cp�r�eL�mJ\�d�yҹ�.�w��$""k�?�A`�c��I)N��Dy���(Oz⮂t괈��飄/N�&I3N1P,Pڿ�]s�5}+�'�T�DDd�}_�n#(N�"I$ Xk���%\�iooO�y~U��$"" ���B�q+��3�D��'K�dc�n�U�ʓ��|x ��΋q*N��D)��cZ*G�ID$�&x�.Lq�%�E�+�D�>%�<�E���$"�^� x*��1~|	�����/i�`��$R*O""�t���Pq���/��-��.�����ھRy�L�ID$}%(Na�m�"I$=Uҭ��cZ$b*O""��I�^J?NdMӀF�I")E9��$b*O""�q��)a��T�	'/�1�.Wy� �'�t8��b�VT��*�<�]�<��$�q�/NS�єx��D��cZ��afeT�DD��x�W@C�1~��S,�sLK&��1-SyI�	_��|=�8�4�ʓ�H2�DpHo�c\|#�8���=�<EO�ID$y>�N��t5��h�HĴh�3�'�d��1�����đ���T�DD���G��B�aQq�5��ʓ�H2���p�i,�M�0��ʓ�H������h�H��T�DDj���1���M��4�W��RDDį�G�����.��,�T�sN3Oi�ID�65�8}�Z�:���'���D���U���IU��T�DDj�(`2����󁉑%_tL�'*O""��B��� 7D�H|�1-�h���Hm�:��	��8�8z���<����ń+N�-T�DǴ���$"o�������h�H\�T�DD���Cz�U �n�$�đ�i�@�ID$�,���/�>.�Uҭ;c��S�`<a�V?`���cvc�:�z;�cځ�ιg3�̂���н_����8������ϣ�#qe�y�9����w�Py�}fԨQ;�q�	 ԯ�	������:Ac�9.\؞����d2�N�2�U�."4����ru g ��&�ę�i�C��kԘ1c�+
#�sÁ�"�q�掎�3f�x;�qEd�\������đ����w��^R6��.�\��Sy�1MMMv�]
|��f�"��Qgg�u?��ߨ��H��g͕��I"���y��=��L&��c�����O�kD>����s��W�8�.���{������nD$z��p�i�9T��JǴT��Shll<���f*_����9����9r���D�e��[�����HI-*��=q��S�Yks�|�c�����2��_�����1��b���I�=�$�ZU�1-�E8$���1b�&.��`�)._�c�u�|~ʰaò�È�0�8/�ˁ��#I$5KǴT_\ޔe�{����O�βM���/��;�H2�$`L�1���$�Դr�i���P�<i��3�{�l6�;���}g�c�)������\�$Y��yG����V�Ñ$���~OU��#�|�O6����w�:q�����HOd�	�L)���4'�D�$%�{�1-�<�ˍ���C����|�2�!Db.� �
1�;�Q�#䑄1Ɣ�ĝf�BPy�����Q;ע����O�!SY�&��m��p,��HI�蘖�Ry������ιk}�!��Y>���w���7_1���1�v��yXY���ZۯRa�N��?S,o�����6'��H���b���鯑$�Ĳ�v�K�$��dje}m�<y����Ec��sD���Ə�!��-�i!�X<I"I��n��E-/�ʓG�ڜs��w�e3��x�!D<[]�N1�����(Ijh��*Qy�hѢEg ;��%�܉�}��nN	1����F()g�Ѣ�*Qy��9�M�*� I�{�lL/�6���˵�8�#�D�*e>q�}�ʠi�455��K�Z��බ�w|��>�k���Aqz*�D�J���R�A�;Yk_�P���̓'ι�3TPo`��"U���p��U�HT�$<ݺ��'>�;@�%��'Aq��p�x�8��3Y�R*O�=z7J�V�E����:�!D*�/p��Q�^� ��$����i��'
�B����_&�9�w�
Y]��1�Pq����h\3OePy�c?���P(���)��)� �>L�z��8=E �Ն��࡝��EǴ�N����n�3T�1&OI�M����B��"Aqz>�@"k:���蘖�Sy�����U��� " �|<��&X�B$�D֭�uO:��t9���Z�[�p��sTC6�ݣ��e�1��b��/k�rߙDʴ�8�b�g��I"���1-��Te/��b���z�1�eK��-�9 ��KnU���Ekm�WP�}#F��$��}�9�w�-�[,����io��Ƙ��ιG��ڞ���	�S�5|���c��1��?w{H�D*OU���R�0o߼��u���{g���1��o]]��]vْʦ�Z������1 g�N���{:��;F%��/p������~p֬Y���� ��\�&�J�I���'�PR�J3�Re�F�ڦX,��w�j�d2l���Q������X,.��.��$�Lcc�Ƙ��È~��Bc̴y���l�ܹ���B��4��Ӣh���i�,�<UY]]ݻ�V���*62P�`���f�zrKp�С/u?�"5.�ϟ
X*�7���ؽ����\.�y���)�{�_���h����V��&xTz@��^}����:R0��T�a����?~���e�W���,���4]k�|~����	�z�\.��{��m�ݘ7oO=�ԺnE���	κ{�r	E6�IJ�~�>����I��*�5kV!��/���Ri�l�w���Xc�jm������vi������|p�1�����ի|0�o�=s��aŊ��?�w�(�ʧY���=阖Ҩ<y��{���e��c���z�3fL�������� l���|���塇bѢ�.a�p*N�_��h��$��Q566N6ƌ��6�tS����;F5h���{���=��ɻ"��"s����>p��<�ⴸ��D��[n�Ο?�]��3�E`S=|�3*O455�㜻�w�J�|��IўV볊`S�������K/}�o��9r䶙L�b�C�s�G}����;c�'m�!�a��˺��Y�L&��c�����C��<���z��ni�%��PWW�;F�b=�W�\���M��:^ ظ����l��B�𒵶�Wp�;��:;;g������r�B��8��8��')�<uӢ��*OL�>��|>�,�!�Y*�W�^QoU�T�w��+W�B��B��`���}:;;����=�P��~�}�?����đ�i��'n.��R�1?4�,rέ�u��F]�m���]ً��"km��h�#�@�!J��dL߾}��5��)S���;�ȚtLK�<�3�䖧�Ƙ˚����L���_�k�����2���.��J�ճ�kfW������9�s�i`�P��a��5kV�n�J�阖��}��������sT@k[[[�ON�0a`gg��ι!ι���\�Np��D'[0�1bH.�{ؤ�W)ι�S���CdM:��24��1�;ι����\S΅݇������5�����k��H�*��m��5weߎ�D�+{.��5^� �1��=�'�'O~�w�5�tLK�N�/V,MBh��/���'Y���lkk;��/j���僷� �g�$:�݂��]�G�ux�X�Sɠ�䜻s�ԩ���!������e=�|c���������y��9�.1���;HDV��f/��H�ꙑu���"���V�����*[����3�̢~��=�E� (�c�����1'�5j�)S�<�;�H�����1-=���hll�e�������V���l������-Y�j�ko��V��s*�s�֩S��C�Z;x��K�m�ݵRy�B�)�wT~�`J��߫W�}'N���w�����ֽ����
���1;9�v����o�ڴdɒ��[���\n�I�&��;���i��������S�_��ι�N�Z�o7���->��/��s��꫔�M�0�\���z��"�cZ*A�"b����|>p��,e���p��b��sKp�B����7��X�bEb��sn8��$q�cZ"��#K�,�x���; ������n�"�Bn��#	ܕ}�ʕ�#T��F��Q��KL蘖��<�ȬY�
g�}����~c�9�w��1�LmmmM��R>l�\m`��ޕ��#�q;� ~�;�HǴ$i���Py��3f����'�}w��<�㜛8dȐ��Αtۂ�{�jH�X\{G�������I�X���s���$1P�1-{�cZ6�'���6�hѢ�ι�}gYK�s��ֆ8�ʾ|�r�~��j��kkk��:�J�>1`�.�1-�����>W윦����    IDAT9ι�@ߙ�7�1g���&eS����-���ֺE�m��wd�Ȯ�B���Ѯ�w��')�<阖Sy����֛���� �;��k��������o\�+�D������wuU��8�7bĈ�����x�9wBWW]]]8��w���1�l6K]]�lvo�Nq�M��jİaò����i����ԩS�v���T��o���V%n/�ur���m<į��?�GG�Qι#�1���t�1���)
N�6��'�M�ƌ1b�l6;�s)�u_�E��u����3f�H�s���[�[d2��k��7�x�������X,4mڴ?��!�b�1Ɯ�Ft�������n�4iҫ�Y�T�j�������������#���Vc�u��<dȐ{��]�D*�����w�j0��������|��̢E�Nt�Y`�
�T��B�0n����V�ubM�)F��m6�=�W�^����*aMI��s�c�ÅB��ӧ/�`T�����|稆A�-hhhxx��Xh�y�W�^�\r�%���'����x�1�`�*�l'p�+�͜9����*O	b�}8�9GWW�B�b��s02��L�\.G.�l�M���TW>���;G5l����խ�/!X��^�"(Y����?{饗�S��R������D�K�{?�3����^O��K�Suuu�����2��)���d���V���c�ʕXk��]��.U���X,.���)�����l����(�9������,Yr�Y��| *OI��D�=����$�?Uc�������
�/W �ڕt+�K�sn����6�[��Pcc�p`P�;K7\0hР}F�}��ɓ��Ti*O)��Р����2�̂�٪I�l�
�����t�NWYk_�{y�Y��:t�K��~zjv,M�����s��Ýs�
�?�3�I�&��;O%�<%KI?�W�\��w0���|�����+��)~�������V�Z��?z�6^���.q�}�w������H>�?���m��0���,%��޽{�<I�2d�ߥ��kE}}\W=9��{��ڥ��L&��w��/^|�ũ{�ʗ���s�s|�衝�����'�����1�R�Z�(pp	�lb�]V�<"����;G%m�I��x_��v�����)��l<�҃C�96�k�4�,%��}�j�I|�M��S6�%�Kŏ��O~޿�}Ŋk�\i;�2�3f뮮��P{�	�W^y�
`�� QK�ww��T����U�ċb���L&s	�ԧO�J/�ێ�-��Ƙ	��z�W�^/]v�e�jk#Lgg��ƘJ�UQƘ�#G��3mڴ���%}w'������=�������袋VT0��z���{��}稄�ܲ��To�����Ec�O|��S�>mmm���D%����XIex�ҥ�yo�1��s�+OuuuK��� ;P��Z�$��1t��=�9"��5��A�������ؿ��?hР�.�`U#�l����s��!�U��0`km=�-0�3��9���l�~����&�c���h��#Bo;&��;}�&KIex��Śy��1f�sn�� Qq�=����������I�ӄ	vvv�W��(WCf��V!v����1f�1fa�޽���È#6ί��V� �<�۾�DA3O	b�}د�Kzu����466>l�9�w����ӦM�Հ�(WC��e`W�WVLT};������sߏz�x���c�3f�����yJ�������5�$��L&s^�X|�S�냌1�Q'����u|��j�snu�Z���o�N��1��笵o���ι����-z��up�� a�<%KI�iѢE*O�]kk�'��g�����x�^=眻��/��r�����ҥK�.Uk�\��I���`�8�r��Z�p��������97��'ݶKk�߀}z��C����(�À�c��<�n���<�XU,?��S5XkJԺJ������ ڄy_�z뭷v[�j�a�3URgWW�f7�xcM�>���d)�ϟ?_3O�ө�π�s�9s�p�)�п߹J�;���7+��E��_}�ŋoCw�2�^�Am��ƶcH�����C�{}	C�)YJ*O����Jl��5~�X��{ｗ�O>�>}��KV;u�ԩ�CTJ�V&�-Wڎ!Z�b�����#�!G��$1R�mX�'��H�����`�ҥ�w�}�p�	444T?Yi�okkk�§�m�`���r�m���v ��#�l���	����33OP�Iq��,Z�&q�q���{}���or�]wq�	'Я_��%+�s��S�^�;G�uo@���߭�s���tu�r���9�v�!,��&�����=�tg��BP���<��Q}���裏f�-��l�Ҭ F�����;HZ�i;�w�y�������E�P�l���}�(�f����2���a��=��aqhoo箻���d����\��{8�������I�1lM�L��=����� *ORsT��Z>�lV��b�?��O������ﾟ|L�w8�]�r���3g�cZ��\t�E+�𺫚َ!%� p����X��d��߮$U�����4w���t���+W^d��0(��~�{����ӦM�W^O*���cH�̓ʓĉn�I�l�!�;L�8q�*`#~���F_%�A�Zpg&��֔)S���#ێ����nѢEۡ�"e���l_��O�'��-n��b�K�)k�a����6l��p�1�K�I�[8\t��5���X,�|ڴi��K������+X��k���ۓ�2&=7���R��PyJ�'���}��c�?��}¬Y�
�l`��aò�ڧX,��d>��(�����+/<:� ��!���v���KB�+� �nǐ���fU�$6T�ķ��f�c� �v)t�������u�l�0��X6iҤe!�����媫��8���<)������S��<�O}���B�1�z4q����X��!K��USS�l�Q������;D5�`K>@�I|�n�c�E�"i��� UR�O��dQy�Y��!Ƙ4��.%ź���iد௾���,*ORmY�f��c��I��[$���+y�w�J3�<�;CX*Oɢ�$�d�8#��w_��QE6�9���VX�j�o}�K�ID�a�#C��0p*�*�D"	��dn�����1Θ1�m�9�RyJ�<I�L ���\&(N+��#������;G��ř�3DA�)YT��Zv�.�߁�w��#�(��w�
y��+�!���,*ORi_Ɔ����Qh�%���f��H�/��ږ���dQy�J:�6���� oEG$�&O���97�w��-������QQyJ�'��/ׇ��e�h`Q4qD���=�s�n�!1��dQy�J�,0��^��U ��kkk{�9w��y����CDI�)YT�$j� ?��s0� �U�\d�DRb�m��S]]]��!9�3f��$J*Oɢ�$Q:
���+�x8�Y"���� <2p���L�vߪ�1�okk��w����WD�E�I��	�cSʼ~)p,�������(�!`�\.ǀ|G*��s��Q	*O��$�s ��o��/'8����6k헀{�~������M7��_���;�ɜ�����;H%�<%K)3O"�7��Mʼ��P�gW�T���+d~�Vy߾}�ׯ�/��W���qS�Ly�w�JQyJݶ�0v%8�wP��w�f�D�---� ���r����߿�B���l6{��ɓ�����}�F�I�Iʵ0ت���Y�]�%I�[n�%;���sM=��~��a�a�ҥ8��N�<yr��sSyJ�')�v���2�w�(�'�D�������ϟ	�^�u}������%K�P(*��d3W�X1z�̙���T��S��<I��$�U�S��;�+�h�HYk���V���z��b/^�jGG��Ѧ+�bc�W[[[�1C�i�S��<I)6v1�e@����(k� ��޲��j�Lf��o��1�d����ff2����8�f��F�IzjS�>`�c4WGG$������f���jk������Æ�=hР3�sc	�0VJ�so�P�ӦM�W�׉5��dQy����|,��㢉#��Ǐ�Hwq�.�0�sqss�����Y�fu 76�f�9���Zkz�Y6��>y���Y�T��E�I6�7pph�1n .�&�H:�7n�B�p/���ruc����?^�'t���?1bĐl6{�1�H�ùKy���3�9�0���%K~7k֬جN�M�)�T�ҧ�E�C�\7DG$ZZZ>U,E���,7�|��������x�����9r�\.��ܮιAι�Lf�b��2��,+��f2��1O����{�ĉ�B�M4��d��>Y�'��!Ƹ	#I$���3�s3(��m�%�L��cǎ}4L�iӦ��<f��vI��v�.�f�ݿ�u'��+�D")���r0�p�ipx��$���S��<��083� g�""=���r�s�;!�y>��s�W>I(���S��<�ڮ�!�8XM�d�>n�GιQ!���p�W&�p�Z��,*O��o_q�_��T� V�q+7�9�#�)�ڥ�SI%h�S��<�jc	v�.ד�	����I�k���/���a�ӝ��*N��,*O�U�%����8�8"�f����� plȡn>g��m��m�dQy���!��`�ע�#�l��!�l`ϐC]m���l�	�yJ��tN�^��}�,�,�H��?�#��	W�pq�9u��\#4�^�&M�� 7R�/D�ܪ{!�D"	�q+ o��1�XR%*Oɢ���X�g����&p$�td�D�Z{d�X���ǭ������rI��]��]�|
��U��� �OE�H$�ZZZN�%\qz8Vũv�<%��S�DP�ʼ~9�i���Dd#��c�s�R�/+ �f2�í��*�T��S��<��G�_�ʼ~p����$XKK�%��{�|8t�رOD�J|њ�dQyJ�����e^�ApH���$���~�9wa�q�	k��Ӭ	��,*O��a�~`�2�/ _"������l\yFȡ~���pʥ�^�N�$t�.�T�j�����e^_�fE�H$���[����������,*O�Q�6*O�e[��Bw(�z�&��@D6`	����'x5����^t�E+"�%1��v	a�UyJ�-	n��b����h�$��vȪU��#X[���گ��������:t��S2 �>b�ˁ�GG$����"\qr�%�گ���������?�vO��3N��c0!�8"�e�ݏ`��-BS0�4577O�(�Ĕ�Sz�7�x���b����I����#�sw��R�U����ͷGKbL��b����m���m�'C�q#v_�ĳ֞ꜻ�p���L&s��V�)%T�bѢE*O�PGP��<�3hD_c�jiiM��V�G �G�;V�����SB4H��e	��I!ƸA�����GKK�%ιIDp܊���Œ�5O	�x�b���f�V��!Ƙ�	tE�H$����{ι�B���VRL�)!���o�.]Z�%*O�a�I��!�x8�`Ѫ����6L�9��N֮���K�.��S����?����Ikm�V���C�ݿ��k��tӚ���۷��Sm������;p"�,�8"�3a�{��-N3�Ϩ8�f����]��\D��w��,�&�H�|�[��}���!��q+��<%D�޽U�j���;2���1�[��Ik�Ν���'\qrƘKu܊�I3O"�w6�����p4��2"��ƍ�X�X���`�r�Q����"�%	��+V���SmL#x®�^�*�H�Xk/�w�c����o�*�$�n�%DCC��S��
��`3�r�Np����$���T�7�+Nˀ�T�d}4�+W�Ty��c�_P����G��+�D"	c�=�`�0�m�e2��ǎ�x4�$�4��z�Ry��C�M�U��K	ʗ~���GKK�%��	W�^�f���8��h�)!V�Z��O���y�r���c�%Ic���9����<��厽�+^�$�$��SB��כ���R.Qy���#(N��y}�Y�w�%I���VڀsB�����/��rm�!=���y����X�������$H�q+��BuO���O׮�R
�yJ���z����0ت���p����$�ǭ�-N?<x��[���<%�f�bc;�`p��;�	�ed�D����9�'Bu=�妦��bI�趝Ht�!�qڱ��0�U �$�>n�~`��8`���F�J�H�)!���LggI�@i�)Z[�F�C�`J4qD�%��V�1�����F�K�I�)!:;;u�ΟM	v1����&�H�Du�
����[#�%)�5O	QWW���G��}C������I����S��ç��*N	����̓}���B�1���H"���|�9w+�b�׀Ú���(���SR�r9���n1�૑�I����K�s��[>i��[4�DZ�]]]*O�SG�9��!Ƙ�D_��k��ι�C�38�Z��(B��I�)!r�����*��i�'��b�;�3	6��n��rKv���m���C������"���v	����0�d�!Ƹ8(��$���_�k�����8�8B�I*I3O	����3����c���#�"��Z;`���w��꧃>G��K�i�I�g&��]�?���M�d��nM�3���#�,'��<%�n�UT�����;Aqz7�8"�0~���
����i�q����(2���SBd�YS(���X�g��q�?���%��Ik힅Ba60$�0c�y��ͭQ��	���(
�y��y��!�8��U�5�7���V61L�1�K��ͳ��%�SZ��lV�)Z_�q������h�$CKK˧���oW���Vq_T�B3O��,0��?^� ��XD�Yk�r���1�b�hk���)��SBh�)2� ?��[�oܪ{.�D"	����U�#��,y)��l��s4�Dʣ��y��Q�/�_)���q�B�H`�q+��V���!W^y傈r��M�BOۅv0��)�޾8��"ݺ�[�Bp�cN�־A,��T�B3O���-���g��5�D"5������!�޽{�v�%�h�4����(c�I{���y}�9ෑ%�q�ǭ�r��<�l�.q�5O饙'ؕ��Ae^�	~Y"���o}k+��V����*NG�yJݶ+�.?�*���^PwE�H�ƍ?~�������C��V$�T�"�ɘb�X�%i.O�P��M������q+�ۄ� |�Z;%�X"���bQ3O=�%Aqک���`jd�Dj��������[�[ko�&�H�h�SBd2����x�=�����#R�ZZZ>M�j���n�9Y�Ij��SBh�i�6%��G�1�����#R�ZZZ�;�n#�q+�L������Q��4��������>b��q���}---_u�͠�����f;v�"�%R*O	������C�Ǧo .�&�H�3�Z�}�J����r9�"5I�BOۭS=08"�7DG��u�2h9�c�	W\q��[�����y��,081����UR+I���V~B��~����T�"�L�I�(C0ctz�1�� tE�H��Yk�-^��W�Q!��ՠA��p���"��/Z�Z0�L�b��3�_I���V~G��4	����$�f�B���s�q��S�����]��;;;�Gǭ���f�B3O |�z����GG�v�7n��+N�s���$�f�B3O\I��w��N ��URoܸq��_����rss�/"�%�yJ���<}�pX>,�&�H��֞T,&\qjN�֪8I"�<%D�g�F ׆��9�}�^�&�H���~	��`W�r-����M*��QyJ���<�<aW��G#K$R�ZZZ�'��#�q+�2��a��?FK$�T�"�3O�n����_'(N/D�H�6�>n�z½'<||�ر��(�Hli�xB��x�c��Q��oG��I���V&n{�����'\~��oD�K$�T���|
��U���lG�Td�_�Ʃ  �IDATDjP�q+3�a!�z8���/_A,����)�mwAqj(������y�%�A�ǭ�Np�:�_gZk������<%D
��5Я��W '��,�H��n	����i2�9'I#�<%D�g��"8on`��wܚ�Y"�t�UW����u?�kȡt܊��f�"�3O���
[�:�D"5�Z;�����+N������f�"�3O� [�y}8�%�D"5�Z{ �/�����vQyJc�q��>��-�m�ʼ��	�4I-k�Q��]/�n�������b��4���'�[lr�O~u���:�չ
pte���ֱ���U�z:s�tf��<8��iK�[u;��b�-�8"��Z{&0�p��/Njnn֮�"��=�B��������vñ+�;0��q:s����ft�����8Pd+�o^$O� �7f�c\L�&�Hm��~�!�ֶ.���>M*�dPy�)�C��	Gl�}�_�xx�"���ͳ���k��?�Su��c<06�8"�������wB�\6�=��+�|.�P"	��C9�_�q,���Hˀ��E�[�ޭ���~|2�?�M���}��@Sȡ������VD�M��3��_ F�g}Vb��L��7��"���`��\7�;��Z.�����3��C58�Z��VD�C����4�B�`j���F���y+����	v�.�L�l��	DR�Z��8&�Pw _Ю�"VKo�5�A��|����R;����]���9F�)��9n�>��0��`�D����C5h���{Id#T���}�:Vrp	������̮6O��3�T���=8���]$��[��r(�"R��
s�d�Ǟ��T�
�e%z�&�B��C��t{ARi���)
���B�1777?�\"i��T!nw6#˷�F����Y�+�)fo�s�K��e��D��cY�1Djָq��/��.cL�����Q�I�4��W����"��1�~\h����w�ar���߿3K."QC�VI �*�}�PDP[/�XT�e����(�-��X1��'�U�XA�w���ZD�r	�l6l6�ٝ_�����������y��g����3��3���9��C�|����8�����N=�Jgɒ%G���l�0���<�FA��R�<(��}�̕4v�};��w�5|w����FN�
�
��Oj9Em��e��/^��bRI�cy*H<�����G��4�H�Yw��6p�����J�T
K�,9=��)�n%˲c/^�ˢrIedyjP��A�A�b{6��n��| ~S����c�8���PR�(h��{*���n�"5��Ԁx(���������0��}��	}��+�{&%���ƶ[�4�]�
p#�<�. �Tz��:Ń�C�k��Rgi%58�~�����{8�}23I�hl��+il�,����nE*N%u�V`.�]�/Sgi5��4�g3ܺ�iP}��}��T*_|��j�F��6⫳g�~�E���%i��i��A�)�������J�7φ���q����hq�qJ�IM ����]um��-Z���T���i�	��|j�xV�,���%O<�8�!I )�<�����.��|O%i[�<�S|1�2�j๩����g��U���&+��c�zE*�.�����6p�B8;���K�SY��!���d\m�?ݔ�_�	72|�f�8�X�3���w��f8%��-(����.�0��|���֚$ÑM�W�^~�:�4��<?��ƶ[ޙ��W�I%ig�T[����4�:3���?sB��cɒ%�_�������$MW�v"�{	�H��T7�t�
�3�:��N���~0?�8xna�����@a��㝕J�΁��ޫ��jR.����}�e4�$�!��<�o.&���<�@<�~
�:K	]z97u���O?}���� G�� �1�CTc�?!����鹩�\m��+��<��."���<mG|)3��'���YJ�F�������N9�׆����
<v/�͛7/[�re_=��<�g��s�^����GR,O��
'��Qr�^zy(u������V�C��&����O����<2�/�n�s����̘1��s���#�N��m�x�M�C |!�r|�j~���C�	!�|�o�/Ƙ���}zժU#;���/�x����/�m����={���:묉m�-�P����Ln�΢�zY�:��S��>�����r�Z�Վ��+�������s����xY�7s%pr��^L!%fy�J<��h�z���0��ηV�tvv�q�e_K�ĕB�ҥK���^x��j��m�n�Sy� �[�������<�����Rgѓ�C��K~ϏRQ����<4˲h���o9�Cf�t�M���|~�V��ǌ�9y��WLDIEp�iL<����)um�F���OD�uvv�eY��'U�eW?�Y�:ط�Ì���?��+��%��' ȋ	܈?�f����ߥ��:;;_�e�u�����̙3�k������[���۳ >�ũ�u�y�:�����\�e�7i��088�c�=Vϗ�eY�j��ԼJ_�����M�C�4�>�:��8���β�k�^��LĆ��T���W.^����IRJ_���!�9����|�IBS.T*���sR������t�_U*��ݧNj~�.q!�#u��,*��:��VWWש��S�W�Vcݺuĸ�)k:::���λk�rI�_��#�L��z�Kg<�e����L�c�s4�Z��q����3f�8��s���Tf�T�Җ�8����:�&l?n��!45B���^�#6l`d�);�\;{��׺O��ZJ[�8�Á禎�:�xW��|��vڼB�<���jl����Ν�V���ZOy˓��+pl�׼U�Z�vmv500@�V�pbwww5q$Iu(�l�x(�����SgQ��z�2uM�������i��fΜ��O~�G��!�~m��n�6r4�Vw\� �T������S�'�R�(gy�|�qxE<�=\��S�D/���~I���W���L@ۓ��:��w�i�=X�:�d�1�	���JW��B��:�
P�׍�����2�^��=Jm�t�����W�S~�/>���b~�TFe��}D� *H���y���)u��E]�g�Z�=<<�4F/ԘBxZ�qΖ������~�8��ke��ӧ/nHD�ĕ�<�:�
3�=����$��Kf����ɲlN�V��!̌1�	!�+@[�f 3�z�����m���}޶3��]���$��2���SP��<m׶�g���x��Eg�����
�e�#�D���F�u���6�Z�6/uI�)Uy����;��H�= m)=��,�fn]|�.@<u�g�4���x��l]J���l]��]��3H�O����@����q�������\��f-=E)Syf� �>�*O�y��cOV�Z�4W�)�����U��}�Sڿ̧��M�"Bh�K
�vU��OO@��s`ݣ���S.�i�vS��T��6�� �>��\&oGӇ�RGPA��<wI!˓Ԣ�sO-O�h����T�J��:�T�/u I�)Wy��mi��mdYV�է��3H�O9��mhZ��v����/ŬT*w�� �>帗z����Ԕ6�c���� �)��.Ƹn˟Y���V�ޛ*��P��֤!�>e+O^܆FB�P'�օg�h��<� eY��V���{��p�gԽ���}5m^�b�?\�lY5uI�)[y��6�y��#4����<� m��3l�R|���<O�$#�xPvK�c2��K�AR��V�K@�꘶�Oj~u���j���<��?C���g}WW׷�ק�2Ij�ZmU���W��ԟ:���+OIOs��1�v-O��/_�@���W��x����k[�<��i�v4mڴ��xf�,E!,O�ARc�U�jl�<�?�B6wL�9�7���������=�ҳ>�ПeY�Z]����tɵ3�]v�Pww��1�J��`�7w���LBRcJU%⁜O O�C�z ����!T��N:iώ��{��Sg)J��˖-[�:��Ɣ��C�ڏ��6�bŊ!�<u�����wU��W��}�mC�N�أ�>���s �޷jժ��A$5�\婃�����ڊ��mmժU#!�.���ȇ._�t����!��*O��ߤΡ�����.]�s���9p����S��T�R��1����pg�������թsԡ/��+WnJDRq�W�"?NA��M��_��)w�}��C?Ld6�j�7,]����A$������#�0�V"�^z�`�eonM�e��;�/_�JeO�8��<e�p8b{����_��R���Q�,;1BxSOO�5��H��+O���੻�P���4�.���u�k�f,'G-]����H�<��0�E<�E>�:�rW�e��!�T8�S�!|hh��"��9~Ŋ��"ir�n�	��7SGPþ�:���˖-�d�e�2a��쾾�WY��r(��@\�-���s�N�������1��<�x�����5E7�� g����7E�)�	��<��ێ�eq��N��6N:�=+��I!�����t31���Z���e˖�l�nCR+oy��~tpPI�Ev^�壩C�y�y�3�N ������>|�����
8��U����թshB"#<?������5tuu==�p���ˀy��]|Y�1�BX�e���~�����)�����8��U�shB~z9"u��pꩧ>x����,`Vaz��1`=�HamOO���$mW��ӡ��&���:��)�p_MC�T^�U0&�H��%�sh�n�6�MB�Tn�.O Lg�p����^a'IJ���)��F���9�Kk9�/�!IR�� C|X�:�v��a#�CH�dy����Ρ��}�|��$���	�rpK�z�H��p=é�H���Ǎ=8�_��lV�5�w��$may�J���V�Ρ���*�!I��,O�ڍ��GS� ��ZIB���Y��~��q2��K�k��<uI��eyڎp;��,u�����@LIR3�<�H�����QBU2�6��R�$i{,O;z�L���e]S%��p+��:�$I;Rhvqo �TRg)����[=]'Ijf�<�B��ZF�?i2n`��-N��fgy��K�8u��XC�M�^6��"IҮx�n�.>�:G�������OD���<M@��\�3u�6�[F8"����A$I/O�M@��� \�:K�8��$Ij5��	
C/�\k�ϩ����ݩ�H�4Q��k@\���2�#q�V��!�wӟ:�$I�p�����[���QZ�r���$����T���� _^�:K�z�ȩ�6�-uI���SB/��/���m�F�%'IR�p�`�@�H�
`��Y�D���3~)Ij'��If#��O9W��R��p;ץ"IR�,O�(ğ����L��.&�����a$I���I�����~�<`��y&I��
n��a$I�L��)0���D��/u��ԀoP��p;7�#I�T�<M���3�� ��N��NU�KT�0���H�4�,O��L#r�w�*�3��=�"�"�r_�0�$�`yj� �R�m����:�6��R�Jn�{�9V����<5�x��98�5��[����&�w34�$IjZ��&e76��x��<�bo}�Z�F"�g�<R��%Ij+��_�L��Gd��&0��,`�=��5���c6�<B�N2��#��wɾI�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�������B��    IEND�B`�       ECFG      _global_script_classes             _global_script_class_icons             application/config/name         AStar      application/run/main_scene         res://Main.tscn    application/config/icon$         res://icons/AstarPathing.png   display/window/dpi/allow_hidpi            display/window/stretch/mode         2d     display/window/stretch/aspect         expand     layer_names/3d_physics/layer_8      
   unwalkable     layer_names/3d_physics/layer_9         walkable   layer_names/3d_physics/layer_10         floor      layer_names/3d_physics/layer_11         grass   )   rendering/environment/default_environment          res://default_env.tres             GDPC