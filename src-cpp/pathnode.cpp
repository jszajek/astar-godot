#include "pathnode.hpp"
#include "vector3.hpp"
#include <string>
#include <math.h>

using namespace std;
using namespace astar;

/// <summary>
/// Initializes a new default instance of PathNode
/// </summary>
PathNode::PathNode()
{
	this->value = this;
	this->heapIndex = -1;
}

/// <summary>
/// Initializes a new instance of PathNode
/// </summary>
/// <param _worldPos>The world position of the node</param>
/// <param _gridX>The row index of the node</param>
/// <param _gridY>The column index of the node</param>
/// <param _walkable>The walkability of the node</param>
/// <param _movementPenalty>The movement penalty of the node</param>
PathNode::PathNode(Vector3 _worldPos, int _gridX, int _gridY, bool _walkable, int _movementPenalty)
{
	this->value = this;
	this->heapIndex = -1;
	this->walkable = _walkable;
	this->worldPosition = _worldPos;
	this->movementPenalty = _movementPenalty;
	this->gridX = _gridX;
	this->gridY = _gridY;
}

/// <summary>
/// PathNode destructor
/// </summary>
PathNode::~PathNode() { }

/// <summary>
/// F Cost of the node.
/// </summary>
int PathNode::fCost()
{
	return this->gCost + this->hCost;
}

/// <summary>
/// Comparison operator of PathNode.
/// </summary>
/// <param comparison>The node compared to</param>
int PathNode::operator<(PathNode* comparison)
{
	int diff = comparison->fCost() - this->fCost();
	return diff == 0 ? comparison->getHCost() - this->hCost : diff;
}

/// <summary>
/// Equality operator of PathNode
/// </summary>
/// <param comparison>The node compared to</param>
bool PathNode::operator==(PathNode* comparison)
{
	return this->value == comparison->value;
}

/// <summary>
/// Gets to string representation of PathNode
/// </summary>
string PathNode::ToString()
{
	return "Node " + worldPosition.ToString() + "\tMove Penalty " 
				   + to_string(movementPenalty) + "\tWalkable " 
				   + to_string(walkable) + "\tFCost " + to_string(fCost());
}