#include "vector3.hpp"
#include <math.h>

using namespace std;

/// <summary>
/// Initializes a new default instance of Vector3
/// </summary>
Vector3::Vector3() 
{
	this->x = this->y = this->z = 0;
}

/// <summary>
/// Initializes a new instance of Vector3
/// </summary>
/// <param _x>The x value</param>
/// <param _y>The y value</param>
/// <param _z>The z value</param>
Vector3::Vector3(float _x, float _y, float _z)
{
	this->x = _x;
	this->y = _y;
	this->z = _z;
}

/// <summary>
/// Vector3 destructor
/// </summary>
Vector3::~Vector3() { }

/// <summary>
/// Addition operator
/// </summary>
/// <param vec>the vector to be added</param>
Vector3& Vector3::operator+(Vector3 vec)
{
	this->x += vec.x;
	this->y += vec.y;
	this->z += vec.z;
	return *this;
}

/// <summary>
/// Subtraction operator
/// </summary>
/// <param vec>the vector to be subtracted</param>
Vector3& Vector3::operator-(Vector3 vec)
{
	this->x -= vec.x;
	this->y -= vec.y;
	this->z -= vec.z;
	return *this;
}

/// <summary>
/// Scalar Multiplication operator
/// </summary>
/// <param scalar>The scalar to multiply by</param>
Vector3& Vector3::operator*(float scalar)
{
	this->x *= scalar;
	this->y *= scalar;
	this->z *= scalar;
	return *this;
}

/// <summary>
/// Gets the Distance to Vector3
/// </summary>
/// <param vec>The vector to be evaluated</param>
float Vector3::DistanceTo(Vector3 vec)
{
	return sqrtf(pow(vec.x - this->x, 2) + pow(vec.y - this->y, 2) + pow(vec.z - this->z, 2));
}

/// <summary>
/// Gets the Magnitude
/// </summary>
float Vector3::Magnitude()
{
	return (float)sqrt(pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2));
}

/// <summary>
/// Gets Normalized Vector3
/// </summary>
Vector3& Vector3::Normalize()
{
	float norm = Magnitude();
	this->x /= norm;
	this->y /= norm;
	this->z /= norm;
	return *this;
}

/// <summary>
/// Gets the dot product
/// </summary>
/// <param vec>the vector to be multiplied</param>
float Vector3::Dot(Vector3 vec)
{
	return (this->x * vec.x) + (this->y * vec.y) + (this->z * vec.z);
}

/// <summary>
/// Equality of the Vector3
/// </summary>
/// <param vec>the vector to be evaluted</param>
bool Vector3::Equals(Vector3 vec)
{
	return this->x == vec.x && this->y == vec.y && this->z == vec.z;
}

/// <summary>
/// Gets the string representation of the Vector3
/// </summary>
string Vector3::ToString()
{
	return "(" + to_string(this->x) + ", " + to_string(this->y) + ", " + to_string(this->z) + ")";
}