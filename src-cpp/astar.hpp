#ifndef ASTAR_HPP
#define ASTAR_HPP

#include <vector>
#include "vector2.hpp"
#include "vector3.hpp"
#include "matrix.hpp"
#include "pathnode.hpp"
#include "grid.hpp"
#include "minheap.hpp"

using namespace std;

namespace astar
{
	class AStar
	{
	private:
		Grid* gridMap;
		int minPenalty;
		int maxPenalty;
	public:
		AStar(std::Vector2 _worldSize, int _minPenalty, int _maxPenalty);
		~AStar();
		void addGridNode(PathNode* node);
		PathNode* getGridNode(int _gridX, int _gridY);
		const vector<std::Vector3> findPath(std::Vector3 _startPosition, std::Vector3 _targetPosition);
		const vector<std::Vector3> retracePath(PathNode* _startNode, PathNode* _endNode);
		const vector<std::Vector3> simplifyPath(const vector<PathNode> _pathNodes);
		int getDistance(PathNode* nodeA, PathNode* nodeB);
		const vector<int> AStar::blurWeights(int _blurSize);

		bool contains(const vector<PathNode> _pathNodes, const PathNode node);
	};
}

#endif // !ASTAR_HPP