#ifndef GDASTAR_HPP
#define GDASTAR_HPP

#include <Godot.hpp>
#include <Node.hpp>
#include "astar.hpp"
#include "smoothpath.hpp"

using namespace astar;

namespace godot
{
	class GDAStar : public Node
	{
		GODOT_CLASS(GDAStar, Node)
	private:
		AStar* astar;
		SmoothPath* smoothpath;

		godot::Vector3 Vector3Formatter(std::Vector3 vec);
		const vector<std::Vector3> convertPoolVector3Array(PoolVector3Array _array);
	public:
		static void _register_methods();
		GDAStar();
		~GDAStar();
		void _init();

		void setupGrid(int sizeX, int sizeY, int _minPenalty, int _maxPenalty);
		void addGridPoint(godot::Vector3 worldPoint, int gridX, int gridY, bool walkable, int movementPenalty);
		Array getGridPoint(int gridX, int gridY);
		PoolVector3Array findPath(godot::Vector3 start, godot::Vector3 end);
		Array blurWeight(int _blurSize);

		void smoothPath(PoolVector3Array _waypoints, godot::Vector3 startPos, float turnDist, float stoppingDist);
		int getTurnBoundariesSize();
		int getSlowDownIndex();
		int getFinishLineIndex();
		float distanceFromPoint(int _index, godot::Vector2 position);
		bool crossedTurnBoundary(int _index, godot::Vector2 position);
		godot::Vector3 getLookPoint(int _index);

		Array getVisualLine(int _index);
	};
}

#endif // !GDASTAR_HPP