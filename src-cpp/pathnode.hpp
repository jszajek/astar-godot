#ifndef PATHNODE_HPP
#define PATHNODE_HPP

#include <string>
#include "vector3.hpp"
#include "minheap.hpp"

using namespace std;

namespace astar
{
	class PathNode : public HeapItem<PathNode>
	{
	private:
		PathNode* parent;
		int gridX = 0, gridY = 0;
		int gCost = 0, hCost = 0;
		bool walkable = false;
		std::Vector3 worldPosition = Vector3();
		int movementPenalty = 0;
	public:
		PathNode();
		PathNode(std::Vector3 _worldPos, int _gridX, int _gridY, bool _walkable, int _movementPenalty);
		~PathNode();

		bool getWalkable() { return walkable; }
		void setWalkable(bool _walkable) { walkable = _walkable; }

		int getGridX() { return gridX; }
		int getGridY() { return gridY; }
		
		std::Vector3 getWorldPosition() { return worldPosition; }

		PathNode* getParent() { return parent; }
		void setParent(PathNode* _parent) { parent = _parent; }

		int getMovementPenalty() { return movementPenalty; }
		void setMovementPenalty(int _movementPenalty) { movementPenalty = _movementPenalty; }
		int getGCost() { return gCost; }
		void setGCost(int _gCost) { gCost = _gCost; }
		int getHCost() { return hCost; }
		void setHCost(int _hCost) { hCost = _hCost; }

		int fCost();
		int operator<(PathNode* node);
		bool operator==(PathNode* node);

		string ToString();
	};
}

#endif // !PATHNODE_HPP
