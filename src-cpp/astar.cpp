#include "astar.hpp"
#include <algorithm>

using namespace std;
using namespace astar;

/// <summary>
/// Initializes a new instance of AStar
/// </summary>
/// <param _worldSize>The world size of the AStar algorithm</param>
/// <param _minPenalty>The minimum movement penalty</param>
/// <param _maxPenalty>The maximum movement penalty</param>
AStar::AStar(Vector2 _worldSize, int _minPenalty, int _maxPenalty)
{
	gridMap = new Grid(_worldSize.x, _worldSize.y, _minPenalty, _maxPenalty);
}

/// <summary>
/// AStar destructor
/// </summary>
AStar::~AStar() 
{
	gridMap->~Grid();
}

/// <summary>
/// Adds a new PathNode to the astar grid.
/// </summary>
/// <param node>The PathNode to be added</param>
void AStar::addGridNode(PathNode* node)
{
	this->gridMap->addToGrid(node->getGridX(), node->getGridY(), node);
}

/// <summary>
/// Gets the PathNode from the astar grid.
/// </summary>
/// <param _gridX>The row index of the grid</param>
/// <param _gridY>The column index of the grid</param>
PathNode* AStar::getGridNode(int _gridX, int _gridY)
{
	return this->gridMap->getGridNode(_gridX, _gridY);
}

/// <summary>
/// Gets a path from the start point to the target point in the form of a list of world points.
/// </summary>
/// <param _startPosition>The start world position</param>
/// <param _targetPosition>The target world position</param>
const vector<Vector3> AStar::findPath(Vector3 _startPosition, Vector3 _targetPosition)
{
	bool path_success = false;
	PathNode* startNode = this->gridMap->nodeFromWorldPoint(_startPosition);
	PathNode* targetNode = this->gridMap->nodeFromWorldPoint(_targetPosition);

	if (startNode->getWalkable() && targetNode->getWalkable())
	{
		// A* Path finding algorithm
		MinHeap<PathNode> openSet = MinHeap<PathNode>(gridMap->getMaxSize());
		vector<PathNode> closedSet = vector<PathNode>();
		openSet.Add(startNode);

		while (openSet.size() > 0)
		{
			PathNode* currentNode = openSet.RemoveFirst()->value;
			closedSet.push_back(*currentNode);

			if (currentNode->Equals(*targetNode))
			{
				path_success = true;
				break;
			}
			vector<PathNode> neighbors = gridMap->getNeighbors(currentNode);
			for (int i = 0; i < neighbors.size(); i++)
			{
				PathNode* neighbor = neighbors.at(i).value;
				if (!neighbor->getWalkable() || contains(closedSet, *neighbor))
				{
					continue;
				}
				int newMoveCostToNeighbor = (currentNode->getGCost() + getDistance(currentNode, neighbor)) + neighbor->getMovementPenalty();
				if (!openSet.Contains(neighbor) || newMoveCostToNeighbor < neighbor->getGCost())
				{
					neighbor->setGCost(newMoveCostToNeighbor);
					neighbor->setHCost(getDistance(neighbor, targetNode));
					neighbor->setParent(currentNode);
					if (!openSet.Contains(neighbor))
					{
						openSet.Add(neighbor);
					}
					else
					{
						openSet.UpdateItem(neighbor);
					}
				}
			}
		}
	}
	if (path_success)
	{
		return retracePath(startNode, targetNode);
	}
	else
	{
		return {};
	}
}

/// <summary>
/// Helper method that checks the passed list of PathNodes whether it contains a PathNode.
/// </summary>
/// <param _pathNodes>The passed list to be checked</param>
/// <param node>The node to be checked</param>
bool AStar::contains(const vector<PathNode> _pathNodes, const PathNode node)
{
	for (int i = 0; i < _pathNodes.size(); i++)
	{
		if (_pathNodes.at(i).value->Equals(*node.value))
		{
			return true;
		}
	}
	return false;
}

/// <summary>
/// Retraces the path.
/// </summary>
/// <param _startNode>The start node</param>
/// <param _endNode>The end node</param>
const vector<Vector3> AStar::retracePath(PathNode* _startNode, PathNode* _endNode)
{
	vector<PathNode> pathNodes = vector<PathNode>();
	PathNode* currentNode = _endNode;
	while (currentNode != _startNode)
	{
		pathNodes.push_back(*currentNode);
		currentNode = currentNode->getParent();
	}

	vector<Vector3> waypoints = simplifyPath(pathNodes);
	reverse(waypoints.begin(), waypoints.end());
	return waypoints;
}

/// <summary>
/// Simplifies the passed path list of world points. 
/// Removing duplicates of same slope.
/// </summary>
/// <param _pathNodes>The path or the list of nodes</param>
const vector<Vector3> AStar::simplifyPath(const vector<PathNode> _pathNodes)
{
	vector<Vector3> waypoints = vector<Vector3>();
	Vector2 directionOld = Vector2(0, 0);
	for (int i = 1; i < _pathNodes.size(); i++)
	{
		PathNode prior = _pathNodes.at(i - 1);
		PathNode current = _pathNodes.at(i);
		Vector2 directionNew = Vector2(prior.getGridX() - current.getGridX(), prior.getGridY() - current.getGridY());

		if ((i == 1 || i == _pathNodes.size() - 1) || directionNew.Equals(directionOld))
		{
			waypoints.push_back(current.getWorldPosition());
		}
		directionOld = directionNew;
	}
	return waypoints;
}

/// <summary>
/// Gets the distance between two PathNodes.
/// </summary>
/// <param nodeA>The comparison node</param>
/// <param nodeB>The other node</param>
int AStar::getDistance(PathNode* nodeA, PathNode* nodeB)
{
	int distX = abs(nodeA->getGridX() - nodeB->getGridX());
	int distY = abs(nodeA->getGridY() - nodeB->getGridY());

	if (distX > distY)
	{
		return 14 * distY + 10 * (distX - distY);
	}
	else
	{
		return 14 * distX + 10 * (distY - distX);
	}
}

/// <summary>
/// BBlurs the movement penalties of the grid.
/// </summary>
/// <param _blurSize>The kernel blur size</param>
const vector<int> AStar::blurWeights(int _blurSize)
{
	return gridMap->blurPenaltyMap(_blurSize);
}