#ifndef MINHEAP_HPP
#define MINHEAP_HPP

namespace std
{
	// Wrapping class for items in heap.
	template <class T>
	class HeapItem
	{
	public:
		int heapIndex;
		T* value;

		// Default constructor
		HeapItem()
		{
			heapIndex = -1;
			value = NULL;
		}

		// Constructor with values
		HeapItem(T* object, int index)
		{
			value = object;
			heapIndex = index;
		} 

		// Comparison method
		const int CompareTo(HeapItem<T> comparison) 
		{
			return this->value->operator<(comparison.value);
		}

		// Equality method
		const bool Equals(HeapItem<T> comparison)
		{
			return this->value->operator==(comparison.value);
		}
	};

	template <class T>
	class MinHeap
	{
	private:
		HeapItem<T>* harr;
		int heapSize;
	public:
		// Constructor with capacity
		MinHeap(int _capacity)
		{
			heapSize = 0;
			harr = new HeapItem<T>[_capacity];
		}

		// Destructor
		~MinHeap() { delete harr; }

		// Gets the current size of the heap.
		int size() { return heapSize; }

		// Gets the index of the parent.
		int parent(HeapItem<T>* item) { return (item->heapIndex - 1) / 2.0; }
		
		// Gets the index of the left child.
		int left(HeapItem<T>* item) { return (2 * item->heapIndex) + 1; }
		
		// Gets the index of the right child.
		int right(HeapItem<T>* item) { return (2 * item->heapIndex) + 2; }

		// Gets heap item at the index.
		HeapItem<T>* getIndex(int index) { return &harr[index]; }

		// Adds a new heap item.
		void Add(HeapItem<T>* item)
		{
			item->heapIndex = heapSize;
			harr[heapSize] = *item;
			SortUp(item);
			heapSize++;
		}

		// Removes and returns the first heap, then resorts the heap.
		HeapItem<T>* RemoveFirst()
		{
			HeapItem<T> first = harr[0];
			heapSize--;
			harr[0].value = harr[heapSize].value;
			harr[0].heapIndex = harr[0].value->heapIndex = 0;
			SortDown(&harr[0]);
			return &first;
		}

		// Updates the position of the item in relation to the heap.
		void UpdateItem(HeapItem<T>* item)
		{
			SortUp(item);
		}

		// Determines if the heap contains the passed item.
		bool Contains(HeapItem<T>* item)
		{
			if (item->value->heapIndex < 0 || harr[item->value->heapIndex].heapIndex < 0)
			{
				return false;
			}
			return item->Equals(harr[item->value->heapIndex]);
		}

		// Sorts the larger item down until it's in the correct position
		void SortDown(HeapItem<T>* item)
		{
			HeapItem<T>* tempItem = item;
			while (true)
			{
				int childIndexLeft = left(tempItem);
				int childIndexRight = right(tempItem);
				int swapIndex = 0;
				if (childIndexLeft < heapSize)
				{
					swapIndex = childIndexLeft;
					if (childIndexRight < heapSize)
					{
						if (((HeapItem<T>)harr[childIndexLeft]).CompareTo(harr[childIndexRight]) < 0)
						{
							swapIndex = childIndexRight;
						}
					}
					if (tempItem->CompareTo(harr[swapIndex]) < 0)
					{
						Swap(tempItem, &harr[swapIndex]);
						tempItem = &harr[swapIndex];
					}
					else
					{
						return;
					}
				}
				else
				{
					return;
				}
			}
		}

		// Sorts the smaller item upwards until it's in the correct position
		void SortUp(HeapItem<T>* item)
		{
			HeapItem<T>* tempItem = item;

			int parentIndex = parent(item);
			while (true)
			{
				if (tempItem->CompareTo(harr[parentIndex]) > 0)
				{
					Swap(tempItem, &harr[parentIndex]);
					tempItem = &harr[parentIndex];
				}
				else
				{
					break;
				}
				parentIndex = parent(tempItem);
			}
		}

		// Swaps the heap items
		void Swap(HeapItem<T>* x, HeapItem<T>* y)
		{
			T* temp = x->value;
			int xIndex = x->heapIndex;
			int yIndex = y->heapIndex;
			harr[x->heapIndex].value = harr[y->heapIndex].value;
			harr[y->heapIndex].value = temp;

			harr[x->heapIndex].heapIndex = harr[x->heapIndex].value->heapIndex = xIndex;
			harr[y->heapIndex].heapIndex = harr[y->heapIndex].value->heapIndex = yIndex;
		}
	};
}

#endif // !MINHEAP_HPP
