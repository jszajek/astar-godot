#include "gdastar.hpp"
#include <vector>

using namespace godot;
using namespace std;

/// <summary>
/// Registers methods for use in gdscript
/// </summary>
void GDAStar::_register_methods()
{
	register_method("setupGrid", &GDAStar::setupGrid);
	register_method("addGridPoint", &GDAStar::addGridPoint);
	register_method("findPath", &GDAStar::findPath);
	register_method("getGridPoint", &GDAStar::getGridPoint);
	register_method("smoothPath", &GDAStar::smoothPath);
	register_method("getTurnBoundariesSize", &GDAStar::getTurnBoundariesSize);
	register_method("getSlowDownIndex", &GDAStar::getSlowDownIndex);
	register_method("getFinishLineIndex", &GDAStar::getFinishLineIndex);
	register_method("distanceFromPoint", &GDAStar::distanceFromPoint);
	register_method("crossedTurnBoundary", &GDAStar::crossedTurnBoundary);
	register_method("getLookPoint", &GDAStar::getLookPoint);
	register_method("blurWeights", &GDAStar::blurWeight);

	register_method("getVisualLine", &GDAStar::getVisualLine);
}

/// <summary>
/// Initializes a new default instance of GDAStar
/// </summary>
GDAStar::GDAStar() { }

/// <summary>
/// GDAStar destructor
/// </summary>
GDAStar::~GDAStar() { }

/// <summary>
/// GDAStar Initializer
/// </summary>
void GDAStar::_init() { }

/// <summary>
/// Sets up the astar grid.
/// </summary>
/// <param sizeX>The number of rows in the grid</param>
/// <param sizeY>The number of columns in the grid</param>
/// <param _minPenalty>The minimum movement penalty</param>
/// <param _maxPenalty>The maximum movement penalty</param>
/// <param _movementPenalty>The movement penalty of the node</param>
void GDAStar::setupGrid(int sizeX, int sizeY, int _minPenalty, int _maxPenalty)
{
	astar = new AStar(std::Vector2(sizeX, sizeY), _minPenalty, _maxPenalty);
}

/// <summary>
/// Adds a grid point to the astar grid.
/// </summary>
/// <param _worldPos>The world position of the node</param>
/// <param _gridX>The row index of the node</param>
/// <param _gridY>The column index of the node</param>
/// <param _walkable>The walkability of the node</param>
/// <param _movementPenalty>The movement penalty of the node</param>
void GDAStar::addGridPoint(godot::Vector3 worldPoint, int gridX, int gridY, bool walkable, int movementPenalty)
{
	astar->addGridNode(new PathNode(std::Vector3(worldPoint.x, worldPoint.y, worldPoint.z), gridX, gridY, walkable, movementPenalty));
}

/// <summary>
/// Gets the values of the PathNode at the grid point.
/// </summary>
/// <param gridX>The row index of the node</param>
/// <param gridY>The column index of the node</param>
Array GDAStar::getGridPoint(int gridX, int gridY)
{
	PathNode* node = astar->getGridNode(gridX, gridY);
	Array _array = Array();
	_array.append(node->getMovementPenalty());
	_array.append(Vector3Formatter(node->getWorldPosition()));
	_array.append(node->getWalkable());
	return _array;
}

/// <summary>
/// Finds a path from the passed start world point to the end world point.
/// </summary>
/// <param start>The starting world position</param>
/// <param end>The ending world position</param>
PoolVector3Array GDAStar::findPath(godot::Vector3 start, godot::Vector3 end)
{
	vector<std::Vector3> waypoints = astar->findPath(std::Vector3(start.x, start.y, start.z), std::Vector3(end.x, end.y, end.z));
	PoolVector3Array arr = PoolVector3Array();
	for (int i = 0; i < waypoints.size(); i++)
	{
		std::Vector3 temp = waypoints.at(i);
		arr.append(godot::Vector3(temp.x, temp.y, temp.z));
	}
	return arr;
}

/// <summary>
/// Blurs the movement penalties of the grid and returns minimum and maximum penalties.
/// </summary>
/// <param _blurSize>The kernel size to blur with</param>
Array GDAStar::blurWeight(int _blurSize)
{
	vector<int> results = astar->blurWeights(_blurSize);
	Array godotResults = Array();
	for (int i = 0; i < results.size(); i++)
	{
		godotResults.append(results.at(i));
	}
	return godotResults;
}

/// <summary>
/// Smooths the currently calculated path.
/// </summary>
/// <param waypoints>The list of world position points</param>
/// <param startPos>The starting world position position</param>
/// <param turnDist>The turn distance for the new path</param>
/// <param stoppingDist>The stopping distance for the new path</param>
void GDAStar::smoothPath(PoolVector3Array _waypoints, godot::Vector3 startPos, float turnDist, float stoppingDist)
{
	vector<std::Vector3> points = convertPoolVector3Array(_waypoints);
	std::Vector3 startPoint = std::Vector3(startPos.x, startPos.y, startPos.z);
	this->smoothpath = new SmoothPath(points, startPoint, turnDist, stoppingDist);
}

/// <summary>
/// Gets the size of the turn boundaries of the smoothed path.
/// </summary>
int GDAStar::getTurnBoundariesSize()
{
	return this->smoothpath->getTurnBoundariesSize();
}

/// <summary>
/// Gets the slow down index of the smoothed path.
/// </summary>
int GDAStar::getSlowDownIndex()
{
	return this->smoothpath->getSlowDownIndex();
}

/// <summary>
/// Gets the finish line index of the smoothed path.
/// </summary>
int GDAStar::getFinishLineIndex()
{
	return this->smoothpath->getFinishLineIndex();
}

/// <summary>
/// Gets the distance of the point from the line in the smoothed path.
/// </summary>
/// <param _index>The index of the line in the smoothed path</param>
/// <param position>The point to be checked</param>
float GDAStar::distanceFromPoint(int _index, godot::Vector2 position)
{
	std::Vector2 vecPosition = std::Vector2(position.x, position.y);
	return this->smoothpath->distanceFromPoint(_index, vecPosition);
}

/// <summary>
/// Gets whether the point has crossed the line in the smoothed path.
/// </summary>
/// <param _index>The index of the line in the smoothed path</param>
/// <param position>The point to be checked</param>
bool GDAStar::crossedTurnBoundary(int _index, godot::Vector2 position)
{
	std::Vector2 vecPosition = std::Vector2(position.x, position.y);
	return this->smoothpath->crossedTurnBoundary(_index, vecPosition);
}

/// <summary>
/// Gets the look point in the smoothed path.
/// </summary>
/// <param _index>The index of the look point in the smoothed path</param>
godot::Vector3 GDAStar::getLookPoint(int _index)
{
	std::Vector3 vec = this->smoothpath->getLookPoint(_index);
	return godot::Vector3(vec.x, vec.y, vec.z);
}

/// <summary>
/// Gets relevent data for visualizing a line.
/// </summary>
/// <param _index>The index of the line in the smoothed path</param>
Array GDAStar::getVisualLine(int _index)
{
	Array arr = Array();
	Line line = this->smoothpath->getLine(_index);
	godot::Vector3 lineDir = godot::Vector3(1, 0, line.getGradient()).normalized();
	godot::Vector3 lineCenter = godot::Vector3(line.getPointOnLine1().x, 0, line.getPointOnLine2().y) + godot::Vector3(0, 1, 0);
	arr.append(lineDir);
	arr.append(lineCenter);
	return arr;
}

#pragma region Private Methods

/// <summary>
/// Converts C++ Vector 3 to Godot Vector 3.
/// </summary>
/// <param vec>The vector to be converted</param>
godot::Vector3 GDAStar::Vector3Formatter(std::Vector3 vec)
{
	return godot::Vector3(vec.x, vec.y, vec.z);
}

/// <summary>
/// Converts Godot Vector 3 Array to C++ vector of Vector 3
/// </summary>
/// <param _array>The array to be converted</param>
const vector<std::Vector3> GDAStar::convertPoolVector3Array(PoolVector3Array _array)
{
	vector<std::Vector3> converted = vector<std::Vector3>();
	for (int i = 0; i < _array.size(); i++)
	{
		godot::Vector3 vec = _array[i];
		converted.push_back(std::Vector3(vec.x, vec.y, vec.z));
	}
	return converted;
}

#pragma endregion Private Methods